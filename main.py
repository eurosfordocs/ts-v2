#!/usr/bin/env python3
import logging
from datetime import datetime
from pathlib import Path

import click

import src.backup
import src.transform
from src import backup, check, legacy_ts
from src.beneficiaires import create_raw_beneficiaire
from src.constants.csv import (
    DECLARATION_CSV_GZ,
    FINESS_CSV,
    FINESS_URL,
    RPPS_URL,
    RPPS_ZIP_NAME,
    TRANSPARENCE_SANTE_URL,
    TS_DECLARATION_CSV_GZ,
    TS_DECLARATION_PARQUET,
)
from src.constants.tables import (
    BENEFICIAIRE,
    ENTREPRISES,
    RAW_DECLARATION,
    RAW_FINESS,
    RPPS,
    TS_DECLARATION,
)
from src.format import ts
from src.merge import merge
from src.metabase.api import load_metabase_cache
from src.metabase.cache import delete_metabase_cache
from src.rpps import reset_rpps
from src.settings import BACKUP_AFTER_PROCESS, CONFIG, S3_BUCKET, Environment
from src.sirene import process_sirene
from src.utils import duck, openstack
from src.utils.downloader import Downloader
from src.utils.utils import frame_it, timeit


@click.group()
def cli():
    pass


@cli.command("extract")
def cmd_extract() -> None:
    pre_process()
    extract()


@frame_it
def extract():
    ts_downloader = Downloader(TRANSPARENCE_SANTE_URL, DECLARATION_CSV_GZ)
    ts_downloader.run()

    rpps_downloader = Downloader(
        RPPS_URL, RPPS_ZIP_NAME, verify_ssl=False, update_days=30
    )
    rpps_downloader.run()

    finess_downloader = Downloader(FINESS_URL, FINESS_CSV, update_days=30)
    finess_downloader.run()

    process_sirene()


@cli.command("transform")
def cmd_transform():
    pre_process()
    src.transform.transform()


@frame_it
def load():
    duck.drop_table(RAW_DECLARATION)
    ts_csv_fp = CONFIG.RAW_DATA_DIR / DECLARATION_CSV_GZ
    duck.import_csv(RAW_DECLARATION, ts_csv_fp, auto=False, column_types=ts.TS_TYPES)

    duck.drop_table(RAW_FINESS)
    duck.import_csv(
        RAW_FINESS,
        CONFIG.RAW_DATA_DIR / FINESS_CSV,
        auto=True,
        delim=",",
        header=True,
    )


@cli.command("load")
def cmd_load():
    pre_process()
    load()


@cli.command("merge")
def cmd_merge():
    pre_process()
    merge()


@cli.command("legacy")
def cmd_legacy():
    legacy_ts.merge_legacy_declaration()


@frame_it
def export():
    for table in [TS_DECLARATION, ENTREPRISES, BENEFICIAIRE, RPPS]:
        output_fp = CONFIG.CLEANED_DATA_DIR / f"{table}.parquet"
        duck.export_parquet(table=table, fp=output_fp)
        logging.info(f"exported to {output_fp}")
        if table == TS_DECLARATION:
            duck.export_csv(table=table, fp=output_fp.with_suffix(".csv.gz"))


@cli.command("export")
def cmd_export():
    pre_process()
    export()


@cli.command("backup")
def cmd_backup():
    pre_process()
    backup.daily_backup()


def pre_process():
    duck.log_mem_limit()
    logging.info(f"Env: {CONFIG.ENVIRONMENT}")

    # This is commented because it was never useful, we must think of another way to follow it (if at all)
    # if CONFIG.ENVIRONMENT == Environment.PROD:
    #    ooms = docker.count_oom_metabase()
    #    logging.info(f"Metabase out of memory errors in the last 24h: {ooms}")
    #    if ooms > 0:
    #        slack.send_slack_message(
    #            f"Metabase out of memory errors in the last 24h: *{ooms}*"
    #        )


@timeit
def process() -> None:
    extract()
    load()
    merge()
    src.transform.transform()
    export()
    if CONFIG.ENVIRONMENT == Environment.PROD:
        if BACKUP_AFTER_PROCESS:
            backup.daily_backup()


@cli.command("import_clean_data")
def cmd_import_clean_data():
    pre_process()
    import_clean_data()


def import_clean_data():
    # in prod, we import the data from S3, since the web server doesn't run the process
    # in dev, the files will be there locally already
    if CONFIG.ENVIRONMENT == Environment.PROD:
        backup.restore_from_S3(date=datetime.today().date())
        # We still download and load the raw declaration so that the raw data is also present in duckdb
        extract()
        load()
    else:
        refresh_db()

    # we generate the clean csv ts_declaration.csv.gz (so that it can be downloaded)
    duck.generate_csv_from_parquet(
        parquet_fp=CONFIG.CLEANED_DATA_DIR / TS_DECLARATION_PARQUET,
        csv_fp=CONFIG.CLEANED_DATA_DIR / TS_DECLARATION_CSV_GZ,
    )

    reset_metabase_cache()
    create_raw_beneficiaire()


def refresh_db():
    """
    Remove the duckdb file to create a new one and reimport only required tables.
    It seems duckdb does not free disk space when removing data.
    """
    tables = [BENEFICIAIRE, ENTREPRISES, TS_DECLARATION]
    fns = [CONFIG.CLEANED_DATA_DIR / f"{table}.parquet" for table in tables]
    for fn in fns:
        assert fn.exists()

    dbname = Path(CONFIG.DUCKDB_FP)
    dbname.unlink()

    for table, fn in zip(tables, fns):
        duck.import_parquet(table, str(fn))

    ts_csv_fp = CONFIG.RAW_DATA_DIR / DECLARATION_CSV_GZ
    duck.import_csv(RAW_DECLARATION, ts_csv_fp, auto=False, column_types=ts.TS_TYPES)


@cli.command("process")
def cmd_process():
    pre_process()
    process()


@cli.command("data_check")
def data_check():
    check.check(table=TS_DECLARATION)


@cli.command("daily_backup")
def cmd_daily_backup():
    backup.daily_backup()


@cli.command("load_metabase_cache")
def cmd_load_metabase_cache():
    pre_process()
    reset_metabase_cache()


def reset_metabase_cache():
    delete_metabase_cache()
    load_metabase_cache()


@cli.command("restore_from_s3")
@click.argument("date", type=click.DateTime(formats=["%Y-%m-%d"]))
@click.option("--bucket", "-b", "bucket", type=str, default=S3_BUCKET)
def cmd_restore_from_s3(date, bucket):
    backup.restore_from_S3(date=date.date())


@cli.command("restore_mb_data_from_s3")
@click.argument("date", type=click.DateTime(formats=["%Y-%m-%d"]))
@click.option("--bucket", "-b", "bucket", type=str, default=S3_BUCKET)
def cmd_restore_mb_data_from_s3(date, bucket):
    backup.restore_mb_data(date=date.date(), bucket=bucket)


@cli.command("reset_rpps")
def cmd_reset_rpps():
    reset_rpps()


@cli.command("shelve_compute_instance")
def shelve_compute_instance():
    openstack.shelve_compute_instance()


@cli.command("unshelve_compute_instance")
def unshelve_compute_instance():
    openstack.unshelve_compute_instance()


if __name__ == "__main__":
    try:
        cli()
    except Exception:
        logging.exception("Failed run")
        raise
