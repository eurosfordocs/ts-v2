#!/usr/bin/env bash

host=${1:-"ts-prod"}

ssh -t root@${host} "cd ts-v2 && docker compose logs --follow metabase"
