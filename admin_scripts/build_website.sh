#!/usr/bin/env bash

host=${1:-"localhost"}

if [ $host == "localhost" ]
then
  docker compose build frontend
  docker compose stop frontend
  docker compose rm -f frontend
  docker compose up -d frontend
else
  ssh -t ubuntu@${host} "cd ts-v2 && git pull && docker compose build frontend && docker compose stop frontend && docker compose rm -f frontend && docker compose up -d frontend"
fi
