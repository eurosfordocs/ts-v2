#!/usr/bin/env bash

source .env

email=$1
prenom=${2:-"Prénom"}
nom=${3:-"Nom"}
host=${4:-"ts-prod"}

if [ $# -eq 0 ]
  then
    echo "No email provided. Usage: create_mb_user <email> [Prénom] [Nom] [host]"
    exit 0
fi

url="http://${host}:3000/api/session"
payload="{\"username\": \"${METABASE_USERNAME}\", \"password\": \"${METABASE_PASSWORD}\"}"
id=$(curl -s -X POST -H 'Content-Type: application/json' \
             -d "${payload}" \
             $url | \
             python3 -c "import sys, json; print(json.load(sys.stdin)['id'])")

echo Authentifié

payload="{\"first_name\": \"${prenom}\", \"last_name\": \"${nom}\", \"email\": \"${email}\"}"
url="http://${host}:3000/api/user"

curl -s -X POST -H 'Content-Type: application/json' \
                -H "X-Metabase-Session: ${id}" \
                -d "${payload}" \
                $url
