#!/usr/bin/env bash
# Backup metabase data from host to local file
set -euo pipefail

host=${1:-"ts-prod"}
tag=${2:-""}

BACKUP_FILE=data/dump/pg_dump_mbpostgres_${host}_`date +%Y-%m-%d_%H-%M-%S`_${tag}.sql
echo Dump metabase backend on ${host} to ${BACKUP_FILE}
echo You will be prompted to give the password for Postgres metabase backend

pg_dump -h ${host} -p 6543 -U postgres > ${BACKUP_FILE}

echo Done.

echo "You can restore this metabase data locally, with command 'admin_scripts/restore_metabase_dump_on_localhost.sh ${BACKUP_FILE}'"
