#!/usr/bin/env bash

host=${1:-"ts-prod"}

ssh -t ubuntu@${host} "cd ts-v2 && docker compose stop metabase && docker compose rm -f metabase  && docker compose up -d metabase && docker compose restart nginx"
