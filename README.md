*This project is the new version of [this project](https://gitlab.com/eurosfordocs/transparence-sante), adapted to the
new format that was rolled out early 2022.
It facilitate the use of [Transparence Santé](www.transparence.sante.gouv.fr) dataset,
as inspired by ProPublica's [DollarsForDocs](https://projects.propublica.org/docdollars) project.
You can see it live at [www.eurosfordocs.fr](http://www.eurosfordocs.fr/) website*

# Tech overview

This project contains:

- the code that downloads new data, cleans it as store it in parquet files as well as in a duckdb file. (folder `src`,
  and see [Usage](#Usage))
- A static website based on Jekyll, see folder `frontend`, and docker containers `nginx` and `frontend`
- A metabase implementation, accessible online at `/metabase`. This is for now a custom metabase image to work with
  duckdb. The very simple Dockerfile is in folder `Metaduck`
- All of this is orchestrated in Docker containers.
- Some admin scripts

# Installation

Installation should be straightforward on UNIX systems (Mac, Linux). It has not been tested on Windows.

Execute the following commands in a terminal :

    git clone https://gitlab.com/eurosfordocs/ts-v2.git
    cd ts-v2
    cp template.env .env  # Create a default .env file from template

Modify your `.env` file to your needs

If necessary, install make:

    sudo apt install make

If necessary and if you want to run the website, install docker and docker-compose, see how
to [here](https://docs.docker.com/engine/install/ubuntu/)

Create a venv and install necessary python libs:

    make install

# Usage

## Servers

Because of memory limitation on the prod server, we now have a process (see .gitlab-ci.yml) that uses 2 servers:

* the prod server is the one serving the website
* the compute instance is the one running the process every night.

The data is backed up on S3 every night after the process, so the prod server just gets the clean data from the S3.

## Data processing

The entry points of this project are defined in the `Makefile`.

You can call each of the function below by calling `make xxx`, where xxx is the function.

### `make extract`

Downloads fresh data for Transarence Santé (TS) and for the RPPS

### `make load`

Loads the TS data in the Duck DB database, in the table `raw_declaration` and `raw_rpps`.

### `make transform`

Cleans the data and adapts its format.
See [this spreadhseet](https://docs.google.com/spreadsheets/d/1szJ1-EFW8FP4ZoK8BLkNrJSq-qwNCN7LSRjm0WNKOLY/edit#gid=0)
to get an overview of the format. Clean data is stored in `ts_declaration` and `rpps`.

### `make merge`

Compares the data of ts_declaration with table all_declaration. Find updates, deletions and creations and historizes
them.

### `make export`

Exports clean data to `data/clean`. Some files are downloadable on the webserver.

### `make backup`

If S3 credentials are present in the .env file, uploads to an S3 bucket the parquet files for table `all_declaration`
and `declaration_hist`, as well as a dump of the Postgres DB used by Metabase (containing users, charts, dashboards,
etc.).
These daily files are kept for 60 days, then only 3 files per months are kept (day 05, 15 and 25).

### `make process`

Is called every night on the compute instance. It is the same as calling successfully `extract`, `load`
, `transform`, `merge`, `export`. If the .env variable `BACKUP_AFTER_PROCESS` is true, also calls `backup`.

### `import_clean_data`

Is called every night on the prod server, after the process has run on the compute instance.
It loads the data from the S3 server into duckDB, cleans the Metabase cache, etc.

## Restoring from S3 backups

See backup logic [here](###`make daily_backup`).
To restore data (`all_declaration` and `declaration_hist`) from a parquet file stored on S3, you can call:

    . venv/bin/activate
    python main.py restore_from_s3 2023-08-30 -b tsv2-prod

-b or --bucket indicates the bucket from which to read (we have `tsv2-test` and `tsv2-prod`)
If omitted, the ENVIRONMENT variable from the .env file is used.
After restoring the data, running the process again

Similarly, you can restore metabase postgres dump by calling:

    python main.py restore_mb_data_from_s3 2023-08-30 -b tsv2-prod

(this requires pg_restore to be installed)

## Running the website

*Note: this will download heavy Docker images, and may take some times to build them.*

    docker compose up -d

When all services are up

- [localhost](http://localhost) serve website's homepage
- Metabase is running at [localhost/metabase/](http://localhost/metabase/)

## Admin scripts

are all in the `admin_scripts` folder and are hopefully self-explanatory. They are often made to be run from our local
computer, and one of the params is the server they have to interact with.

## A note on gitlab runners

This is more of a personnal note since I tend to always forget. On the prod server, the runner is installed at system
level (`sudo gitlab-runner register...`).
On the compute instance, the gitlab runner is also running at system level, but also running *as a service*.
This is important otherwise it is not active when the instance is unshelved, so the process would then not start.
So: `systemctl enable gitlab-runner`

# Contributing

Please contact us: contact@eurosfordocs.fr
