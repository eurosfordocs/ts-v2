import pandas as pd

from src.rpps import adeli_to_rpps, date_from_name, origin_account

from tests.conftest import load_data, read_table, type_rpps


class TestDateFromName:
    def test_without_date(self):
        inp = "Whatever"
        assert date_from_name(inp) == "CURRENT_DATE"

    def test_with_date(self):
        inp = "PS_LibreAcces_202204181148.zip"
        assert date_from_name(inp) == "'2022-04-18'"


class TestAdeliToRPPS:
    def test_adeli_no_rpps_no_change(self):
        inp = pd.DataFrame(
            {
                "nom": "nom",
                "prenom": "prenom",
                "profession": "medecin",
                "code_postal": "01000",
                "adeli": ["1", "2"],
            }
        ).pipe(type_rpps)
        load_data("rpps", inp)
        adeli_to_rpps()

        out = read_table("rpps")
        pd.testing.assert_frame_equal(out, inp)

    def test_postal_multi_adeli_single_rpps(self):
        inp = pd.DataFrame(
            {
                "nom": "nom",
                "prenom": "prenom",
                "profession": "medecin",
                "code_postal": "01000",
                "adeli": ["1", "2", None, None],
                "rpps": [None, None, "3", None],
            }
        ).pipe(type_rpps)
        load_data("rpps", inp)
        adeli_to_rpps()

        out = read_table("rpps")
        exp = inp.copy(deep=True)
        exp.loc[[0, 1, 3], "rpps"] = "3"
        pd.testing.assert_frame_equal(out, exp)

    def test_postal_multi_rpps_doesnt_change(self):
        inp = pd.DataFrame(
            {
                "nom": "nom",
                "prenom": "prenom",
                "profession": "medecin",
                "code_postal": "01000",
                "adeli": ["1", "2", None, None],
                "rpps": [None, None, "3", "4"],
            }
        ).pipe(type_rpps)
        load_data("rpps", inp)
        adeli_to_rpps()

        out = read_table("rpps")
        pd.testing.assert_frame_equal(out, inp)

    def test_profession_multi_rpps_doesnt_change(self):
        inp = pd.DataFrame(
            {
                "nom": "nom",
                "prenom": "prenom",
                "profession": "medecin",
                "code_postal": "01000",
                "adeli": ["1", "2", None, None],
                "rpps": [None, None, "3", "4"],
            }
        ).pipe(type_rpps)
        load_data("rpps", inp)
        adeli_to_rpps()

        out = read_table("rpps")
        pd.testing.assert_frame_equal(out, inp)

    def test_profession_single_rpps(self):
        inp = pd.DataFrame(
            {
                "nom": "nom",
                "prenom": "prenom",
                "profession": "medecin",
                "adeli": ["1", "2", None, "5"],
                "rpps": [None, None, "3", "3"],
            }
        ).pipe(type_rpps)
        load_data("rpps", inp)
        adeli_to_rpps()

        out = read_table("rpps")
        exp = inp.copy(deep=True)
        exp.loc[[0, 1], "rpps"] = "3"
        pd.testing.assert_frame_equal(out, exp)

    def test_variantes_adeli_with_single_rpps(self):
        inp = pd.DataFrame(
            {
                "nom": ["nom", "nom", "nom1"],
                "prenom": "prenom",
                "profession": "medecin",
                "adeli": ["1", None, "1"],
                "rpps": [None, "3", None],
            }
        ).pipe(type_rpps)
        load_data("rpps", inp)
        adeli_to_rpps()

        out = read_table("rpps")
        exp = inp.copy(deep=True)
        exp.loc[[0, 2], "rpps"] = "3"
        pd.testing.assert_frame_equal(out, exp)


class TestOriginAcccount:
    def test_origin_account_same_commune(self):
        inp = pd.DataFrame(
            {
                "nom": "nom",
                "prenom": "prenom",
                "profession": "medecin",
                "code_postal": "01000",
                "adeli": ["1", "2"],
            }
        ).pipe(type_rpps)
        load_data("rpps", inp)
        origin_account()

        out = read_table("rpps")
        exp = inp.copy(deep=True)
        exp.loc[[1], "origin_account_same_commune"] = "1"
        exp.loc[[0, 1], "origin_account"] = "1"
        pd.testing.assert_frame_equal(out, exp)

    def test_origin_account_same_commune_not_filled_if_rpps(self):
        inp = pd.DataFrame(
            {
                "nom": "nom",
                "prenom": "prenom",
                "profession": "medecin",
                "code_postal": "01000",
                "adeli": ["1", None, "2"],
                "rpps": [None, "3", None],
            }
        ).pipe(type_rpps)
        load_data("rpps", inp)
        origin_account()

        out = read_table("rpps")
        exp = inp.copy(deep=True)
        exp.loc[[0, 1, 2], "origin_account"] = ["1", "3", "2"]
        pd.testing.assert_frame_equal(out, exp)

    def test_origin_account_movings(self):
        inp = pd.DataFrame(
            {
                "nom": "nom",
                "prenom": "prenom",
                "profession": "medecin",
                "adeli": ["1", "2"],
                "rpps": ["3", None],
                "import_date_": ["2024-01-01", "2024-02-02"],
                "supr_date_": ["2024-02-02", None],
            }
        ).pipe(type_rpps)
        load_data("rpps", inp)
        origin_account()

        out = read_table("rpps")
        exp = inp.copy(deep=True)
        exp.loc[[0], "origin_account_moving"] = "2"
        exp.loc[[0, 1], "origin_account"] = "3"
        pd.testing.assert_frame_equal(out, exp)
