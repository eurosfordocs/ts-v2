import numpy as np
import pandas as pd

from src.beneficiaires import (
    BEN_SQL_DIR,
    ensure_single_ids,
    match_rpps_unique_names,
    regroup_same_name,
)
from src.settings import CONFIG
from src.utils import duck

from tests.conftest import (
    DEFAULT_RPPS,
    DEFAULT_SIREN,
    load_data,
    read_table,
    type_benex,
    type_rpps,
)


class TestRemoveIncoherentRPPSFROMACCOUNTS:
    def setup_method(self):
        load_data("rpps", DEFAULT_RPPS)
        DEFAULT_SIREN.to_parquet(CONFIG.CLEANED_DATA_DIR / "sirene.parquet")

    def test_account_without_duplicate_and_bad_score_is_not_changed(self):

        benex = pd.DataFrame(
            {
                "efd_id": [0],
                "nom": ["DURAND"],
                "prenom": "Paul",
                "code_postal": "18000",
                "rpps": "1",
                "adeli": None,
                "categorie_code": "PRS",
                "has_rpps": True,
                "profession": "infirmier",
                "siren": "9" * 9,
            }
        ).pipe(type_benex)

        load_data("beneficiaire_extended", benex)
        ensure_single_ids()

        out = read_table("beneficiaire_extended")
        pd.testing.assert_frame_equal(out, benex)

    def test_account_with_duplicate_and_bad_score_is_fully_removed(self):
        benex = pd.DataFrame(
            {
                "efd_id": 1,
                "nom": ["DURAND", "DURANT"],
                "prenom": "Paul",
                "code_postal": "18000",
                "rpps": ["1" * 11, "2" * 11],
                "adeli": None,
                "categorie_code": "PRS",
                "has_rpps": True,
                "profession": "infirmier",
                "siren": "9" * 9,
            }
        ).pipe(type_benex)

        load_data("beneficiaire_extended", benex)
        ensure_single_ids()

        out = read_table("beneficiaire_extended")
        exp = benex.copy(deep=True)
        exp.loc[[0, 1], "rpps"] = None
        pd.testing.assert_frame_equal(out, exp)

    def test_account_with_duplicate_and_bad_score_lose_worst_score(self):
        benex = pd.DataFrame(
            {
                "efd_id": 0,
                "nom": ["RPPFIRST", "FIRST"],
                "prenom": "First",
                "code_postal": "01000",
                "rpps": ["1" * 11, "2" * 11],
                "adeli": None,
                "categorie_code": "PRS",
                "has_rpps": True,
                "profession": "infirmier",
                "siren": "9" * 9,
            }
        ).pipe(type_benex)

        load_data("beneficiaire_extended", benex)
        ensure_single_ids()

        out = read_table("beneficiaire_extended")
        exp = benex.copy(deep=True)
        exp.loc[1, "rpps"] = None
        pd.testing.assert_frame_equal(out, exp)

    def test_account_with_duplicate_adeli_and_bad_score_lose_worst_score(self):
        benex = pd.DataFrame(
            {
                "efd_id": 0,
                "nom": ["RPPTHIRD", "THIRD"],
                "prenom": "Third",
                "code_postal": "03000",
                "adeli": ["3" * 9, "4" * 9],
                "rpps": None,
                "categorie_code": "PRS",
                "has_rpps": True,
                "profession": "infirmier",
                "siren": "9" * 9,
            }
        ).pipe(type_benex)

        load_data("beneficiaire_extended", benex)
        ensure_single_ids()

        out = read_table("beneficiaire_extended")
        exp = benex.copy(deep=True)
        exp.loc[1, "adeli"] = None
        pd.testing.assert_frame_equal(out, exp)

    def test_account_with_duplicate_and_same_medium_score_keep_medium(self):
        benex = pd.DataFrame(
            {
                "efd_id": 0,
                "nom": ["RPPFIRST", "FIRST", "RPPFIRS", "RPPFIRS"],
                "prenom": "First",
                "code_postal": "01000",
                "rpps": ["1" * 11, "2" * 11, "1" * 11, "1" * 11],
                "adeli": [None, None, None, "2" * 9],
                "categorie_code": "PRS",
                "has_rpps": True,
                "profession": "infirmier",
                "siren": "9" * 9,
            }
        ).pipe(type_benex)

        load_data("beneficiaire_extended", benex)
        ensure_single_ids()

        out = read_table("beneficiaire_extended")
        exp = benex.copy(deep=True)
        exp.loc[1, "rpps"] = None
        pd.testing.assert_frame_equal(out, exp)

    def test_account_with_duplicate_siren_and_same_medium_score_keep_medium(self):
        benex = pd.DataFrame(
            {
                "efd_id": 0,
                "nom": ["ENTREPFIRST", "ENTREPFIRS", "ENTREPFIRST"],
                "prenom": "First",
                "code_postal": "01000",
                "categorie_code": "ETA",
                "siren": ["1" * 9, "1" * 9, "2" * 9],
            }
        ).pipe(type_benex)

        load_data("beneficiaire_extended", benex)
        ensure_single_ids()

        out = read_table("beneficiaire_extended")
        exp = benex.copy(deep=True)
        exp.loc[2, "siren"] = None
        pd.testing.assert_frame_equal(out, exp)


class TestMatchRPPSUnique:

    def test_set_rpps_if_unique_name_profession_in_rpps(self):
        benex = pd.DataFrame(
            {
                "efd_id": 0,
                "nom": ["FIRST"],
                "prenom": "Professionnel",
                "categorie_code": "PRS",
                "nb_decla": 1,
                "has_rpps": True,
                "profession": "medecin",
            }
        ).pipe(type_benex)
        rpps = pd.DataFrame(
            {
                "nom": ["FIRST", "SECOND", "FIRST"],
                "prenom": "Professionnel",
                "meta_profession": ["medecin", "medecin", "infirmier"],
                "rpps": ["2" * 11, "1" * 11, "3" * 11],
                "origin_account": ["1" * 11, "1" * 11, "3" * 11],
            }
        ).pipe(type_rpps)
        load_data("rpps", rpps)
        load_data("beneficiaire_extended", benex)

        match_rpps_unique_names()

        out = read_table("beneficiaire_extended")
        exp = out.copy(deep=True)
        exp.loc[0, "rpps"] = "1" * 11
        pd.testing.assert_frame_equal(out, exp)

    def test_do_not_set_rpps_if_homonyme_in_rpps(self):
        benex = pd.DataFrame(
            {
                "efd_id": 0,
                "nom": ["FIRST"],
                "prenom": "Professionnel",
                "categorie_code": "PRS",
                "nb_decla": 1,
                "has_rpps": True,
                "profession": "medecin",
            }
        ).pipe(type_benex)
        rpps = pd.DataFrame(
            {
                "nom": ["FIRST", "SECOND", "FIRST"],
                "prenom": "Professionnel",
                "meta_profession": "medecin",
                "rpps": ["2" * 11, "1" * 11, "3" * 11],
                "origin_account": ["1" * 11, "1" * 11, "3" * 11],
            }
        ).pipe(type_rpps)
        load_data("rpps", rpps)
        load_data("beneficiaire_extended", benex)

        match_rpps_unique_names()

        out = read_table("beneficiaire_extended")
        pd.testing.assert_frame_equal(out, benex)

    def test_dont_set_adeli_if_multiple_name_profession_in_rpps(self):
        benex = pd.DataFrame(
            {
                "efd_id": 0,
                "nom": ["FIRST"],
                "prenom": "Professionnel",
                "categorie_code": "PRS",
                "nb_decla": 1,
                "has_rpps": True,
                "profession": "medecin",
            }
        ).pipe(type_benex)
        rpps = pd.DataFrame(
            {
                "nom": ["FIRST", "SECOND", "FIRST"],
                "prenom": "Professionnel",
                "meta_profession": "medecin",
                "adeli": ["2" * 9, "1" * 9, "3" * 9],
                "rpps": None,
                "origin_account": ["1" * 9, "1" * 9, "3" * 9],
            }
        ).pipe(type_rpps)
        load_data("rpps", rpps)
        load_data("beneficiaire_extended", benex)

        match_rpps_unique_names()

        out = read_table("beneficiaire_extended")
        pd.testing.assert_frame_equal(out, benex)

    def test_dont_fill_rpps_if_adeli_present(self):
        benex = pd.DataFrame(
            {
                "efd_id": 0,
                "nom": ["FIRST"],
                "prenom": "Professionnel",
                "categorie_code": "PRS",
                "nb_decla": 1,
                "has_rpps": True,
                "profession": "medecin",
                "rpps": "1",
            }
        ).pipe(type_benex)
        rpps = pd.DataFrame(
            {
                "nom": ["FIRST"],
                "prenom": "Professionnel",
                "meta_profession": "medecin",
                "rpps": "2" * 11,
                "origin_account": "2" * 11,
            }
        ).pipe(type_rpps)
        load_data("rpps", rpps)
        load_data("beneficiaire_extended", benex)

        match_rpps_unique_names()

        out = read_table("beneficiaire_extended")
        pd.testing.assert_frame_equal(out, benex)

    def test_set_adeli_if_unique_name_profession_in_rpps(self):
        benex = pd.DataFrame(
            {
                "efd_id": 0,
                "nom": ["FIRST"],
                "prenom": "Professionnel",
                "categorie_code": "PRS",
                "nb_decla": 1,
                "has_rpps": True,
                "profession": "medecin",
            }
        ).pipe(type_benex)
        rpps = pd.DataFrame(
            {
                "nom": ["FIRST", "SECOND", "FIRST"],
                "prenom": "Professionnel",
                "meta_profession": ["medecin", "medecin", "infirmier"],
                "adeli": ["2" * 9, "1" * 9, "3" * 9],
                "rpps": None,
                "origin_account": ["1" * 9, "1" * 9, "3" * 9],
            }
        ).pipe(type_rpps)
        load_data("rpps", rpps)
        load_data("beneficiaire_extended", benex)

        match_rpps_unique_names()

        out = read_table("beneficiaire_extended")
        exp = out.copy(deep=True)
        exp.loc[0, "adeli"] = "1" * 9
        pd.testing.assert_frame_equal(out, exp)

    def test_set_rpps_if_unique_name_commune_in_rpps(self):
        benex = pd.DataFrame(
            {
                "efd_id": 0,
                "nom": ["FIRST"],
                "prenom": "Professionnel",
                "categorie_code": "PRS",
                "nb_decla": 1,
                "has_rpps": True,
                "commune_code": "01000",
            }
        ).pipe(type_benex)
        rpps = pd.DataFrame(
            {
                "nom": ["FIRST", "SECOND", "FIRST"],
                "prenom": "Professionnel",
                "commune_code": ["01000", "02000", "02000"],
                "rpps": ["2" * 11, "1" * 11, "3" * 11],
                "origin_account": ["1" * 11, "1" * 11, "3" * 11],
            }
        ).pipe(type_rpps)
        load_data("rpps", rpps)
        load_data("beneficiaire_extended", benex)

        match_rpps_unique_names()

        out = read_table("beneficiaire_extended")
        exp = out.copy(deep=True)
        exp.loc[0, "rpps"] = "1" * 11
        pd.testing.assert_frame_equal(out, exp)

    def test_set_rpps_if_unique_names(self):
        benex = pd.DataFrame(
            {
                "efd_id": 0,
                "nom": ["FIRST"],
                "prenom": "Professionnel",
                "categorie_code": "PRS",
                "nb_decla": 1,
                "has_rpps": True,
                "commune_code": "01000",
            }
        ).pipe(type_benex)
        rpps = pd.DataFrame(
            {
                "nom": ["FIRST", "SECOND", "FIRST"],
                "prenom": "Professionnel",
                "commune_code": ["01000", "02000", "02000"],
                "adeli": ["2" * 9, "1" * 9, "2" * 9],
                "origin_account": "1" * 9,
            }
        ).pipe(type_rpps)
        load_data("rpps", rpps)
        load_data("beneficiaire_extended", benex)

        match_rpps_unique_names()

        out = read_table("beneficiaire_extended")
        exp = out.copy(deep=True)
        exp.loc[0, "adeli"] = "1" * 9
        pd.testing.assert_frame_equal(out, exp)


class TestRegroupSameName:
    def setup_method(self):
        load_data("rpps", DEFAULT_RPPS)

    def test_add_origin_account_with_same_name_profession(self):
        benex = pd.DataFrame(
            {
                "efd_id": [0, 1, 3],
                "nom": "DUPOND",
                "prenom": "Jean",
                "profession": ["medecin", "medecin", "infirmier"],
                "commune_code": "01000",
                "categorie_code": "PRS",
            }
        ).pipe(type_benex)
        load_data("beneficiaire_extended", benex)
        regroup_same_name()

        out = read_table("beneficiaire_extended")
        exp = benex.copy(deep=True)
        exp.loc[1, "same_name_commune_profession_origin"] = 0
        pd.testing.assert_frame_equal(out, exp)

    def test_add_origin_account_without_incoherences(self):
        benex = pd.DataFrame(
            {
                "efd_id": [0, 1, 3, 4],
                "nom": ["FIRST", "FIRST", "SECOND", "SECOND"],
                "siren": ["1", None, "2", "3"],
                "categorie_code": "HOS",
            }
        ).pipe(type_benex)
        load_data("beneficiaire_extended", benex)
        regroup_same_name()

        out = read_table("beneficiaire_extended")
        exp = benex.copy(deep=True)
        exp.loc[1, "same_name_no_inco_origin"] = 0
        pd.testing.assert_frame_equal(out, exp)

    def test_remove_bad_rpps_from_same_name(self):
        rpps = pd.DataFrame(
            {
                "nom": ["DUPOND", "DUPONT", "DUPDUP"],
                "prenom": "Jean",
                "meta_profession": "medecin",
                "code_postal": "01000",
                "rpps": ["1" * 11, "2" * 11, "1" * 11],
                "origin_account": "1" * 9,
            }
        ).pipe(type_rpps)
        load_data("rpps", rpps)

        benex = pd.DataFrame(
            {
                "efd_id": [0, 1],
                "nom": "DUPOND",
                "prenom": "Jean",
                "profession": "medecin",
                "code_postal": "01000",
                "rpps": ["1" * 11, "2" * 11],
                "categorie_code": "PRS",
                "has_rpps": True,
            }
        ).pipe(type_benex)
        load_data("beneficiaire_extended", benex)
        regroup_same_name()

        out = read_table("beneficiaire_extended")
        exp = benex.copy(deep=True)
        exp.loc[1, "rpps"] = None
        pd.testing.assert_frame_equal(out, exp)

    def test_remove_bad_rpps_with_composed_name(self):
        rpps = pd.DataFrame(
            {
                "nom": ["BOURDON BERNARD", "BERNARD"],
                "prenom": "Delphine",
                "meta_profession": "medecin",
                "code_postal": "01000",
                "rpps": ["1" * 11, "2" * 11],
                "origin_account": None,
            }
        ).pipe(type_rpps)
        load_data("rpps", rpps)

        benex = pd.DataFrame(
            {
                "efd_id": [0, 1],
                "nom": "BERNARD BOURDON",
                "prenom": "Delphine",
                "profession": "medecin",
                "code_postal": "01000",
                "rpps": ["1" * 11, "2" * 11],
                "categorie_code": "PRS",
                "has_rpps": True,
            }
        ).pipe(type_benex)
        load_data("beneficiaire_extended", benex)
        regroup_same_name()

        out = read_table("beneficiaire_extended")
        exp = benex.copy(deep=True)
        exp.loc[1, "rpps"] = None
        pd.testing.assert_frame_equal(out, exp)


class TestBeneficiaireInject:
    def test_with_normal_value(self):
        transform = pd.DataFrame({"efd_id": 1, "ben_profession": ["medecin", None]})
        load_data("transform_declaration", transform)

        benef = pd.DataFrame({"efd_id": [1], "profession": "infirmier"})
        load_data("beneficiaire_origin_properties", benef)

        duck.execute_script(
            BEN_SQL_DIR / "beneficiaire_inject.sql",
            parameters={"source": "ben_profession", "target": "profession"},
        )

        out = read_table("transform_declaration")
        exp = pd.DataFrame({"efd_id": [1, 1], "ben_profession": "infirmier"})
        pd.testing.assert_frame_equal(out, exp)

    def test_with_missing(self):
        transform = pd.DataFrame({"efd_id": [1, 2], "ben_profession": "medecin"})
        load_data("transform_declaration", transform)

        benef = pd.DataFrame({"efd_id": [1, 2], "profession": ["infirmier", None]})
        load_data("beneficiaire_origin_properties", benef)

        duck.execute_script(
            BEN_SQL_DIR / "beneficiaire_inject.sql",
            parameters={"source": "ben_profession", "target": "profession"},
        )

        out = read_table("transform_declaration")
        exp = pd.DataFrame({"efd_id": [1, 2], "ben_profession": ["infirmier", None]})
        pd.testing.assert_frame_equal(out, exp)


class TestPreTreatment:
    def test_autre_rpps(self):
        benex = (
            pd.DataFrame(
                [
                    ["CASTERA", "Rosen", "1"],
                    ["CLEMENT", "Alice", "2"],
                    ["CHRISTINE", "Recoules", "3"],
                    ["MARISSAL", "Noemi", "4"],
                    ["LE FILLEUL", "Alphonse", "5"],
                    ["KEDIM TOUATI", "Rabea", "6"],
                ],
                columns=["nom", "prenom", "identifiant"],
            )
            .assign(
                efd_id=lambda df: np.arange(len(df)),
                type_code="AUTRE",
                categorie_code="PRS",
            )
            .pipe(type_benex)
        )
        load_data("beneficiaire_extended", benex)

        rpps = pd.DataFrame(
            [
                ["CASTERA", "Rosen", "1"],
                ["ADEM", "Carlos", "2"],
                ["RECOULES", "Christine", "3"],
                ["MARISSAL BIDAUX", "Noemi", "4"],
                ["LEFILLEUL", "Alphonse", "5"],
                ["TOUATI KHEDIM", "Rabea", "6"],
            ],
            columns=["nom", "prenom", "rpps"],
        ).pipe(type_rpps)
        load_data("rpps", rpps)

        duck.execute_script(BEN_SQL_DIR / "rpps_from_autre_code.sql")

        out = read_table("beneficiaire_extended")
        exp = benex.copy(deep=True).assign(
            rpps=["1", None, "3", "4", "5", "6", "0" * 11]
        )
        pd.testing.assert_frame_equal(out, exp)
