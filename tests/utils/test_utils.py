from src.utils.utils import list_params_to_str


def test_list_params_to_str():
    assert list_params_to_str(["first", "second"]) == "'first','second'"
