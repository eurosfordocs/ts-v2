import pandas as pd

from src.graph_dedup import GrapDeduplicator


class TestGraphDeduplicator:
    def test_with_no_duplication(self):
        inp = pd.DataFrame({"id": [1, 2], "siren": [1, 2], "nb_siren": 1})

        g = GrapDeduplicator(id_column="id", param_columns=["siren"])

        g.run_deduplication(inp)

        exp = pd.DataFrame(columns=["id", "origin"])
        pd.testing.assert_frame_equal(g.dups, exp)

    def test_with_multiple_duplicates(self):
        inp = pd.DataFrame(
            {
                "id": [1, 2, 3],
                "siren": ["1", "1", None],
                "finess": [None, "1", "1"],
                "nb_siren": 1,
                "nb_finess": 1,
            }
        )
        g = GrapDeduplicator(id_column="id", param_columns=["siren", "finess"])

        g.run_deduplication(inp)

        exp = pd.DataFrame({"id": [2, 3], "origin": 1})
        pd.testing.assert_frame_equal(g.dups, exp)

    def test_with_incoherence_in_cluster(self):
        inp = pd.DataFrame(
            {"id": [1, 2], "siren": 1, "finess": [1, 2], "nb_siren": 1, "nb_finess": 1}
        )

        g = GrapDeduplicator(id_column="id", param_columns=["siren", "finess"])

        g.run_deduplication(inp)

        exp = pd.DataFrame(columns=["id", "origin"])
        pd.testing.assert_frame_equal(g.dups, exp)

        exp_incoherence = pd.DataFrame({"id": [1, 2], "group": 0})
        pd.testing.assert_frame_equal(g.incoherences, exp_incoherence)

    def test_with_incoherence_unflagged_in_cluster(self):
        inp = pd.DataFrame(
            {"id": [1, 2], "siren": 1, "finess": [1, 2], "nb_siren": 1, "nb_finess": 1}
        )

        g = GrapDeduplicator(
            id_column="id", param_columns=["siren", "finess"], no_incoherence=["finess"]
        )

        g.run_deduplication(inp)

        exp = pd.DataFrame({"id": [2], "origin": 1})
        pd.testing.assert_frame_equal(g.dups, exp)

        assert len(g.incoherences) == 0

    def test_with_incoherent_data_only_link(self):
        inp = pd.DataFrame({"id": [1, 2], "finess": 1, "nb_finess": 1})

        g = GrapDeduplicator(
            id_column="id", param_columns=["finess"], no_incoherence=["finess"]
        )

        g.run_deduplication(inp)

        exp = pd.DataFrame({"id": [2], "origin": 1})
        pd.testing.assert_frame_equal(g.dups, exp)

        assert len(g.incoherences) == 0

    def test_with_multiple_duplicates_and_direct_link(self):
        inp = pd.DataFrame(
            {
                "id": [1, 2, 3],
                "siren": ["1", "1", None],
                "nb_siren": 1,
                "origin_account": [None, None, 2],
            }
        )
        g = GrapDeduplicator(
            id_column="id",
            param_columns=["siren"],
            direct_link=["origin_account"],
        )

        g.run_deduplication(inp)
        exp = pd.DataFrame({"id": [2, 3], "origin": 1})
        pd.testing.assert_frame_equal(g.dups, exp)
