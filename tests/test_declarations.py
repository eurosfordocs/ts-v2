from datetime import datetime

import pandas as pd

from src.declarations import DECLA_SQL_DIR
from src.utils import duck

from tests.conftest import load_data, read_table


class TestDateEvenement:
    def test_with_standard_formats(self):
        inp = pd.DataFrame(
            {
                "information_evenement": [
                    "force_type",
                    "27-06-2019 joue les tours",
                    "2016-05-20 joue les tours",
                    "2024.07.01 - jnc sponsorship",
                    "staff-mici-limoges-19/01/20024",
                ],
                "date_transmission": datetime(2020, 1, 1),
            }
        )
        inp["date_evenement"] = [datetime(2029, 1, 1)] + [None] * (len(inp) - 1)
        load_data("transform_declaration", inp)

        duck.execute_script(DECLA_SQL_DIR / "date_evenement.sql")

        out = read_table("transform_declaration")
        exp = inp.assign(
            date_evenement=[
                datetime(2029, 1, 1),
                datetime(2019, 6, 27),
                datetime(2016, 5, 20),
                datetime(2024, 7, 1),
                None,
            ]
        )
        pd.testing.assert_frame_equal(out, exp)

    def test_with_various_separators(self):
        inp = pd.DataFrame(
            {
                "information_evenement": [
                    "force_type",
                    "27-06-2019 joue les tours",
                    "27.06.2019 joue les tours",
                    "27 06 2019 joue les tours",
                    "27/06/2019 joue les tours",
                ],
                "date_evenement": [datetime(2029, 1, 1)] + [None] * 4,
                "date_transmission": datetime(2020, 1, 1),
            }
        )
        load_data("transform_declaration", inp)

        duck.execute_script(DECLA_SQL_DIR / "date_evenement.sql")

        out = read_table("transform_declaration")
        exp = inp.assign(
            date_evenement=[
                datetime(2029, 1, 1),
                datetime(2019, 6, 27),
                datetime(2019, 6, 27),
                datetime(2019, 6, 27),
                datetime(2019, 6, 27),
            ]
        )
        pd.testing.assert_frame_equal(out, exp)

    def test_add_zeros_with_single_digits(self):
        inp = pd.DataFrame(
            {
                "information_evenement": [
                    "force_type",
                    "2-6-2020 joue les tours",
                ],
                "date_evenement": [datetime(2029, 1, 1)] + [None] * 1,
                "date_transmission": datetime(2020, 1, 1),
            }
        )
        load_data("transform_declaration", inp)

        duck.execute_script(DECLA_SQL_DIR / "date_evenement.sql")

        out = read_table("transform_declaration")
        exp = inp.assign(
            date_evenement=[datetime(2029, 1, 1)] + [datetime(2020, 6, 2)] * 1
        )
        pd.testing.assert_frame_equal(out, exp)

    def test_with_literal_months(self):
        inp = pd.DataFrame(
            {
                "information_evenement": [
                    "force_type",
                    "cancer (oct dec 2022)",
                    "12 janvier 2024",
                ],
                "date_evenement": [datetime(2029, 1, 1)] + [None] * 2,
                "date_transmission": datetime(2020, 1, 1),
            }
        )
        load_data("transform_declaration", inp)

        duck.execute_script(DECLA_SQL_DIR / "date_evenement.sql")

        out = read_table("transform_declaration")
        exp = inp.assign(
            date_evenement=[
                datetime(2029, 1, 1),
                datetime(2022, 12, 1),
                datetime(2024, 1, 12),
            ]
        )
        pd.testing.assert_frame_equal(out, exp)

    def test_with_numbers_only(self):
        inp = pd.DataFrame(
            {
                "information_evenement": [
                    "force_type",
                    "n°23022001",
                    "20160520 joue les tours",
                    "12062020 joue les tours",
                ],
                "date_evenement": [datetime(2029, 1, 1)] + [None] * 3,
                "date_transmission": datetime(2020, 1, 1),
            }
        )
        load_data("transform_declaration", inp)

        duck.execute_script(DECLA_SQL_DIR / "date_evenement.sql")

        out = read_table("transform_declaration")
        exp = inp.assign(
            date_evenement=[
                datetime(2029, 1, 1),
                None,
                datetime(2016, 5, 20),
                datetime(2020, 6, 12),
            ]
        )
        pd.testing.assert_frame_equal(out, exp)

    def test_ambiguities_use_date_transmission(self):
        inp = pd.DataFrame(
            {
                "information_evenement": [
                    "force_type",
                    "23 02 01 ad board global",
                    "23 02 01 ad board global",
                ],
                "date_transmission": [
                    datetime(2020, 1, 1),
                    datetime(2023, 1, 1),
                    datetime(2000, 1, 1),
                ],
            }
        )
        inp["date_evenement"] = [datetime(2029, 1, 1)] + [None] * (len(inp) - 1)
        load_data("transform_declaration", inp)

        duck.execute_script(DECLA_SQL_DIR / "date_evenement.sql")

        out = read_table("transform_declaration")
        exp = inp.assign(
            date_evenement=[
                datetime(2029, 1, 1),
                datetime(2023, 2, 1),
                datetime(2001, 2, 23),
            ]
        )
        pd.testing.assert_frame_equal(out, exp)
