import os
import tempfile
from pathlib import Path
from unittest.mock import patch

import pandas as pd
import pytest

from src.utils import duck

DEFAULT_RPPS = pd.DataFrame(
    {
        "rpps": ["1" * 11, "2" * 11, "3" * 11, "4" * 11],
        "adeli": ["1" * 9, "2" * 9, "3" * 9, "4" * 9],
        "nom": ["RPPFIRST", "RPPSECOND", "RPPTHIRD", "RPPFOURTH"],
        "prenom": ["First", "Second", "Third", "Fourth"],
        "code_postal": ["01000", "02000", "03000", "04000"],
        "profession": ["médecin", "Infirmier", "Infirmier", "Infirmier"],
        "origin_account": ["1" * 11, "2" * 11, "3" * 9, "4" * 9],
        "import_date_": "2024-01-01",
        "supr_date_": "2024-02-02",
    }
).assign(
    meta_profession=lambda df: df["profession"],
    commune_code=lambda df: df["code_postal"],
    origin_account_same_commune=lambda df: df["origin_account"],
    origin_account_moving=lambda df: df["origin_account"],
)

DEFAULT_SIREN = pd.DataFrame(
    {"siren": ["1" * 9, "2" * 2], "nom": ["ENTREPFIRST", "ENTREPSECOND"]}
)

BENEX_TYPED = pd.DataFrame(
    {
        "efd_id": [-1],
        "nom": ["DEFAULT"],
        "prenom": "Default",
        "code_postal": "00000",
        "commune_code": "00000",
        "rpps": "0" * 11,
        "adeli": "0" * 9,
        "categorie_code": "PRS",
        "has_rpps": True,
        "profession": "infirmier",
        "siren": "9" * 9,
        "nb_decla": 1,
        "origin_account": -1,
        "ordre": "1",
        "rna": "0",
        "same_name_commune_profession_origin": -1,
        "same_name_no_inco_origin": -1,
        "same_name_origin": -1,
    }
)


def load_data(table_name: str, frame: pd.DataFrame):
    rq = f"CREATE OR REPLACE TABLE {table_name} AS SELECT * FROM extra_table"
    duck.execute_query(rq, extra_table=frame)


def read_table(table_name: str):
    rq = f"SELECT * FROM {table_name}"
    with duck.get_con() as con:
        return con.execute(rq).df()


@pytest.fixture(autouse=True)
def testdb():
    with tempfile.TemporaryDirectory() as tmpdirname:
        with patch.dict(
            os.environ,
            {"DATA_ROOT_DIR": tmpdirname},
        ):
            tmpdirname = Path(tmpdirname)
            (tmpdirname / "clean").mkdir()
            (tmpdirname / "duckdb").mkdir()
            yield


def type_benex(frame: pd.DataFrame) -> pd.DataFrame:
    return pd.concat([frame, BENEX_TYPED], ignore_index=True)


def type_rpps(frame: pd.DataFrame) -> pd.DataFrame:
    return pd.concat([frame, DEFAULT_RPPS.head(1)], ignore_index=True)
