from datetime import datetime

import pandas as pd

from src.legacy_ts import LEGACY_SQL_DIR
from src.utils import duck

from tests.conftest import load_data, read_table


def test_remove_date_ajout_if_legacy_still_present():
    legacy = pd.DataFrame(
        {
            "id": [1, 2, 4],
            "date_ajout": datetime(2022, 4, 22),
            "date_modif": datetime(2022, 4, 22),
        }
    )
    load_data("legacy_ts", legacy)

    current = pd.DataFrame(
        {
            "id": [1, 1, 2, 3],
            "date_ajout": [datetime(2023, 1, 1), None, None, datetime(2023, 1, 1)],
            "date_modif": datetime(2023, 1, 1),
        }
    )
    load_data("all_raw_declaration", current)

    duck.execute_script(LEGACY_SQL_DIR / "update_stock_date_ajout.sql")

    out = read_table("all_raw_declaration")
    exp = pd.DataFrame(
        {
            "id": [1, 1, 2, 3],
            "date_ajout": [None, None, None, datetime(2023, 1, 1)],
            "date_modif": datetime(2023, 1, 1),
        }
    )
    pd.testing.assert_frame_equal(out, exp)


def test_unknown_legacy_are_suppressed_with_smallest_date():
    legacy = pd.DataFrame(
        {
            "id": [1, -1, 0],
            "date_suppression": [None, None, datetime(2022, 4, 22)],
            "supprime": [None, None, True],
        }
    )
    load_data("legacy_ts", legacy)

    current = pd.DataFrame(
        {
            "id": [1, 2],
            "date_modif": [datetime(2023, 1, 2), datetime(2023, 1, 1)],
        }
    )
    load_data("all_raw_declaration", current)

    duck.execute_script(LEGACY_SQL_DIR / "deleted_legacy.sql")

    out = read_table("legacy_ts")
    exp = pd.DataFrame(
        {
            "id": [1, -1, 0],
            "date_suppression": [None, datetime(2023, 1, 1), datetime(2022, 4, 22)],
            "supprime": [None, True, True],
        }
    )
    pd.testing.assert_frame_equal(out, exp)
