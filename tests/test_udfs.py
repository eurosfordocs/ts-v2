from src.utils.udfs import luhn_verify


class TestLuhnVerify:
    def test_valid_number(self):
        assert luhn_verify("758035885")

    def test_invalid_number(self):
        assert not luhn_verify("758035886")

    def test_valid_number_with_letters(self):
        assert luhn_verify("2B6021939", True)

    def test_invalid_number_with_letters(self):
        assert not luhn_verify("2A6021939", True)

    def test_invalid_alpha_if_not_letters(self):
        assert not luhn_verify("2B6021939")
