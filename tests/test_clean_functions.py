from src.clean_functions import (
    clean_nom,
    clean_prenom,
    clean_type_declaration,
    correct_latin_utf_glitches,
    remove_extra_space,
    remove_unicode_space,
)
from src.constants.miscellaneous import ADVANTAGE, CONVENTION, REMUNERATION


def test_clean_type_declaration():
    values = ["convention", "whatever", "remuneration", "avantage"]
    expected_results = [CONVENTION, None, REMUNERATION, ADVANTAGE]
    for value, expected_result in zip(values, expected_results):
        assert clean_type_declaration(value) == expected_result


class TestCleanNom:
    def test_remove_dash(self):
        assert clean_nom("Dupont-Durand") == "DUPONT DURAND"

    def test_remove_double_spaces(self):
        assert clean_nom("Dupont  Durand") == "DUPONT DURAND"


class TestCleanPrenom:
    def test_remove_dash(self):
        assert clean_prenom("Jean-Pierre") == "Jean Pierre"

    def test_remove_double_spaces(self):
        assert clean_prenom("Jean  pierre") == "Jean Pierre"


def test_remove_extra_space():
    inp = " hello  world "
    expected = "hello world"
    assert remove_extra_space(inp) == expected


def test_correct_latin_utf_glitches():
    inp = "Ã©"
    expected = "é"
    assert correct_latin_utf_glitches(inp) == expected


def test_remove_unicode_space():
    inp = "hello\u00a0world"
    expected = "hello world"
    assert remove_unicode_space(inp) == expected
