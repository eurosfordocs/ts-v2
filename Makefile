install: requirements.txt
	python3 -m venv venv
	. venv/bin/activate && pip install -r requirements.txt

activate: install
	. venv/bin/activate

process: activate
	./venv/bin/python3 main.py process

import_clean_data: activate
	./venv/bin/python3 main.py import_clean_data

shelve_compute_instance: activate
	./venv/bin/python3 main.py shelve_compute_instance

unshelve_compute_instance: activate
	./venv/bin/python3 main.py unshelve_compute_instance

extract: activate
	./venv/bin/python3 main.py extract

load: activate
	./venv/bin/python3 main.py load

transform: activate
	./venv/bin/python3 main.py transform

merge: activate
	./venv/bin/python3 main.py merge

create_views: activate
	./venv/bin/python3 main.py create_views

export: activate
	./venv/bin/python3 main.py export

backup: activate
	./venv/bin/python3 main.py backup

load_metabase_cache: activate
	./venv/bin/python3 main.py load_metabase_cache
