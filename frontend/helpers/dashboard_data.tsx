export class Filter {
    constructor(public fieldId: string,
                public fieldName: string,
                public label: string) {
    }
}

export const ALL_SLUGS = ['recherche-beneficiaire-par-nom', 'vision-par-entreprise', 'vision-densemble'];
export type DashboardSlug = typeof ALL_SLUGS[number];

export function parseSlug(maybeSlug: unknown): DashboardSlug | undefined {
    const slug = ALL_SLUGS.find((validSlug) => validSlug === maybeSlug);
    if (slug !== undefined) {
        return slug;
    }
    return undefined;
}

export class DashboardConfig {
    constructor(public slug: DashboardSlug,
                public uuid: string,
                public id: number,
                public title: string,
                public filters: Filter[]) {
    }
}

export type DashboardConfigs = Map<DashboardSlug, DashboardConfig>;

export const dashboardConfigs: DashboardConfigs = new Map();

dashboardConfigs.set('recherche-beneficiaire-par-nom',
    new DashboardConfig(
        'recherche-beneficiaire-par-nom',
        '2d57eefb-3ac1-45c1-9fc9-14989624fe30',
        279,
        'Recherche bénéficiaire par nom',
        []
    ));

dashboardConfigs.set('vision-densemble',
    new DashboardConfig(
        'vision-densemble',
        '8f544a13-2bc6-4260-91c0-643e18a35b89',
        280,
        "Vision d'ensemble",
        []
    ));


dashboardConfigs.set('vision-par-entreprise',
    new DashboardConfig(
        'vision-par-entreprise',
        'e84b28c7-eb74-4d47-89aa-5c5367595eda',
        281,
        "Vision par entreprise",
        []
    ));
