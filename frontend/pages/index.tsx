import React from "react";
import CustomMarkdown from "../components/custom_markdown";
import Jumbotron from "react-bootstrap/Jumbotron";
import Container from "react-bootstrap/Container";
import Col from "react-bootstrap/Col";
import Row from "react-bootstrap/Row";
import Seo from "../components/seo";
import Layout from "../components/layout";

const homePageMarkdown = `
<p style="text-align:center;" class="alert alert-success">
    Octobre 2023 : Nos données sont de nouveau à jour ! Détails <a href="/news">ici</a>
</p>
<p style="text-align:center;" class="alert alert-success">
    1er juin 2021 : Notre nouveau site européen est en ligne !
    <a href="https://eurosfordocs.eu/"> <strong>eurosfordocs.eu</strong></a>

</p>

Les études scientifiques et les prescriptions médicales manquent d’indépendance, influencées par une industrie discrètement omniprésente.


Depuis 2010, le scandale du Médiator a amorcé une évolution.
Les industriels doivent désormais déclarer tous leurs liens d'intérêts financiers dans la base **[Transparence-Santé](https://www.transparence.sante.gouv.fr/pages/rechercheavancee/)**.
Les déclarations sont extrêmement riches, mais le site public ne permet pas de les explorer facilement.

Au point que **ce dispositif de transparence est généralement perçu comme opaque**.

L'association **Euros For Docs** simplifie l'accès à la base Transparence-Santé, en nettoyant les données
et en offrant une interface simplifiée.

<p style="text-align:center;margin-bottom: 0px;">
<img
    src="/images/aurel_efd.jpg"
    alt="Aurel EFD"
    style="max-width:600px; width: 100%;"
/>
<br/><small>Crédit photo: Aurel pour <i>Le Monde</i></small>
</p>
<br/><br/>

Avant toute utilisation, merci de lire les <a href="/warning" >**avertissements techniques et légaux**</a>.

Les <a href="/explore" >**fonctionnalités avancées**</a> nécessitent de se connecter à la plateforme.
Avant toute publication utilisant Euros For Docs, nous vous invitons à nous contacter (contact@eurosfordocs.fr), nous donnons volontiers des conseils pour éviter une mauvaise interpretation des données de la base Transparence-Santé.

`;


function HomePage() {
    return (
        <div>
            <Seo title="Accueil"></Seo>
            <Layout>
                <Jumbotron className="ts-hero">
                    <Container>
                        <h1>Bienvenue</h1>
                        <p>
                            Pour la transparence du lobbying des industries de santé.
                        </p>
                    </Container>
                    <span className="ts-hero-caption">
                </span>
                </Jumbotron>
                <div className="container ts-markdown">
                    <Row>
                        <Col md={{offset: 2, span: 8}}>
                            <CustomMarkdown source={homePageMarkdown}/>
                        </Col>
                    </Row>
                </div>
            </Layout>
            <style jsx>{`
                :global(.ts-hero) {
                    background-color: #000;
                    background-image: linear-gradient(rgba(0, 0, 0, 0.2), rgba(0, 0, 0, 0.2)), url(/images/trust-transparency.jpg);
                    background-size: cover;
                    background-repeat: no-repeat;
                    background-position: center;
                    position: relative;
                }

                :global(.ts-hero) h1 {
                    color: white;
                    font-size: 3em;
                    text-shadow: 1px 1px 4px rgba(0,0,0,0.5);
                }

                :global(.ts-hero) p {
                    color: white;
                    font-size: 1.8em;
                    text-shadow: 1px 1px 4px rgba(0,0,0,0.5);
                    padding-top: 20px;
                    padding-bottom: 20px;
                }

                :global(.ts-hero) .ts-hero-caption {
                    position: absolute;
                    bottom: 0;
                    right: 0;
                    margin: 0 auto;
                    padding: 2px 5px;
                    color: #fff;
                    font-family: Georgia,Times,serif;
                    background: #000;
                    text-align: right;
                    z-index: 5;
                    opacity: 0.5;
                    border-radius: 4px 0 0 0;
                }

                .ts-markdown {
                    font-size: 1.2rem;
                }

                @media(max-width: 767.98px) {
                    :global(.ts-feature-row-image) {
                        margin-bottom: 30px;
                    }
                }
            `}</style>
        </div>

    );
}


export default HomePage;
