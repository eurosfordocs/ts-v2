import React from "react";
import ContentPage from "../components/content_page";

const pageText = `
## 2011 : Mediator et loi Bertrand

En 2010, à la suite du scandale du[ Mediator](https://fr.wikipedia.org/wiki/Affaire_du_Mediator), le sénat lance une mission d’information dont [le rapport](https://www.senat.fr/rap/r10-675-1/r10-675-1_mono.html) sort début 2011. Il révèle des [dysfonctionnements systémiques](https://www.senat.fr/rap/r10-675-1/r10-675-1_mono.html##toc1) dans toute la chaîne du médicament.

La corruption de l'expertise sanitaire par le lobby pharmaceutique ([Partie 1, II.B.2](https://www.senat.fr/rap/r10-675-1/r10-675-1_mono.html##toc237)) apparaît comme une cause récurrente de ces dysfonctionnements. Le rapport propose de nombreuses pistes d’évolutions législatives, dont certaines seront mises en pratique fin 2011 avec la “[loi Bertrand](https://www.legifrance.gouv.fr/loda/id/JORFTEXT000025053440)”.

Pour lutter contre les conflits d'intérêts, sa disposition phare est de rendre transparents les liens entre industriels et professionnels de santé. Désormais, les entreprises devront déclarer le détail des dons, subventions et contrats sur un site internet public.


## 2013 : Un décret vidé de sa substance

[Le décret d’application de la loi Bertrand](https://www.legifrance.gouv.fr/loda/id/JORFTEXT000027434029/?isSuggest=true) réduit considérablement le champs d’action de la loi:



* les rémunérations ne sont pas rendues publiques ;
* les bénéficiaires finaux ne sont pas indiqués ;
* l'objet des contrats est tenu secret.

L’association Formindep et le Conseil National de l’Ordre des Médecins (CNOM)  attaquent le décret devant le Conseil d'État.


## 2016 : le Conseil d’État annule le décret

Suite à ce recours, le Conseil d'État [annule](https://www.legifrance.gouv.fr/affichJuriAdmin.do?idTexte=CETATEXT000030283091) en partie ce décret. Cette décision est complétée par un [décret fin 2016](https://www.legifrance.gouv.fr/eli/decret/2016/12/28/AFSX1637582D/jo/texte) et une [ordonnance début 2017](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000033893406&categorieLien=id).

Dorénavant, les rémunérations doivent être publiées par les entreprises, qui doivent également déclarer les éventuels bénéficiaires indirects et finaux.

Début 2016, [la loi évolue également](https://www.legifrance.gouv.fr/loda/id/LEGIARTI000031916228/2016-01-28##LEGIARTI000031916228) pour permettre une libre réutilisation des données de la base Transparence-Santé, via leur publication sur le site data.gouv.fr, dans le respect de la finalité de transparence des liens d'intérêts et de la loi informatique et libertés.


## 2019 : Interdication de l'hospitalité pour les étudiants

[La loi du 24 juille 2019](https://www.legifrance.gouv.fr/jorf/article_jo/JORFARTI000038821281?r=3zctO4ENgl) interdit désormais formellement aux étudiants en formation initiale de recevoir tout type d’hospitalité (repas, frais de déplacement, frais d’hébergement ou encore frais d’inscription à un congrès) financé directement par une entreprise, ou indirectement via une association d'étudiants.

Ou plus précisément, les étudiants et associations d'étudiants, sont exclus de l'exception au principe d'interdiction ([simple non ?](https://www.legifrance.gouv.fr/codes/article_lc/LEGIARTI000038888276/)).


## 2020 : Loi “Encadrement des avantages”

À partir d’Octobre 2020, un [nouveau décret](https://www.legifrance.gouv.fr/jorf/id/JORFARTI000041999489) de la loi “Anti-cadeau” de 1993 s’applique. Il crée un nouveau régime de contrôle des avantages par Conseil National de l’Ordre des Médecins (CNOM) ou par l'Autorité Régionale de Santé (ARS).  Pour chaque catégorie de l’avantage (hospitalité, rémunération, don pour la recherche, etc.) est définie un seuil ([arrêté les définissant](https://www.legifrance.gouv.fr/loda/id/JORFTEXT000042234007/)), en dessous duquel les avantages doivent être déclarés au CNOM / à l’ARS. Au-delà de ce seuil, le CNOM / l’ARS doit donner son autorisation.

Un [décret du 30 décembre 2019](https://www.legifrance.gouv.fr/jorf/id/JORFTEXT000039698990), appliqué à partir de début janvier 2020, ajoute les influenceurs parmi la liste des catégories de  bénéficiaires.


## 2022 : refonte technique de la base et du site de consultation

Suite au scandale du Levothyrox, [Agnès Buzyn alors ministre de la santé s'était engagée en septembre 2018](https://solidarites-sante.gouv.fr/actualites/presse/discours/article/discours-d-agnes-buzyn-remise-du-rapport-sur-l-amelioration-de-l-information) à _"sécuriser, moderniser et améliorer l'accessibilité de la base Transparence Santé en adaptant ses fonctionnalités, son ergonomie et l’exploitation des données qui peut en être faite"_.

Après de nombreuses années d’attente, une refonte a lieu début 2022. Le site de déclaration pour les entreprises est amélioré, pour limiter le travail de gestion des déclarations et améliorer la qualité des données, avec notamment l’usage du référentiel RPPS pour mieux identifier les professionnels de santé.

Le site grand public de restitution est amélioré, mais totalement inabouti, avec de nombreux bugs et limites. Les [fonctionnalités d’exploration de données](https://www.transparence.sante.gouv.fr/explore/embed/dataset/declarations/analyze/?dataChart=eyJxdWVyaWVzIjpbeyJjb25maWciOnsiZGF0YXNldCI6ImRlY2xhcmF0aW9ucyIsIm9wdGlvbnMiOnt9fSwiY2hhcnRzIjpbeyJhbGlnbk1vbnRoIjp0cnVlLCJ0eXBlIjoibGluZSIsImZ1bmMiOiJTVU0iLCJ5QXhpcyI6Im1vbnRhbnQiLCJzY2llbnRpZmljRGlzcGxheSI6dHJ1ZSwiY29sb3IiOiJyYW5nZS1BY2NlbnQifV0sInhBeGlzIjoiZGF0ZV9wdWJsaWNhdGlvbiIsIm1heHBvaW50cyI6IiIsInRpbWVzY2FsZSI6InllYXIiLCJzb3J0IjoiIiwic2VyaWVzQnJlYWtkb3duIjoibGllbl9pbnRlcmV0In1dLCJkaXNwbGF5TGVnZW5kIjp0cnVlLCJhbGlnbk1vbnRoIjp0cnVlfQ%3D%3D) de la solution technique employée (OpenDataSoft) ne sont pas utilisées pour améliorer la lisibilité des informations, et ne sont nulle part mentionnées dans l’interface.

`;


function ContextPage() {
    return (
        <ContentPage title="Transparence-Santé : contexte" content={pageText} path="/context"></ContentPage>
    );
}

export default ContextPage;
