import React from "react";

import CustomMarkdown from "../components/custom_markdown";
import ListGroup from "react-bootstrap/ListGroup";
import ListGroupItem from "react-bootstrap/ListGroupItem";
import Col from "react-bootstrap/Col";
import Row from "react-bootstrap/Row";
import Seo from "../components/seo";
import Layout from "../components/layout";

enum FeatureRowOrientation {
    left, right
}

const featureRows: FeatureRowProps[] = [
    {
        image: "/images/formindep.jpg",
        imageAlt: "Formindep",
        imageOrientation: FeatureRowOrientation.left,
        text: `
**Mai 2024** : Collaboration d’Euros For Docs avec le Formindep sur les vaccins anti grippe.

Suite à la publication d’un [rapport](https://sfgg.org/espace-presse/communiques-de-presse/grippe-la-communaute-geriatrique-appelle-a-vacciner-de-toute-urgence-les-personnes-agees-de-plus-de-65-ans/) par des sociétés savantes appelant à utiliser un vaccin anti gripal spécifique, le Formindep a cherché à identifier les flux financiers entre ces sociétés et ses membres, et les principaux laboratoires produisant des vaccins.

Le Formindep nous a alors transmis une liste de 90 professionnels de santé et de 20 associations impliqués sur les vaccins pour personnes âgées.
Identifier ces personnes dans la base transparence santé a nécessité du travail d’analyse pour regrouper les variations des noms des professionnels et retirer les homonymes.
L’expertise métier du Formindep a aussi permis d’identifier différentes variations des noms des associations; par exemple lorsque celles-ci ne sont nommées que par leur sigle et pas leur nom complet.
Les équipes Euros For Docs ont alors pu regrouper de manière pérenne ces comptes dupliqués et améliorer ainsi la qualité des données des liens financiers sur le domaine de la gérontologie.

Cette collaboration a mené à un [article sur le site du Formindep](https://formindep.fr/vaccin-grippe-recommandation-geriatrie-transparence/) mettant en lumière une corrélation entre la recommandation d’utiliser un vaccin proposé par SANOFI, et le fait que ce laboratoire ait versé près de 500k€ aux associations et professionnels concernés.

L’article de Formindep, et donc le travail d’Euros For Docs, a été repris par Médiapart dans le cadre d’une [enquête plus étendue concernant SANOFI](https://www.mediapart.fr/journal/france/150524/marchandage-et-lobbying-les-dessous-de-l-annonce-du-milliard-de-sanofi).

`
    },
    {
        image: "/images/MillebornesRS.jpg",
        imageAlt: "Nouveau site",
        imageOrientation: FeatureRowOrientation.right,
        text: `
**Octobre 2023** : Mise à jour de eurosfordocs.fr

Depuis début 2022, les données de Euros For Docs n'étaient plus mises à jour (voir ci dessous).

Nous avons enfin trouvé le temps d'adapter Euros For Docs au nouveau format de Transparence Santé et sommes bien contents de pouvoir à nouveau faciliter l'accès aux données de la base.
Nous en avons aussi profité pour revoir les dashboard et améliorer la clarté du site.

Les dashboards sans connexion sont de retour, et de nouvelles fonctionnalités font leur apparition, comme l'affichage du top des bénéficiaires sur une recherche, ou la possibilité de voir les modifications d'une déclaration dans le temps.
Sous le capot, le projet a été complétement revu. Il utilise maintenant [DuckDB](https://duckdb.org/), une base de données open source, légère et très rapide. On garde [Metabase](https://www.metabase.com/) pour les dashboards, un autre précieux outil open-source.
`
    },
    {
        image: "/images/MillebornesC.jpg",
        imageAlt: "Euros For Docs cassé",
        imageOrientation: FeatureRowOrientation.left,
        text: `
**Début 2022** : Mise à jour de Transparence Santé

Début 2022, Transparence Santé connait [une évolution majeure](https://www.snitem.fr/wp-content/uploads/2022/01/Snitem-Info-224-Base-transparence.pdf).
Le changement du format des données "casse" le traitement quotidien que nous faisions. **Les données de Euros For Docs ne sont alors plus actualisées**.
Le site est toujours accessible avec les anciennes données, mais d'autres problèmes techniques ralentissent beaucoup son fonctionnement et nous conduisent à supprimer l'accès aux dashboards sans authentification préalable.

Le site public d'accès à la base Transparence Santé ([transparence.sante.gouv.fr](transparence.sante.gouv.fr)) a lui aussi fait peau neuve.

Nous avons un moment pensé pouvoir arrêter Euros For Docs - qui ne devrait pas être nécessaire si le site public était bien fait.
Malgré la mise à jour, ce n'est pas le cas. Nous avons tenté de faire des retours à la Direction Générale de la Santé, qui est responsable de la base et du site [transparence.sante.gouv.fr](transparence.sante.gouv.fr), mais n'avons pas eu de réponse.

Certains des [multiples problèmes](/limitations) identifiés au lancement de notre projet en 2018 sont toujours d'actualité dans la nouvelle version.
`
    },
    {
        image: "/images/france_europe.png",
        imageAlt: "Europe",
        imageOrientation: FeatureRowOrientation.right,
        text: `
**1er juin 2021** : [Ouverture du site européen](https://eurosfordocs.eu)

Dans la plupart des autres pays européens, les laboratoire pharmaceutiques ont mis en place d'eux-même un dispositif de transparence. Sous la façade de la transparence, ce dispositif permet surtout de **cacher** beaucoup de liens financiers dans des grandes catégories sans détails. Pour éviter que l'information libérées permettent d'y voir un peu plus clair, elle est dispersée dans des milliers de PDF, sur des milliers de sites internet.

Ce dispositif de transparence par l'industrie de ses propres pratiques est une hypocrisie, qui a en pratique tout d'une opacité organisée.
Son principal résultat est d'avoir jusqu'à présent évité l'émergence de lois ambitieuses dans les autre pays européens ou à l'échelle de l'Europe.

Le nouveau site [eurosfordocs.eu](https://eurosfordocs.eu) collecte ces informations dispersée par l'industrie dans une base unique. L'objectif est de montrer qu'un **Sunshine Act européen** est non seulement nécessaire, mais qu'il serait même techniquement très facile à mettre en oeuvre, ce que nous avons fait dans plusieurs articles scientifiques rédigés en collaboration avec des chercheurs universitaires spécialisés sur ces questions.
`
    },
    {
        image: "/images/checknews.png",
        imageAlt: "Libération",
        imageOrientation: FeatureRowOrientation.left,
        text: `
**4 janvier 2021** : 418 millions d'euros pour la presse et les médias ?

Les montants très élevés déclarés dans la catégorie *Presse et média* de Transparence-Santé circulent sur les réseaux sociaux, mettant en cause l'indépendance de la presse pour son traitement de la controverse sur l'hydroxychloroquine.

Checknews mène l'enquête, et publie au passage un article de fond présentant l'historique du projet Euros for Docs :
[Qu'est-ce que la plateforme EurosForDocs, qui recense les liens d'intérêts avec les industriels pharmaceutiques ?](https://www.liberation.fr/checknews/2021/01/04/qu-est-ce-que-la-plateforme-eurosfordocs-qui-recense-les-liens-d-interets-avec-les-industriels-pharm_1809498) ([pdf](download/2021_checknews.pdf))

`
    },
    {
        image: "/images/covid19.png",
        imageAlt: "covid-19",
        imageOrientation: FeatureRowOrientation.right,
        text: `
**Printemps 2020** : Crise sanitaire de la Covid-19.

L'influence commerciale joue à plein durant la crise.
La recherche se concentre massivement sur les solutions médicamenteuses, plutôt que sur [l'épidemiologie de terrain](https://www.mediapart.fr/journal/france/110720/donnees-epidemiologiques-la-penurie-cachee) et les interventions non médicamenteuses.
De nombreux résultats préliminaires sont diffusés via des communiqués de presse d'entreprises, faussant les perceptions du public et les réponses politiques à la pandémie.
Un exemple clé de ce problème est le Remdesivir de Gilead [[BMJ](https://www.bmj.com/content/369/bmj.m2456)].

En France, des observateurs s'appuient sur EurosForDocs pour débattre des potentiels conflits d'intérêts en jeu [Mediapart : [Conseil scientifique](https://www.mediapart.fr/journal/france/310320/covid-19-les-conseillers-du-pouvoir-face-aux-conflits-d-interets),
[Pr Raoult](https://www.mediapart.fr/journal/france/070420/chloroquine-pourquoi-le-passe-de-didier-raoult-joue-contre-lui?onglet=full)].

Promotteur d'un traitement à base d'hydroxychloroquine, le professeur Raoult fait face à des critiques sur la valeur de ses études, et à l'émergence d'un consensus scientifique en sa défaveur [Prescrire [1](https://www.prescrire.org/Fr/203/1845/58630/0/PositionDetails.aspx), [2](https://www.prescrire.org/fr/203/1845/58634/0/PositionDetails.aspx), [3](https://www.prescrire.org/fr/203/1845/58909/0/PositionDetails.aspx), [4](https://www.prescrire.org/fr/3/31/60189/0/NewsDetails.aspx)].
Afin d'échapper aux critiques sur le fond, il instrumentalise alors la question des conflits d’intérêts [[Formindep](https://formindep.fr/quelques-lecons-de-la-crise/)] pour faire taire ses détracteurs,
cherchant à montrer qu'ils sont en conflit d'intérêt avec Gilead,
ignorant les critiques émanant d'acteurs indépendants de l'industrie tels que la Revue Prescrire.

Auditionné par la commission d'enquête parlementaire, il invite à utiliser EurosForDocs, ce que feront plusieurs médias :
[Quotidien](https://drive.google.com/file/d/1gbjt9hYYg0YxPs_7INNsBdT7ApTqyPXg/view?usp=sharing),
[20 minutes](https://www.20minutes.fr/societe/2807951-20200626-coronavirus-simple-comme-chou-verifier-liens-entre-medecins-laboratoires-pharmaceutiques-comme-dit-didier-raoult),
[Sciences et Avenir](https://www.sciencesetavenir.fr/sante/enquete-les-infectiologues-francais-sont-ils-trop-proches-de-gilead_145585),
[France Info](https://www.francetvinfo.fr/sante/maladie/coronavirus/conflits-d-interets-le-conseil-scientifique-est-il-lie-aux-laboratoires-pharmaceutiques-comme-le-sous-entend-didier-raoult_4031489.html),
[AFP](https://factuel.afp.com/les-liens-linterets-entre-labos-et-medecins-le-soupcon-permanent).
`
    },
    {
        image: "/images/pqr.png",
        imageAlt: "PQR",
        imageOrientation: FeatureRowOrientation.left,
        text: `
**11 janvier 2020** : [#TransparenceCHU](https://twitter.com/search?q=transparencechu)

Les journalistes du collectif [DATA+LOCAL](https://collectif-datalocal.github.io/) utilisent EurosForDocs pour mener une enquête sur les liens dangereux entre les CHU et l'industrie.

Cette enquête est publiée simultanément dans 12 titres de la presse quotidienne régionale, puis relayée dans de nombreux journaux nationaux : [Le Monde](https://www.lemonde.fr/sante/article/2020/01/10/hopitaux-les-chu-ont-recu-170-millions-d-euros-de-laboratoires-pharmaceutiques-en-2018_6025479_1651302.html), [Une du Canard Enchaîné](/images/une_canard.png), etc.

Une [tribune collective dans le Monde](https://www.lemonde.fr/idees/article/2020/02/04/le-terreau-des-conflits-d-interets-a-l-hopital-reste-fertile_6028302_3232.html) propose ensuite 9 mesures pour améliorer la situation des conflits d'intérêts, dont la première est d'améliorer le système Transparence-Santé.
`
    }, {
        image: "/images/mediapart.png",
        imageAlt: "Logo Mediapart",
        imageOrientation: FeatureRowOrientation.right,
        text: `
**novembre 2019** : Le lobby pharmaceutique avance masqué

Une [tribune collective publiée dans l'Opinion](https://www.lopinion.fr/edition/economie/delais-d-acces-aux-traitements-medicaux-innovants-rattrapons-nos-199941) invite à accélérer la mise sur le marché des médicaments.

Les 86 médecins signataires cumulent [16 millions d'euros de liens d'intérêts déclarés depuis 2013](https://twitter.com/eurosfordocs/status/1194299087547449344?s=20), soit 31000€ par an en moyenne chacun.

[L'enquête menée par Mediapart](https://www.mediapart.fr/journal/france/121119/le-lobby-pharmaceutique-avance-masque) montre que cette tribune a été coordonnée par Agipharm, une association de 14 laboratoires pharmaceutiques américains.
`
    }, {
        image: "/images/prescrire_point.png",
        imageAlt: "Logo de Prescrire et du Point",
        imageOrientation: FeatureRowOrientation.left,
        text: `
**octobre 2019**

À l'occasion du procès du Mediator, Prescrire publie :
[La firme Servier, un financeur de poids du monde médical](/download/2019_10_Prescire_La-firme-Servier-un-financeur-de-poids-du-monde-medical.pdf) ([résumé internet](https://www.prescrire.org/fr/3/31/57696/0/NewsDetails.aspx)).

Cette étude est repris dans un éditorial du Point : [Ces acteurs de la santé qui ont bénéficié des largesses du laboratoire Servier](https://www.lepoint.fr/editos-du-point/anne-jeanblanc/ces-acteurs-de-la-sante-qui-ont-beneficie-des-largesses-du-laboratoire-servier-30-10-2019-2344437_57.php).
`
    }, {
        image: "/images/match_inter.png",
        imageAlt: "Paris Match & France Inter",
        imageOrientation: FeatureRowOrientation.right,
        text: `
**30 avril 2019**

Vaccins anti-HPV : 15 médecins dénoncent des conflits d'intérêts

En utilisant EurosForDocs, ils démontrent que les signataires d'un appel en faveur d'une généralisation des vaccins anti-HPV
ont reçu 1,6 millions d'euros de financement de la part des industriels produisant ces vaccins.

- [Site du contre-appel](https://spark.adobe.com/page/4cbZuGBONhmDC/)
- [Article de Paris Match](https://www.parismatch.com/Actu/Sante/Vaccins-anti-HPV-15-medecins-denoncent-les-risques-des-conflits-d-interets-1621133)
- [Chronique sur France Inter](https://www.franceinter.fr/emissions/sante-polemique/sante-polemique-07-mai-2019)
`
    }, {
        image: "/images/prescrire.jpg",
        imageAlt: "Logo de Prescrire",
        imageOrientation: FeatureRowOrientation.left,
        text: `
**avril 2019**

La revue médicale indépendante Prescrire rappelle
l'[utilité des bases de données de type Transparence Santé](/download/2019_04_prescrire.pdf).

Elle rappelle l'engagement de l'État à améliorer l'accessibilité de la base, et met en avant EurosForDocs.
`
    }, {
        image: "/images/mediapart_mediacites.png",
        imageAlt: "Logo Mediapart et Médiacitées",
        imageOrientation: FeatureRowOrientation.right,
        text: `
**13 mars 2019** : Des victimes d'un médicament anti-calvitie attaquent le laboratoire MSD.

**3 avril 2019** : Des victimes d'un médicament anti-pilosité attaquent le laboratoire Bayer.

Dans des articles publiés sur Mediapart
([1](https://www.mediapart.fr/journal/france/130319/les-victimes-d-un-traitement-anti-calvitie-poursuivent-le-laboratoire-msd),
[2](https://www.mediapart.fr/journal/france/030419/les-victimes-d-un-medicament-antipilosite-poursuivent-le-laboratoire-bayer))
et [Médiacités](https://www.mediacites.fr/lyon/enquete-lyon/2019/04/03/scandale-androcur-les-victimes-du-medicament-anti-pilosite-attaquent-bayer/),
la journaliste Rozenn Le Saint déchiffre les conflits d'intérêts ayant mené aux mésusages de ces médicaments.

EurosForDocs permet notamment de mesurer l'ampleur des liens d'intérêts entre
la Société française de dermatologie et le laboratoire MSD d'une part,
et Société française d’endocrinologie et le laboratoire Bayer d'autre part.
`
    }, {
        image: "/images/implant_files_le_monde.jpg",
        imageAlt: "ImplantFiles - Photo d'un TAVI",
        imageOrientation: FeatureRowOrientation.left,
        text: `
**28 novembre 2018**

Les journalistes du Monde ont utilisé EurosForDocs durant l'enquête
[« Implants médicaux : les industriels sont désormais dans le bloc opératoire »](https://www.lemonde.fr/implant-files/article/2018/11/28/implants-medicaux-les-industriels-sont-desormais-dans-le-bloc-operatoire_5389563_5385406.html).

Dans la partie « Des essais cliniques très intéressés », ils montrent comment les experts chargés d'évaluer l'intérêt du TAVI ont été personnellement financés par Medtronic.
`
    } , {
        image: "/images/pharma_papers.jpg",
        imageAlt: "PHARMA PAPERS. Lobbying et mégaprofits. Tout ce que les laboratoires pharmaceutiques voudraient vous cacher",
        imageOrientation: FeatureRowOrientation.right,
        text: `
**13 novembre 2018**

C'est notamment grâce à EurosForDocs que les journaux en ligne Basta! et l'Observatoire des multinationales publient les [« Pharma Papers »](https://www.bastamag.net/webdocs/pharmapapers/), une série d'enquêtes sur le lobbying et les mégaprofits des laboratoires pharmaceutiques.

EurosForDocs a été [rendu publique](https://www.bastamag.net/webdocs/pharmapapers/l-argent-de-l-influence/eurosfordocs-une-base-de-donnees-d-utilite-publique/)
    lors de la parution du premier volet : [« L’argent de l’influence »](https://www.bastamag.net/webdocs/pharmapapers/l-argent-de-l-influence/).

Cette enquête a fait la [Une](https://www.franceculture.fr/emissions/journal-de-8-h/journal-de-8h-du-mardi-13-novembre-2018)
    de la matinale de France Culture.
`
    }
];


interface FeatureRowProps {
    image: string;
    imageAlt: string;
    text: string;
    imageOrientation: FeatureRowOrientation;
}

function FeatureRow({image, imageAlt, text, imageOrientation}: FeatureRowProps) {
    return (
        <div style={{padding: "15px", margin: "20px 0"}}>
            <Row>
                <Col xs={{span: 12, order: 1}}
                     md={{span: 5, order: imageOrientation === FeatureRowOrientation.left ? 1 : 2}}
                     className="ts-feature-row-image">
                    <img src={image} alt={imageAlt} style={{width: '100%', height: '100%', objectFit: 'contain'}}/>
                </Col>
                <Col xs={{span: 12, order: 2}}
                     md={{span: 7, order: imageOrientation === FeatureRowOrientation.left ? 2 : 1}}>
                    <CustomMarkdown source={text}/>
                </Col>
            </Row>
        </div>
    );
}

function Impact() {
    return (
        <div>
            <Seo title="Actualités" path="/news"></Seo>
            <Layout>
                <div>
                    <div className="container ts-markdown">
                        <h2 style={{marginTop: '50px', marginBottom: '30px'}}>Actualités</h2>
                        <ListGroup>
                            {featureRows.map(row => {
                                return (
                                    <ListGroupItem key={row.image}>
                                        <FeatureRow {...row}></FeatureRow>
                                    </ListGroupItem>
                                );
                            })}
                        </ListGroup>
                    </div>
                </div>
            </Layout>
        </div>


    );
}

export default Impact;
