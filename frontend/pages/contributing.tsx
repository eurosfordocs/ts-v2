import React from "react";
import ContentPage from "../components/content_page";

const pageText = `


<i class="fab fa-angellist fa-fw" style="color:Green"></i>
Toutes vos remarques ou idées, tous vos encouragements ou contacts sont les bienvenus !

## Vous impliquer

Nous avons les besoins suivant pour enrichir et compléter l'équipe :

- <i class="fa fa-address-book fa-fw"></i>
**Développement du réseau de relation** avec les journalistes, associations de lobbying et institutions européennes

- <i class="fas fa-laptop-code fa-fw"></i>
**Traitement et analyse de données**, notre cœur de compétence qui fait notre utilité première sur le sujet

- <i class="fas fa-edit fa-fw"></i>
**Publication d'articles** de blog et scientifiques, en relation avec notre réseau d'experts et de chercheurs

- <i class="fas fa-balance-scale fa-fw"></i>
 **Compétences juridiques**, pour analyser les législations et développer notre plaidoyer

- <i class="fas fa-capsules fa-fw"></i>
Développement d'un projet sur le **versant illégal** des stratégies d'influences des industriels de santé

<i class="fas fa-envelope fa-fw" style="color:DarkBlue"></i>
Écrivez à [\`contact@eurosfordocs.fr\`](mailto:contact@eurosfordocs.fr).

Nos membres sont actuellement à Berlin, Paris, Tours et Rennes.
Nous nous réunissons régulièrement par visio-conférence.

## Soutien financier

<i class="fas fa-euro-sign fa-fw" style="color:Gold"></i>
Vous pouvez également financer le projet via ce [collectif ouvert](https://opencollective.com/eurosfordocs).
Ce système offre une comptabilité totalement transparente, en accord avec nos principes.

Le projet est bénévole. Les besoins financiers correspondent à l'hébergement du site et à l'organisation de rencontres.

Votre soutien &mdash; même symbolique &mdash; nous sera très utile, car il démontre l'intérêt citoyen du projet.
`;


function ContextPage() {
    return (
        <ContentPage title="Contribuer" content={pageText} path="/contributing"></ContentPage>
    );
}

export default ContextPage;
