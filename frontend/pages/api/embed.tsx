import {NextApiRequest, NextApiResponse} from "next";
import {sign} from "jsonwebtoken";
import * as _ from "lodash";
import {dashboardConfigs, DashboardSlug, parseSlug} from "../../helpers/dashboard_data";


export function makeIframeUrl(slug: DashboardSlug, filterValues?: any[]): string {
    const dashboardConfig = dashboardConfigs.get(slug);
    if (!dashboardConfig) {
        throw new Error('invalid dashboard slug: ' + slug);
    }

    const params: { [k: string]: any } = {};
    _.zip(dashboardConfig.filters, filterValues).forEach(([filter, filterValue]: any[]) => {
        params[filter.fieldName] = filterValue || null;
    });

    const payload = {
        resource: {dashboard: dashboardConfig.id},
        params: params,
        exp: Math.round(Date.now() / 1000) + (10 * 60) // 10 minute expiration
    };
    console.log('---> embed payload before siging:', payload);

    if (!process.env.FRONTEND_METABASE_SECRET_KEY) {
        throw new Error("env FRONTEND_METABASE_SECRET_KEY is not defined");
    }
    const token = sign(payload, process.env.FRONTEND_METABASE_SECRET_KEY);

    return `${process.env.FRONTEND_METABASE_SITE_URL}/embed/dashboard/${token}#bordered=false&titled=false`;
}


export default async (req: NextApiRequest, res: NextApiResponse) => {
    let params;
    try {
        params = JSON.parse(req.body);
    } catch(e) {
        res.status(400).json({'msg': `invalid json`});
        return;
    }
    const slug = parseSlug(params.slug);
    const filterValues = params.filterValues;

    if (!slug) {
        res.status(400).json({'msg': `invalid dashboard slug: ${params.slug}`});
        return;
    }

    if (!(filterValues instanceof Array)) {
        res.status(400).json({'msg': `invalid dashboard slug: ${params.slug}`});
        return;
    }
    console.log('--> generating iframeUrl for slug:', slug);

    res.json({iframeUrl: makeIframeUrl(slug, filterValues)});
}
