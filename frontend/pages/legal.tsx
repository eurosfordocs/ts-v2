import React from "react";
import ContentPage from "../components/content_page";

const pageText = `
## Collecte de données personnelles
EurosForDocs ne collecte pas de données personnelles lorsque vous naviguez sur le site sans connexion.

Des données personnelles sont collectées lorsque vous vous connectez à Metabase
([détails des données collectées sur cette page](/explore#vos-donn-es-personnelles)).

La collecte de ces données personnelles est nécessaire à la fourniture de notre service.
La base légale de ces traitements repose donc sur notre obligation contractuelle envers vous.

Ces données sont uniquement collectées afin de :
- Permettre la création et la gestion d'un compte utilisateur sur l'outil Metabase ;
- Suivre l'usage de la plateforme et l'améliorer ;
- Le cas échéant, pour permettre à l'équipe d'EurosForDocs de communiquer avec les utilisateurs.

Si vous nous soumettez d'autres informations personnelles, par exemple lors d'une prise de contact, elles seront traitées sur la base de votre consentement et uniquement afin de répondre à votre demande.

EurosForDocs ne partage aucune information personnelle avec des tiers, sauf en cas d'obligation légale.

Vous disposez d'un droit d'accès, de rectification, d'effacement de vos données à caractère personnel ainsi que d'un droit de limitation ou d'opposition à leur traitement.
Vous pouvez exercer ces droits en contactant EurosForDocs à l'adresse [\`contact@eurosfordocs.fr\`](mailto:contact@eurosfordocs.fr).

Vous pouvez également saisir la CNIL de toute réclamation.

## Traitement de données personnelles

La base transparence santé contient des données à caractères personnels.

Nous l'exploitons dans la finalité de transparence des liens d'intérêts,
en respect de la [licence](/download/Licence_reutilisation_donnees_transparence_sante.pdf) d'utilisation.

En cas d'erreur dans la base Transparence-Santé, vous pouvez exercer votre droit de rectification auprès des administrateurs du site.
Les corrections seront automatiquement propagées sur EurosForDocs.

Détails supplémentaires sur la page [Avertissements](/warning).

`;


function LegalPage() {
    return (
        <ContentPage title="Données personnelles" content={pageText} withToc={true} path="/legal"></ContentPage>
    );
}

export default LegalPage;
