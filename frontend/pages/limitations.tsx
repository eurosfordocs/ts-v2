import React from "react";
import ContentPage from "../components/content_page";

const pageText = `

Les problèmes de qualité des données de la base transparence santé et les limitations de l'interface officielle du Ministère de la Santé, sont la raison d’être de Euros For Docs. Nous en listons ici les points principaux, et essayerons de maintenir cette liste à jour.

## La recherche est difficile

La recherche simple du site Transparence-Santé cherche dans les noms, prénoms et villes des bénéficiaires, et dans les noms des entreprises. Ne s’affichent que 20 résultats, or pour des noms très courants (Rémi Martin), on en obtient souvent bien plus, sans beaucoup de possibilités d’affiner sa recherche pour sélectionner le bon.

La recherche avancée autorise plus de filtres, mais reste très limitée : impossible de filtrer sur plusieurs entreprises, ou plusieurs catégories. La conception de l’interface de recherche semble très loin des besoins des usagers, et ignore tout de la réalité des données : proposer de filtrer sur le champ “information événement” est par exemple une très mauvaise idée, puisque ce champ est presque toujours vide.

[ADEX](https://adex.has-sante.fr) est un outil créé par la Haute Autorité de Santé indépendemment du ministère, qui permet de chercher dans Transparence-Santé les liens d'intérêts de professionels de santé. L'outil offre une recherche plus efficace, permet d’abord de sélectionner les bénéficiaires en gérant les homonymies, puis de voir tous leurs liens d'intérêt de manière synthétique et de télécharger une synthèse au format Excel. Cet outil n'offre en revanche pas de visualisation de données, ne permet pas de chercher efficacement des bénéficiaires autres que des professionels de santé, et ne permet pas d'autres types d'analyses, comme l'étude des liens créés par les entreprises.

## Les bénéficiaires ne sont pas (ou mal) regroupés

Depuis la refonte du site lancée en 2022, les bénéficiaires ont des identifiants numériques attribués par Transparence-Santé. Malheureusement, ce regroupement apparaît particulièrement déficient : de nombreux bénéficiaires ne sont pas regroupés malgré un identifiant commun (RPPS pour les médecins, SIREN pour les organisations). À l’inverse, des bénéficiaires différents sont parfois (rarement il semble) regroupés à tort.

## Les entreprises non plus

Les entreprises peuvent avoir différents comptes, éventuellement liés entre eux, mais pas systématiquement. Si on cherche “Pfizer”, sur le site officiel, on trouve une myriade de filiales de Pfizer, qui n’ont chacune que quelques déclarations, mais il est impossible de trouver lequel de ces comptes correspond à Pfizer France.


## Pas de vision d’ensemble sur le site

Le site transparence santé ne propose que la recherche, la consultation et le téléchargement des déclarations, strictement rien de plus. On ne peut pas voir le montant total des liens d'intérêt d’un bénéficiaire ou d’une entreprise. On ne peut pas voir non plus l’évolution des sommes déclarées par année, les répartitions par catégories ou par type de bénéficiaires, etc. Dès 2016, la Cour des Comptes avait regretté un tel manquement, et proposé qu'une analyse au moins annuelle soit produite à destination du Parlement…


## Les déclarations sont de mauvaise qualité

Les déclarations transmises par les industriels sont hétérogènes et incomplètes. Les vérifications techniques sont manifestement insuffisantes. Voir à ce sujet notre page sur le nettoyage des données que nous effectuons.

En pratique, il y a des déclarations dont le montant est manifestement faux (frais d’inscription à une manifestation de plusieurs millions d’euros), des erreurs évidentes (champ nom et champ prénom ayant la même valeur pour toutes les déclarations d’une entreprise), des doublons, et de nombreuses informations manquantes, notamment énormément de conventions sans montant.

En l’état actuel de la base, il est impossible de répondre avec confiance à des questions pourtant simples, telles que: “Quelle est la somme des rémunérations et / ou avantages perçus par le praticien X?”, ou “Le nombre et valeurs des invitations à des congrès augmente-t-il d’année en année?”.


## Manque de contrôle et absence de sanctions

En cas de déclaration irrégulière, une entreprise [encourt 45 000 € d'amende](https://www.legifrance.gouv.fr/jorf/article_jo/JORFARTI000025053642). Cette somme est négligeable au vu des dépenses engagées. L'écriture de la loi rend cette condamnation impossible en pratique, car il faudrait démontrer que l'erreur est volontaire (_"… omettre **sciemment** de rendre publics…"_).

Il faudrait que ces déclarations soient certifiées par des organismes public ou d'audit indépendants, ou à grand minima contrôlées par un organisme ayant un réel pouvoir de sanction, par exemple l'interdiction de réaliser les activités mal déclarées.

À notre connaissance, il n’y a eu aucune sanction depuis la création de Transparence-Santé, et il ne semble même pas y avoir d’organisme chargé du contrôle des déclarations, capable de prendre en compte et de faire sanctionner des signalements sur des oublis manifestes.

`;


function ContextPage() {
    return (
        <ContentPage title="Transparence-Santé : limitations" content={pageText} path="/context"></ContentPage>
    );
}

export default ContextPage;
