import React from "react";
import ContentPage from "../components/content_page";

const connexionPageText = `
<p style="text-align:center;">
<img
    src="/images/alexander-andrews-636454-unsplash-resized.jpg"
    alt="Map measure photo by Alexander Andrews on Unsplash"
    style="max-width:600px; width: 100%"
/>
</p>

EurosForDocs met à disposition l'outil Metabase pour explorer simplement une version [nettoyée](/data#nettoyage-des-donn-es) de la base Transparence-Santé.
L'accès aux fonctionnalités avancées necessite de créer un compte.

<div class="ts-notes">
    <ul>
        <li><strong>Metabase</strong> est un outil d'analyse de données, développé selon un modèle économique <a href="https://fr.wikipedia.org/wiki/Freemium">freemium</a>. Nous utilisons la version gratuite et open source.</li>
    </ul>
</div>


## Fonctionnalités avancées

La création d'un compte permet d'accéder aux fonctionnalités suivantes
- **Partage ou sauvegarde d'un dashboard avec des filtres**, en copiant l'URL (exemple sur [Les associations de patients recevant des sous de Sanofi](metabase/dashboard/279-recherche-par-beneficiaire?cat%25C3%25A9gorie_de_b%25C3%25A9n%25C3%25A9ficiaire=Association%20d%27usagers%20du%20syst%C3%A8me%20de%20sant%C3%A9&entreprise=SANOFI%20AVENTIS%20FRANCE))
- Accès à **davantage de [tableaux préconfigurés](/metabase/collection/root)** selon d'autres axes d'analyse.
- Possibilité de **créer et sauvegarder ses propres analyses** et visualisations.

Pour les analyses en autonomie, nous vous invitons à lire la section expliquant le nettoyage des [données](/data), et à nous contacter si besoin de compléments.
N'hésitez pas à explorer le fonctionnement de Metabase, qui est assez intuitif.
Si besoin la [documentation est ici](https://www.metabase.com/docs/latest/getting-started.html).

## Création d'un compte

En créant un compte, vous déclarez accepter le traitement de vos informations personnelles (cf [section dédiée](#vos-donn-es-personnelles)).

La création d'un compte peut se faire :
- en écrivant à [contact@eurosfordocs.fr](mailto:contact@eurosfordocs.fr) ;
- ou directement via le \`sign-in Google\`, si vous avez une adresse email terminant par \`@gmail.com\`.

<div class="ts-notes">
    <li>Vous n'avez nullement besoin de nous donner une justification, ni même votre nom, mais nous apprécions toujours un gentil message et une explication.
    Nous créerons alors manuellement votre compte dès que possible.
    </li>
    <li>EurosForDocs et Metabase sont indépendants de Google.
    Seule la création de compte est simplifiée si vous disposez d'une adresse email terminant par <code>@gmail.com</code>.
    </li>
</div>


<p style="text-align:center;">
    <a class="btn btn-success btn-large" href="/metabase" style="padding:15px 30px; margin:10px;">
    Se connecter à Metabase
    </a>
</p>

## Vos données personnelles

Lorsque vous créez un compte Metabase
- Si vous utilisez le \`sign-in Google\`, nous accédons à votre adresse _email_, votre _nom_ et votre _prénom_.
Google est informé lors des connexions.
- Si vous nous demandez la création d'un compte, nous enregistrons votre adresse _email_,
ainsi que _votre nom et votre prénom_ si renseignés.
Votre _mot de passe_ est stocké de façon sécurisée.

Lorsque vous naviguez sur Metabase en étant connecté, l'outil enregistre votre _activité_.

_En vous connectant à Metabase, vous acceptez le traitement de ces informations personnelles
(précisions sur cette [page](/legal))._

<div class="ts-notes">
    <ul>
        <li><strong>Email :</strong> EuroForDocs est susceptible de contacter les utilisateurs enregistrés sur Metabase. EurosForDocs n'a pas mis en place de newsletter email.
        <a href="https://twitter.com/eurosfordocs">Suivez nous sur Twitter</a> pour être tenu à jour de nos travaux.
        <li><strong>Nom et prénom :</strong> Le nom et le prénom sont modifiables dans les réglages du compte utilisateur de Metabase.
        <li><strong>Activité :</strong> L'outil Metabase trace la plupart de vos actions, comme d'ailleurs la plupart des sites internet. Nous utilisons ces informations pour suivre l'utilisation du site.
        <li><strong>Mot de passe :</strong> Le mot de passe est <a href="https://fr.wikipedia.org/wiki/Salage_(cryptographie)">salé</a> et hashé par Metabase. Pour <strong>votre</strong> sécurité, utilisez des mots de passe <strong>différents</strong> sur <strog>tous</strog> les sites internet.
    </ul>
</div>

`;


function ExplorePage() {
    return (
        <ContentPage title="Utilisation de Metabase" content={connexionPageText} path="/explore"></ContentPage>
    );
}

export default ExplorePage;
