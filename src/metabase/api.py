import logging
import sys
import time
from typing import Union

import requests

from src.constants.Exceptions import TSV2Exception
from src.settings import METABASE_BASE_URL, METABASE_PASSWORD, METABASE_USERNAME
from src.utils import duck, slack

MAIN_DASHBOARDS = [279]


def metabase_get_token(host=METABASE_BASE_URL) -> str:
    payload = {"username": METABASE_USERNAME, "password": METABASE_PASSWORD}
    r = requests.post(host + "/api/session", json=payload)
    if not str(r.status_code).startswith("2"):
        raise ValueError(
            f"We could not get token. Endpoint returned with status code '{r.status_code}', with message : '{r.text[:300]}'"
        )
    return r.json()["id"]


def metabase_request(
    get: bool,
    endpoint: str,
    token: str,
    params: dict = None,
    payload: dict = None,
) -> Union[dict, set[dict]]:
    if params is None:
        params = {}

    r = None
    if get:
        r = requests.get(
            METABASE_BASE_URL + endpoint,
            headers={"X-Metabase-Session": token},
            params=params,
        )
    else:
        if payload is None:
            payload = {}
        r = requests.post(
            METABASE_BASE_URL + endpoint,
            headers={"X-Metabase-Session": token},
            params=params,
            json=payload,
        )
    if r.status_code == 524:
        # we get error 524 for some strange reason. We restart metabase.
        logging.info("Status 524, we try to close the connection and try again.")
        con = duck.get_con()
        con.close()
        time.sleep(5)
        wait_for_metabase()
        return metabase_request(get=get, endpoint=endpoint, token=token, params=params)

    if not str(r.status_code).startswith("2"):
        raise TSV2Exception(
            f"GET request on endpoint '{endpoint}' returned with non 2xx status code '{r.status_code}' "
            f"and message '{r.text[:300]}'"
        )
    return r.json()


def metabase_query_card(
    card_id: int, token: str, ignore_cache: bool = True, parameters: list = None
) -> dict:
    endpoint = f"/api/card/{card_id}/query"
    payload = {"ignore_cache": ignore_cache, "parameters": parameters or []}
    return metabase_request(get=False, endpoint=endpoint, token=token, payload=payload)


def metabase_query_cards(
    cards_id: set[int], token: str, ignore_cache: bool = True, parameters: list = None
) -> None:
    for card_id in cards_id:
        logging.debug(f"Query card {card_id}")
        try:
            result = metabase_query_card(card_id, token, ignore_cache, parameters)
            if result["status"] == "completed":
                logging.info(
                    f"Question {card_id} completed in {result['running_time']:>4}ms "
                )
            else:
                msg = f"Question {card_id:>3} failed: {METABASE_BASE_URL}/question/{card_id}"
                logging.error(msg)
                slack.send_slack_message(msg)
        except TSV2Exception as e:
            logging.error(e)
            slack.send_slack_message(str(e))


def get_dashboards_in_root_collection(token) -> list[int]:
    dashboard_list = metabase_request(get=True, endpoint="/api/dashboard/", token=token)
    for dashboard in dashboard_list:
        if dashboard["collection_id"] is None:
            yield dashboard["id"]


def get_cards_in_dashboard_list(dashboard_id_list: list[int], token) -> set[int]:
    cards = dict()
    for dashboard_id in dashboard_id_list:
        dashboard = metabase_request(
            get=True, endpoint=f"/api/dashboard/{dashboard_id}", token=token
        )
        for card_container in dashboard["ordered_cards"]:
            card = card_container["card"]
            if "id" in card:
                cards[card["id"]] = card

    return {c["id"] for c in cards.values()}


def load_metabase_cache():
    wait_for_metabase()
    token = metabase_get_token()
    cards_id = get_cards_in_dashboard_list(MAIN_DASHBOARDS, token)
    metabase_query_cards(cards_id, token, ignore_cache=False)
    logging.info("Queried cards from main dashboards")


def wait_for_metabase(max_time=300):
    logging.info("Waiting until Metabase accept connexions")
    for i in range(max_time):
        try:
            r = requests.get(METABASE_BASE_URL + "/api/health")
            if str(r.status_code).startswith("2"):
                sys.stdout.write("\n")
                sys.stdout.flush()
                return True
        except (requests.exceptions.ConnectionError, ValueError):
            pass
        sys.stdout.write(".")
        sys.stdout.flush()
        time.sleep(1)

    logging.info(f"Exceeding max waiting time of {max_time}s")
    return False
