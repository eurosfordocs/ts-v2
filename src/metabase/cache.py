import logging

from sqlalchemy import create_engine, text

from src.settings import MB_POSTGRES_HOST, MB_POSTGRES_PASSWORD, MB_POSTGRES_PORT
from src.utils.utils import TimeoutException, time_limit

MB_POSTGRES_USER = "postgres"

MB_POSTGRES_URL = f"postgresql://{MB_POSTGRES_USER}:{MB_POSTGRES_PASSWORD}@{MB_POSTGRES_HOST}:{MB_POSTGRES_PORT}"


def delete_metabase_cache():
    try:
        with time_limit(60):
            engine = create_engine(MB_POSTGRES_URL)
            with engine.connect() as connection:
                query = text("TRUNCATE TABLE public.query_cache")
                connection.execute(query)
                logging.info("truncated cache table")
    except TimeoutException:
        logging.warning("could not connect to MB Postgres, did not truncate the cache")
