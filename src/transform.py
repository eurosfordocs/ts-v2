import logging

from src import check
from src.beneficiaires import process_beneficiaire
from src.constants.columns import (
    BEN_CLEANING_FUNCTIONS,
    CLEANING_FUNCTIONS,
    ENRICHED_COLUMNS,
    RENAMING_MAP,
    STATIC_COLUMNS,
    TO_DROP,
)
from src.constants.miscellaneous import BAD_IDENTITE, BAD_PRENOM
from src.constants.tables import (
    CONVENTION,
    ENTREPRISES,
    RAW_DECLARATION,
    TRANSFORM_DECLARATION,
    TS_DECLARATION,
)
from src.declarations import process_declarations
from src.entreprises import process_entreprises
from src.finess import transform_finess
from src.format import efd
from src.rpps import transform_rpps
from src.settings import CONFIG, Environment
from src.utils import duck, slack
from src.utils.utils import frame_it, list_params_to_str


@frame_it
def transform():
    transform_finess()
    transform_rpps()
    reset_transform_work_tables()
    clean_bad_data()
    add_static_columns()
    process_entreprises()
    process_beneficiaire()
    process_declarations()
    rename_columns()
    add_enriched_columns()
    drop_columns()

    try:
        check.check()
    except AssertionError as ae:
        msg = f"Problem found during data quality checks: {str(ae)}"
        logging.error(msg)
        slack.send_slack_message(msg)
        if CONFIG.ENVIRONMENT != Environment.TEST:
            raise
    create_clean_table()
    if CONFIG.ENVIRONMENT == Environment.PROD:
        drop_transform_work_tables()
        duck.drop_table("finess_regroup_siren")


def reset_transform_work_tables():
    drop_transform_work_tables()
    duck.drop_table(ENTREPRISES)
    duck.drop_table("raw_entreprise")
    if CONFIG.ENVIRONMENT == Environment.TEST:
        duck.execute_script(CONFIG.SQL_DIR / "published_declaration.sql")
    else:
        duck.execute_query(
            f"create table {TRANSFORM_DECLARATION} as (select * from {RAW_DECLARATION})"
        )

    logging.info(f"Created {TRANSFORM_DECLARATION} from {RAW_DECLARATION}")


def drop_transform_work_tables():
    for t in [
        TRANSFORM_DECLARATION,
        CONVENTION,
        "beneficiaire_dedup_incoherences",
        "beneficiaire_dedup_origin",
        "beneficiaire_extended",
        "beneficiaire_match_multiple_rpps",
        "beneficiaire_origin_properties",
        "beneficiaire_regroup_manual",
        "entreprise_manual_correction",
        "rpps_dedup_origin",
        "rpps_incoherences",
    ]:
        duck.drop_table(t)


def rename_columns():
    for raw, transformed in RENAMING_MAP.items():
        if raw != transformed:
            duck.execute_query(
                f"ALTER TABLE {TRANSFORM_DECLARATION} RENAME COLUMN {raw} TO {transformed}"
            )
    logging.info("Renamed columns")


def drop_columns():
    for col in TO_DROP:
        duck.execute_query(f"ALTER TABLE {TRANSFORM_DECLARATION} DROP COLUMN {col}")
    logging.info("Dropped columns")


def add_static_columns():
    """
    Add new columns with a fixed value or a simple transformation of raw_declaration columns.
    """
    for column, col_type, value in STATIC_COLUMNS:
        duck.add_column(TRANSFORM_DECLARATION, column, col_type, value)
    logging.info("Added static columns")


def add_enriched_columns():
    """
    Add columns containing a (complex) transformation of existing columns.
    """
    for column, col_type, value in ENRICHED_COLUMNS:
        duck.add_column(TRANSFORM_DECLARATION, column, col_type, value)
    logging.info("Added enriched columns")


def create_clean_table():
    duck.drop_table(TS_DECLARATION)
    # querying the columns instead of * allows us to have it in the correct order.
    query = f"create table {TS_DECLARATION} as (select {efd.DECLARATION_TS_ID} from {TRANSFORM_DECLARATION})"
    duck.execute_query(query)
    logging.info("Created ts_declaration")
    for col, col_type in efd.EFT_TYPES.items():
        if col != efd.DECLARATION_TS_ID:
            query = f"alter table ts_declaration add {col} {col_type}"
            duck.execute_query(query)

            query = f"""
                update ts_declaration d set {col} = t.{col}
                from transform_declaration t
                where d.declaration_ts_id = t.declaration_ts_id"""
            duck.execute_query(query)
            logging.info(f"Create ts_declaration: added {col}")

    logging.info("Created fresh table ts_declaration")


def clean_bad_data():
    duck.clean_columns(CLEANING_FUNCTIONS)
    duck.clean_columns(BEN_CLEANING_FUNCTIONS, "transform_declaration")
    duck.execute_script(
        CONFIG.SQL_DIR / "transform_clean_bad_data.sql",
        parameters={
            "bad_identite": list_params_to_str(BAD_IDENTITE),
            "bad_prenom": list_params_to_str(BAD_PRENOM),
        },
    )
