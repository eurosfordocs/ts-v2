import json

import requests

from src.settings import SLACK_WEB_HOOK


def send_slack_message(msg):
    if SLACK_WEB_HOOK:
        webhook = f"https://hooks.slack.com/services/{SLACK_WEB_HOOK}"
        payload = {"text": msg}
        requests.post(webhook, json.dumps(payload))
