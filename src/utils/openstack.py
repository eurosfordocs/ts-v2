import logging
import time
from os import environ as env

import openstack

from src.settings import COMPUTE_INSTANCE_SERVER_ID
from src.utils.utils import frame_it

CLIENT = None

STATUS_SHELVED = "shelved_offloaded"
STATUS_ACTIVE = "active"


def get_client():
    global CLIENT
    if CLIENT is None:
        CLIENT = openstack.connect(
            auth_url=env["OS_AUTH_URL"],
            project_name=env["OS_TENANT_ID"],
            username=env["OS_USERNAME"],
            password=env["OS_PASSWORD"],
            region_name=env["OS_REGION_NAME"],
            user_domain_name="Default",
            project_domain_name="Default",
            # app_name='examples',
            app_version="3.0",
        )
        logging.info("Initialised OS client")
    return CLIENT


def wait_for_server_status():
    task_state = get_client().compute.get_server(COMPUTE_INSTANCE_SERVER_ID).task_state
    while task_state is not None:
        logging.info(f"Server is {task_state}, waiting a bit")
        time.sleep(30)
        task_state = (
            get_client().compute.get_server(COMPUTE_INSTANCE_SERVER_ID).task_state
        )
    return get_client().compute.get_server(COMPUTE_INSTANCE_SERVER_ID).vm_state


@frame_it
def shelve_compute_instance():
    if wait_for_server_status() == STATUS_ACTIVE:
        logging.info("Shelve server")
        get_client().compute.shelve_server(COMPUTE_INSTANCE_SERVER_ID)

    logging.info(f"Remote server: {wait_for_server_status()}")


@frame_it
def unshelve_compute_instance():
    if wait_for_server_status() == STATUS_SHELVED:
        logging.info("Unshelve server")
        get_client().compute.unshelve_server(COMPUTE_INSTANCE_SERVER_ID)

    logging.info(f"Remote server: {wait_for_server_status()}")
