import gzip
import logging
import os.path
import platform
import shutil
from collections.abc import Iterable
from pathlib import Path

import duckdb
import pandas as pd

from src.constants.columns import (
    BEN_CLEANING_FUNCTIONS,
    CLEANING_FUNCTIONS,
    DECLARATION_CLEANING_FUNCTIONS,
    RPPS_CLEANING_FUNCTIONS,
)
from src.constants.csv import DEFAULT_SEPARATOR
from src.constants.tables import TRANSFORM_DECLARATION
from src.format.duck_types import VARCHAR
from src.settings import CONFIG, DUCKDB_MEMORY_LIMIT, ROWS_LIMIT
from src.utils import docker
from src.utils.udfs import PythonUDF
from src.utils.utils import timeit


def get_con(
    retry=False, single_thread=False, read_only=False
) -> duckdb.DuckDBPyConnection:
    try:
        con = duckdb.connect(str(CONFIG.DUCKDB_FP), read_only=read_only)
        if DUCKDB_MEMORY_LIMIT:
            con.execute(f"SET memory_limit='{DUCKDB_MEMORY_LIMIT}';")
            logging.debug(f"Limited DuckDB memory use to {DUCKDB_MEMORY_LIMIT}")
        if single_thread:
            con.execute("SET threads to 1;")

            logging.debug("Limited DuckDB memory to 1 thread")
        return con
    except duckdb.IOException as e:
        if not retry:
            # DuckDB file is locked
            logging.error(
                f"DuckDB file {CONFIG.DUCKDB_FP} locked. Restarting metabase and retrying"
            )
            docker.restart_metabase()
            return get_con(retry=True)
        else:
            logging.error(
                f"DuckDB file {CONFIG.DUCKDB_FP} still locked after restarting metabase, failing"
            )
            raise e


def log_mem_limit():
    limit = execute_query(
        "select current_setting('memory_limit');", fetch_results="first"
    )[0]
    logging.info(f"DuckDB memory limit: {limit}")


@timeit
def import_parquet(table_name, fp, delete=False, **kwargs):
    if delete:
        execute_query(f"DROP TABLE IF EXISTS {table_name}")
    query = f"Create table {table_name} as (select * from read_parquet('{fp}') limit {ROWS_LIMIT})"
    execute_query(query, single_thread=True, **kwargs)
    logging.info(f"imported {fp} into {table_name}")


@timeit
def export_parquet(table, fp, **kwargs):
    query = f"COPY {table} TO '{fp}' (FORMAT PARQUET);"
    execute_query(query, single_thread=True, **kwargs)
    logging.info(f"exported {table} to {fp}")


@timeit
def import_csv(
    table_name,
    fp,
    auto=True,
    column_types=None,
    delim=DEFAULT_SEPARATOR,
    quote='"',
    sample_size=20480,
    header=True,
    csv_opts: dict | None = {},
    **kwargs,
):
    files_to_delete = []
    # We extract gz files (only happens for declarations.csv.gz)
    if str(fp).endswith(".gz"):
        unzipped_fp = str(fp)[:-3]
        with gzip.open(fp, "rb") as f_in, open(unzipped_fp, "wb") as f_out:
            logging.info(f"Extracting '{f_in}' > '{f_out}'")
            shutil.copyfileobj(f_in, f_out)
        fp = Path(unzipped_fp)
        files_to_delete.append(unzipped_fp)

    csv_opts = ",".join([""] + [f"{k}={v}" for k, v in csv_opts.items()])
    if auto:
        query = (
            f"create table {table_name} as "
            f"(select * from read_csv_auto('{fp}', header={header},  sample_size={sample_size}, delim='{delim}', quote='{quote}'{csv_opts}) limit {ROWS_LIMIT})"
        )
        execute_query(query, **kwargs)
    else:
        query = f"""create table {table_name} as (
                select * from
                    read_csv(
                        '{fp}',
                        header={header},
                        parallel=false,
                        columns={column_types},
                        delim='{delim}',
                        quote='{quote}'
                        {csv_opts}
                    )
                    limit {ROWS_LIMIT}
                )"""
        execute_query(query, single_thread=True, **kwargs)

    for f in files_to_delete:
        os.unlink(unzipped_fp)

    logging.info(f"imported {fp} into {table_name}")


@timeit
def generate_csv_from_parquet(parquet_fp, csv_fp):
    execute_query(f"copy (select * from '{parquet_fp}') to '{csv_fp}'")


@timeit
def export_csv(table, fp):
    query = f"COPY {table} TO '{fp}' (FORMAT CSV, DELIMITER '{DEFAULT_SEPARATOR}', HEADER );"
    execute_query(query, single_thread=True)
    logging.info(f"exported {table} to {fp}")


def drop_table(table_name, **kwargs):
    query = f"drop table if exists {table_name}"
    execute_query(query, **kwargs)
    logging.info(f"Dropped {table_name}")


def execute_queries(queries):
    """
    Execute a series of queries in the same transaction
    :param queries:
    :return:
    """
    with get_con() as con:
        con.begin()
        for query in queries:
            logging.debug(query)
            con.execute(query)
        con.commit()


def execute_query(
    query,
    parameters: dict | None = None,
    single_thread: bool = False,
    fetch_results: str | None = None,
    udfs: Iterable[PythonUDF] | None = None,
    extra_table: pd.DataFrame | None = None,
    **kwargs,
):
    log_query_description(query)
    with get_con(single_thread=single_thread, **kwargs) as con:
        _define_udfs(con, udfs)
        query = inject_params(query, parameters)
        r = con.execute(query)
        result = _query_result(r, fetch_results)
        return result


def _define_udfs(con, udfs: Iterable[PythonUDF] | None = None):
    """
    Add udf to the context fo the connexion as udf's are not persisted in the database itself.
    """
    for udf in udfs or []:
        con.create_function(
            udf.name, udf.fct, udf.input_types, udf.out_type, type=udf.exec_type
        )


def _query_result(session, fetch_option: str | None):
    assert fetch_option in [None, "first", "all"]
    result = None
    if fetch_option == "first":
        result = session.fetchone()
    elif fetch_option == "all":
        result = session.fetchall()
    return result


def execute_script(fp, **kwargs):
    logging.info(f"Executing script: {fp}")
    queries = read_queries(fp)
    for query in queries:
        execute_query(query, **kwargs)


def inject_params(query: str, parameters: dict | None = None) -> str:
    """
    Replace parameters in a query with sql injection as duckdb does not support named parameters.

    https://duckdb.org/docs/sql/query_syntax/prepared_statements.html
    """
    if "$" not in query or not parameters:
        return query
    for k, v in parameters.items():
        query = query.replace(f"${k}", str(v))
    return query


def read_queries(fp: str) -> list[str]:
    with open(fp) as f:
        script = f.read()
    return [q.strip() for q in script.split(";")]


def log_query_description(query: str):
    agg = []
    for line in query.split("\n"):
        if line.startswith("--"):
            agg.append(line[2:].strip())
        elif line:
            break
    if agg:
        logging.info(" ".join(agg))


def table_exists(table_name):
    tables = execute_query("SHOW TABLES;", fetch_results="all")
    logging.debug(f"Existing tables: {[t[0] for t in tables]}")
    return table_name in [t[0] for t in tables]


def reset():
    tables = execute_query("SHOW TABLES;", fetch_results="all")
    if tables is None:
        logging.info("No tables to delete")
        return
    tables = tables
    for table in [t[0] for t in tables]:
        drop_table(table)
    logging.info("Dropped all tables")


def clean_columns(config: dict, table_name: str = TRANSFORM_DECLARATION):
    """
    Apply normalizing or cleaning functions to free text columns.
    """
    for column in config:
        con = get_con(single_thread=platform.system() == "Darwin")
        con.create_function(
            "clean_value",
            clean_value,
            [VARCHAR, VARCHAR],
            VARCHAR,
        )
        q = f"""
        update {table_name} set {column} = map.clean
        from (
            select {column} as raw, clean_value({column}, '{column}') as clean from (
                select distinct {column} from {table_name}
            )
        ) map where {column} = map.raw
        """
        logging.debug(q)
        con.execute(q)
        con.close()
        logging.info(f"Cleaned {column}")


def clean_value(input_str: str, column: str) -> str:
    """
    Apply a cleaning function to a specific column.
    """
    dicts = (
        BEN_CLEANING_FUNCTIONS
        | CLEANING_FUNCTIONS
        | RPPS_CLEANING_FUNCTIONS
        | DECLARATION_CLEANING_FUNCTIONS
    )
    if input_str is not None:
        fct = dicts.get(column)
        if fct is None:
            logging.warning(f"Unknown column: {column}")
            return
        return fct(input_str)


def calculate_md5(table: str, hash_name: str, columns: list[str], **kwargs):
    execute_query(
        f"ALTER TABLE {table} ADD COLUMN IF NOT EXISTS {hash_name} VARCHAR", **kwargs
    )
    execute_query(
        f"update {table} set {hash_name} = " f"md5(concat({', '.join(columns)}))",
        **kwargs,
    )
    logging.info(f"Calculated hash for table {table}")


def calculate_hash(table: str, hash_name: str, columns: list[str]):
    execute_query(f"ALTER TABLE {table} ADD COLUMN IF NOT EXISTS {hash_name} UBIGINT")
    filled = [f"coalesce({x}::VARCHAR, '__NULL__')" for x in columns]
    execute_query(
        f"update {table} set {hash_name} = " f"hash(concat({', '.join(filled)}))"
    )
    logging.info(f"Calculated hash for table {table}")


def add_import_date(table: str, date: str | None = None):
    execute_query(f"ALTER TABLE {table} ADD COLUMN IF NOT EXISTS import_date_ DATE")
    execute_query(f"update {table} set import_date_ = {date}")
    logging.info(f"Calculated import_date_ for table {table}")


def add_column(table: str, column: str, col_type: str, value=None):
    execute_query(f"ALTER TABLE {table} ADD COLUMN {column} {col_type}")
    logging.info(f"Added column {column}")
    if value is not None:
        execute_query(f"UPDATE {table} SET {column} = {value}")
        logging.info(f"Set values of column {column}")


def drop_column(table: str, column: str):
    execute_query(f"ALTER TABLE {table} DROP COLUMN {column}")
    logging.info(f"Dropped column {column}")


def list_columns(table: str) -> list[str]:
    descs = execute_query(f"DESCRIBE {table}", fetch_results="all")
    return [d[0] for d in descs]
