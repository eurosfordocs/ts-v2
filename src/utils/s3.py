import datetime
import logging
import os

import boto3
import botocore
from botocore.exceptions import BotoCoreError, ClientError
from tqdm import tqdm

from src.constants.Exceptions import FileNotFoundOnS3Bucket
from src.settings import S3_ACCESS_KEY, S3_BUCKET, S3_REGION, S3_SECRET_KEY

s3 = None

# These pollute our logs if we run it on DEBUG
logging.getLogger("boto3").setLevel(logging.INFO)
logging.getLogger("botocore").setLevel(logging.INFO)
logging.getLogger("nose").setLevel(logging.INFO)
logging.getLogger("s3transfer").setLevel(logging.INFO)
logging.getLogger("urllib3").setLevel(logging.INFO)


def can_connect():
    if None not in (S3_REGION, S3_ACCESS_KEY, S3_ACCESS_KEY):
        try:
            get_client()
            return True
        except BotoCoreError:
            pass
    return False


def get_client():
    global s3
    if s3 is None:
        logging.info("S3 INIT")
        s3 = boto3.client(
            endpoint_url="https://s3.gra.cloud.ovh.net",
            region_name=S3_REGION,
            aws_access_key_id=S3_ACCESS_KEY,
            aws_secret_access_key=S3_SECRET_KEY,
            service_name="s3",
        )
    return s3


def upload_file(file_path, object_name=None, bucket=S3_BUCKET):
    """Upload a file to an S3 bucket

    :param file_path: File to upload
    :param bucket: Bucket to upload to
    :param object_name: S3 object name. If not specified then file_name is used
    :return: True if file was uploaded, else False
    """

    # If S3 object_name was not specified, use date/file_name
    if object_name is None:
        object_name = f"{datetime.date.today()}/{os.path.basename(file_path)}"

    file_size = os.path.getsize(file_path)
    client = get_client()
    # Upload the file
    try:
        with tqdm(
            total=file_size,
            desc=f"Uploading {object_name}",
            bar_format="{percentage:.1f}%|{bar:25} | {rate_fmt} | {desc}",
            unit="B",
            unit_scale=True,
            unit_divisor=1024,
        ) as pbar:
            client.upload_file(file_path, bucket, object_name, Callback=pbar.update)
    except ClientError as e:
        logging.error(e)
        return False
    return True


def download(object_name, file_path, bucket=S3_BUCKET):
    meta_data = get_client().head_object(Bucket=bucket, Key=object_name)
    total_length = int(meta_data.get("ContentLength", 0))
    client = get_client()
    with tqdm(
        total=total_length,
        desc=f"Downloading {object_name}",
        bar_format="{percentage:.1f}%|{bar:25} | {rate_fmt} | {desc}",
        unit="B",
        unit_scale=True,
        unit_divisor=1024,
    ) as pbar:
        with open(file_path, "wb") as f:
            try:
                client.download_fileobj(bucket, object_name, f, Callback=pbar.update)
            except botocore.exceptions.ClientError as e:
                if e.response["Error"]["Code"] == "404":
                    raise FileNotFoundOnS3Bucket(
                        f"Could not find file {object_name}, please check if the date is correct"
                    )


def list_objects(bucket=S3_BUCKET):
    return get_client().list_objects(Bucket=bucket)


def clean_bucket(bucket=S3_BUCKET):
    """
    We keep all files for 60 days, then 3 dates per months, the files with day number 05, 15 and 25
    :param bucket:
    :return:
    """
    for obj in list_objects()["Contents"]:
        if "/" in obj["Key"]:
            try:
                date_str = obj["Key"].split("/")[0]
                obj_date = datetime.datetime.strptime(date_str, "%Y-%m-%d").date()
                if datetime.datetime.today().date() - obj_date > datetime.timedelta(
                    days=60
                ) and not date_str.endswith("5"):
                    delete_object(obj["Key"])
                    logging.info(f"Deleted {obj['Key']} from S3 bucket")

            except ValueError:
                logging.info(
                    f"weird filename in s3 bucket, 'folder' part is not parsable as a date: {obj['Key']}"
                )


def delete_object(object_key, bucket=S3_BUCKET):
    get_client().delete_object(Key=object_key, Bucket=bucket)
