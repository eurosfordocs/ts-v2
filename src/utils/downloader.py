import gzip
import logging
import shutil
import zipfile
from datetime import datetime, timedelta

import requests
from tqdm import tqdm

from src.settings import CONFIG, CSV_CHUNK_SIZE, ROWS_LIMIT
from src.utils.utils import prefix, remove_file, timeit


class Downloader:
    def __init__(self, url, file_name, verify_ssl=True, update_days=0):
        self.url = url
        self.directory = CONFIG.RAW_DATA_DIR
        self.file_name = file_name
        self.file_path = self.directory / self.file_name
        self.verify_ssl = verify_ssl
        self.update_days = update_days

    def reset(self):
        remove_file(self.file_path)

    @timeit
    def run(self):
        if not self.file_path.exists():
            self.download_file_from_url()
        else:
            last_update = datetime.fromtimestamp(self.file_path.stat().st_mtime)
            if last_update.date() < datetime.today().date() - timedelta(
                days=self.update_days
            ):
                logging.info(
                    f"File {self.file_path} exists, but too old. Re-downloading."
                )
                self.download_file_from_url()

    def download_file_from_url(self):
        logging.info(f"Download file at url {self.url} to '{self.file_path}'")

        with requests.get(self.url, stream=True, verify=self.verify_ssl) as r:
            r.raise_for_status()
            total_size_in_bytes = int(r.headers.get("content-length", 0))
            progress_bar = tqdm(total=total_size_in_bytes, unit="B", unit_scale=True)

            with open(self.file_path, "wb") as f:
                for chunk in r.iter_content(chunk_size=CSV_CHUNK_SIZE):
                    progress_bar.update(len(chunk))
                    f.write(chunk)
            progress_bar.close()


class Extractor:
    def __init__(self, zip_archive_name, file_name):
        self.directory = CONFIG.RAW_DATA_DIR
        self.zip_archive_name = zip_archive_name
        self.zip_path = self.directory / self.zip_archive_name

        self.file_name = file_name
        self.file_path = self.directory / self.file_name

    @property
    def zf(self):
        return zipfile.ZipFile(self.zip_path)

    def reset(self):
        remove_file(self.file_path)

    @timeit
    def run(self):
        logging.debug(f"Extracting file {self.file_name} from {self.zip_path}")
        # we extract if the unzipped file doesn't exist or is older than the zip file
        if (
            not self.file_path.exists()
            or self.zip_path.stat().st_mtime > self.file_path.stat().st_mtime
        ):
            self.extract_file()

    def extract_file(self):
        zip_file_name = self.get_zip_file_matching_prefix()
        logging.info(f"Extracting '{self.zip_path}' > '{self.file_path}'")

        with self.zf.open(zip_file_name) as input_file, open(
            self.file_path, "wb"
        ) as output_file:
            for i, line in enumerate(input_file):
                if i > ROWS_LIMIT:
                    return
                output_file.write(line)

    def get_zip_file_matching_prefix(self):
        file_prefix = prefix(self.file_name)
        for zip_file_name in self.zf.namelist():
            if zip_file_name.startswith(file_prefix):
                return zip_file_name
        raise FileNotFoundError(
            f"File with prefix '{file_prefix}' was not found in zip archive."
        )


class Gz_extractor(Extractor):
    def extract_file(self):
        with gzip.open(self.zip_path, "rb") as f_in, open(
            self.file_path, "wb"
        ) as f_out:
            logging.info(f"Extracting '{self.zip_path}' > '{self.file_path}'")
            shutil.copyfileobj(f_in, f_out)
