from collections.abc import Iterable
from typing import Any, Callable

from attr import dataclass

from src.clean_functions import remove_unicode_space
from src.format.duck_types import BOOLEAN, VARCHAR


@dataclass(frozen=True)
class PythonUDF:
    name: str
    fct: Callable
    input_types: Iterable
    out_type: Any
    exec_type: str


def luhn_verify(value, letters: bool = False):
    """
    Check the validity of a string wit respect to the luhn algorithm.
    It would seem that for the adeli code, a letter is replaced by its position in the alphabet.
    """
    value = str(value).lower()

    if (not value.isdigit() and not letters) or (not value.isalnum() and letters):
        return False

    if letters and value.isalnum():
        value = "".join(
            char if char.isdigit() else str(ord(char) - ord("a") + 1) for char in value
        )
    digits = list(map(int, value))
    oddSum = sum(digits[-1::-2])
    evnSum = sum([sum(divmod(2 * d, 10)) for d in digits[-2::-2]])
    return (oddSum + evnSum) % 10 == 0


LUHN_UDF = PythonUDF("luhn_verify", luhn_verify, [VARCHAR, BOOLEAN], BOOLEAN, "native")
REMOVE_UNICODE_SPACE_UDF = PythonUDF(
    "remove_unicode_space", remove_unicode_space, [VARCHAR], VARCHAR, "native"
)
