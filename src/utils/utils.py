import logging
import os
import signal
import time
from collections.abc import Iterable
from contextlib import contextmanager

from src.utils import slack


class TimeoutException(Exception):
    pass


@contextmanager
def time_limit(seconds):
    def signal_handler(signum, frame):
        raise TimeoutException("Timed out!")

    signal.signal(signal.SIGALRM, signal_handler)
    signal.alarm(seconds)
    try:
        yield
    finally:
        signal.alarm(0)


def remove_file(file_path):
    if os.path.exists(file_path):
        logging.info(f"Removing file at '{file_path}'")
        os.remove(file_path)
        logging.debug(f"- file '{file_path}' removed")


def prefix(file_name):
    return os.path.splitext(file_name)[0]


def timeit(function_to_time):
    def timed(*args, **kwargs):
        start_time = time.time()
        result = function_to_time(*args, **kwargs)
        end_time = time.time()
        msg = f"Function '{function_to_time.__name__}{args}' took {end_time - start_time:.2f} seconds"
        logging.info(msg)
        return result

    return timed


def frame_it(function):
    def framed(*args, **kwargs):
        start_time = time.time()
        result = function(*args, **kwargs)
        end_time = time.time()
        minutes, seconds = divmod(round(end_time - start_time), 60)
        msg = f"{function.__name__} took {minutes} min {seconds}"
        slack.send_slack_message(msg)
        logging.info(f"╔{'':═^90}╗")
        logging.info(f"║{msg:^90}║ ")
        logging.info(f"╚{'':═^90}╝")
        return result

    return framed


# Developer interaction
def get_confirmation(question, positive_answer="I confirm", negative_answer="No"):
    valid = {positive_answer.lower(): True, negative_answer.lower(): False}
    answer_information = (
        f"Please answer with '{positive_answer}' or '{negative_answer}'."
    )
    print(question)
    while True:
        print(answer_information)
        answer = input()
        if answer.lower() in valid:
            if valid[answer.lower()]:
                return
            else:
                print("Aborting")
                exit(0)
        else:
            print(f"Your answer was '{answer}'")


def list_params_to_str(parameters: Iterable):
    return ",".join(map(lambda x: f"'{x}'", parameters))
