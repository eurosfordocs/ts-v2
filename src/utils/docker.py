import logging
import subprocess

from src.settings import DOCKER_METABASE


def stop_docker(docker_name):
    logging.info(f"restarting {docker_name}")
    cmd = f"docker compose stop {docker_name}"
    out = subprocess.run(cmd, shell=True)
    logging.info(out)


def restart_docker(docker_name):
    logging.info(f"restarting {docker_name}")
    cmd = f"docker compose restart {docker_name}"
    out = subprocess.run(cmd, shell=True)
    logging.info(out)


def restart_metabase():
    restart_docker(DOCKER_METABASE)


def count_oom_metabase():
    cmd = "docker events --since '24h' --until '0s' --filter 'image=ts-v2-metabase' --filter 'event=oom'"
    res = subprocess.run(cmd, shell=True, capture_output=True, text=True)
    return len(res.stdout.splitlines())
