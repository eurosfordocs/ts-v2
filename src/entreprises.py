import logging

from src.constants.csv import REGROUPEMENT_ENTREPRISES_MANUEL_URL
from src.settings import CONFIG
from src.utils import duck

ENT_SQL_DIR = CONFIG.SQL_DIR / "entreprise"


def process_entreprises():
    import_manual_corrections()
    create_entreprises("entreprise")
    create_entreprises("raw_entreprise")
    correct_siren()
    entreprise_francaise_si_siren()
    regroup_entreprises()
    correct_siren_duplicates()
    fill_empty_mere_info()
    change_mere_id_to_grand_parent_id()
    update_transform_declaration()
    quality_checks()


def import_manual_corrections():
    rq = f"""
    CREATE OR REPLACE TABLE entreprise_manual_correction AS
    SELECT * FROM READ_CSV_AUTO('{REGROUPEMENT_ENTREPRISES_MANUEL_URL}')
    """
    duck.execute_query(rq)


def create_entreprises(entreprise_name):
    duck.execute_script(
        ENT_SQL_DIR / "create_entreprises.sql",
        parameters={
            "entreprise_name": entreprise_name,
        },
    )


def regroup_entreprises():
    """
    Merge multiple accounts of the same companies into a single one.

    Some companies use multiple accounts to declare their conventions.
    Even though the system for mother company and branches exists, it is not necessarily used properly.
    A manual matching has been made for part of the companies and is available at :
    https://docs.google.com/spreadsheets/d/1uzxY1M2xQpuZL_1XrZT3xlg0aaP9H2HX5crK15YsOX0/edit#gid=1789677758
    """
    query = """
    UPDATE entreprise t
    SET mere_id = re.manual_mere_id
    FROM entreprise_manual_correction re
    WHERE t.entreprise_id = re.entreprise_id AND re.manual_mere_id IS NOT NULL
    """
    res = duck.execute_query(query, fetch_results="first")
    logging.info(f"Regroupement entreprise: {res[0]} rows updated")
    drop_circular_mere()


def update_transform_declaration():
    duck.execute_script(ENT_SQL_DIR / "entreprises_inject.sql")


def correct_siren_duplicates():
    """
    Correct or define a mere_id for collections of companies with same siren.
    """
    duck.execute_script(ENT_SQL_DIR / "remove_siren_duplicates.sql")


def fill_empty_mere_info():
    """
    Fill mere info from the smallest filiale.
    """
    duck.execute_script(ENT_SQL_DIR / "entreprise_fill_mere_info.sql")


def drop_circular_mere():
    """
    Set a null mere_id where it is equal to entreprise_id
    """
    query = """
    update entreprise
    set mere_id = null
    where mere_id = entreprise_id
    """
    rows = duck.execute_query(query, fetch_results="first")[0]
    logging.info(f"{rows} circular mere have been dropped")


def change_mere_id_to_grand_parent_id():
    """
    Set the mere_id of a company to its mother's mother if applicable.
    """
    fn = ENT_SQL_DIR / "mere_id_to_grand_parent_id.sql"
    with fn.open() as f:
        query = f.read()
        rows = duck.execute_query(query, fetch_results="first")[0]
    logging.info(f"{rows} mere_id have been moved to grand parent")


def entreprise_francaise_si_siren():
    """
    Ajoute le pays FR si présence de siren
    """
    query = """
    update entreprise
    set pays = 'FR'
    where siren not null
    and pays is null
    """
    rows = duck.execute_query(query, fetch_results="first")[0]
    logging.info(f"{rows} siren have been localized to France")


def correct_siren():
    """
    A couple of siren have a suffix which should be removed.
    """
    query = """
    update entreprise
    set siren = left(siren, 9)
    where siren like '%-%'
    """
    rows = duck.execute_query(query, fetch_results="first")[0]
    logging.info(f"{rows} siren have been corrected")


def quality_checks():
    with open(ENT_SQL_DIR / "entreprise_in_decla_have_account.sql") as f:
        rq = f.read()
        rows = duck.execute_query(rq, fetch_results="first")[0]
        assert not rows, f"Entreprise id without account : {rows}"
