import logging

from src.constants.columns import DECLARATION_CLEANING_FUNCTIONS
from src.constants.csv import CATEGORISATION_MOTIFS_URL, META_CONVENTION_MOTIF_URL
from src.constants.miscellaneous import (
    MOTIF_REGEX,
    MOTIFS_KEYWORDS,
    NULL_AUTRE_MOTIF,
    NULL_INFORMATION_EVENEMENT,
)
from src.settings import CONFIG
from src.utils import duck
from src.utils.utils import list_params_to_str

DECLA_SQL_DIR = CONFIG.SQL_DIR / "declaration"


def process_declarations():
    duck.clean_columns(DECLARATION_CLEANING_FUNCTIONS)
    clean_declaration_info()
    join_conventions()
    convention_aggregates()
    fill_meta_motif()


def clean_declaration_info():
    duck.execute_script(
        DECLA_SQL_DIR / "clean_infos.sql",
        parameters={
            "null_autre_motifs": list_params_to_str(NULL_AUTRE_MOTIF),
            "null_information_evenement": list_params_to_str(
                NULL_INFORMATION_EVENEMENT
            ),
            "null_autre_motif": list_params_to_str(NULL_AUTRE_MOTIF),
        },
    )
    motif_per_regex()
    similarity_motif_matching()
    manual_autre_motif()
    duck.execute_script(DECLA_SQL_DIR / "date_evenement.sql")


def motif_per_regex():
    patterns = MOTIF_REGEX | {
        r"(^|.*[ ,/\[\(:+-])(" + "|".join(v) + r")($|[\], /\-)\+].*)": k
        for k, v in MOTIFS_KEYWORDS.items()
    }
    for pattern, motif in patterns.items():
        duck.execute_script(
            DECLA_SQL_DIR / "motif_per_regex.sql",
            parameters={"motif": motif, "pattern": pattern},
        )
        logging.info(f"Run pattern {pattern} for motif {motif}")


def manual_autre_motif():
    with open(DECLA_SQL_DIR / "list_obsolete_motifs.sql") as f:
        obsoletes = duck.execute_query(
            f.read(),
            parameters={
                "categorisation_motifs_url": CATEGORISATION_MOTIFS_URL,
            },
            fetch_results="all",
        )
        obsoletes = [x[0] for x in obsoletes]
        logging.info(f"Obsolete manual autre motif : {', '.join(obsoletes)}")
    duck.execute_script(
        DECLA_SQL_DIR / "manual_autre_motif.sql",
        parameters={
            "categorisation_motifs_url": CATEGORISATION_MOTIFS_URL,
        },
    )


def similarity_motif_matching():
    with open(DECLA_SQL_DIR / "similarity_autre_official.sql") as f:
        query = f.read()
        rows = duck.execute_query(query, fetch_results="first")
        logging.info(f"{rows} autre_motif automatically attributed by similarity")


def join_conventions():
    """
    - Join advantages and remunerations to the corresponding convention.

    - Add the convention descriptor the corresponding remunerations.
    """
    duck.execute_script(DECLA_SQL_DIR / "link_conventions.sql")


def convention_aggregates():
    """
    - Create an aggregate table of the conventions and their properties.
    - Add those agregates to each corresponding convention in the transform table.
    """
    duck.execute_script(DECLA_SQL_DIR / "convention_agg.sql")


def fill_meta_motif():
    duck.execute_script(
        DECLA_SQL_DIR / "meta_motif.sql",
        parameters={"meta_motif_match_url": META_CONVENTION_MOTIF_URL},
    )
