import logging
from collections import Counter
from collections.abc import Iterable

import igraph as ig
import numpy as np
import pandas as pd


class GrapDeduplicator:
    def __init__(
        self,
        id_column: str,
        param_columns: Iterable[str],
        no_incoherence: Iterable[str] | None = None,
        direct_link: Iterable[str] | None = None,
    ):
        self.id_column = id_column
        self.param_columns = param_columns
        self.no_incoherence = no_incoherence or []
        self.direct_link = direct_link or []

    def run_deduplication(self, df: pd.DataFrame):
        self.create_graph(df)
        self.find_duplicates()
        self.format_clusters()
        self.format_incoherences()
        return self

    def create_graph(self, df: pd.DataFrame):
        """
        Create a graph linking all beneficiaire with vertices representing their identifiers.
        """
        self.graph = ig.Graph()
        self.add_vertices(df)
        df = self.add_vertex_id_to_identifiers(df)
        self.add_edges(df)
        logging.info(
            f"dedup graph {len(self.graph.vs)} vertices; {len(self.graph.es)} edges."
        )

    def add_vertices(self, df: pd.DataFrame):
        for categ in [self.id_column] + self.param_columns:
            vertices = df[categ].dropna().drop_duplicates().to_numpy()
            logging.info(f"deduplication nodes : {categ} {len(vertices)}")
            self.graph.add_vertices(
                len(vertices), attributes={"categ": categ, "value": vertices}
            )

    def add_vertex_id_to_identifiers(self, df: pd.DataFrame) -> pd.DataFrame:
        matching = pd.DataFrame(
            {
                "categ": self.graph.vs["categ"],
                "value": self.graph.vs["value"],
                "id_vertex": np.arange(len(self.graph.vs)),
            }
        )

        edges_types = [self.id_column] + self.edge_types()
        for categ in edges_types:
            to_compare = self.id_column if categ in self.direct_link else categ
            df = (
                pd.merge(
                    df,
                    matching[matching["categ"] == to_compare],
                    left_on=categ,
                    right_on="value",
                    how="left",
                )
                .drop(columns=["categ", "value"])
                .rename(columns={"id_vertex": f"id_vertex_{categ}"})
            )
        return df

    def edge_types(self):
        return self.param_columns + self.direct_link

    def add_edges(self, df: pd.DataFrame):
        for categ in self.edge_types():
            edges = (
                df[[f"id_vertex_{self.id_column}", f"id_vertex_{categ}"]]
                .dropna()
                .astype(int)
                .drop_duplicates()
                .to_numpy()
            )
            self.graph.add_edges(edges)
            logging.info(f"dedup edges : {categ} {len(edges)}")

    def find_duplicates(self):
        """
        Create vertices cluster and categorify into proper duplicates and bad data.
        """
        clusters = self.graph.clusters()
        self.dups = []
        self.incoherences = []
        for clt in clusters:
            self.qualify_cluster(clt)

    def qualify_cluster(self, clt):
        """
        Send the cluster to dups or incoherences depending on its properties.
        """
        vtx = self.graph.vs[clt]
        is_coherent = self._is_cluster_coherent(vtx)
        if is_coherent == 0:
            return

        id_benef = sorted(
            [
                val
                for val, categ in zip(vtx["value"], vtx["categ"])
                if categ == self.id_column
            ]
        )
        if is_coherent == -1:
            self.incoherences.append(id_benef)
        else:
            self.dups.append(id_benef)

    def _is_cluster_coherent(self, vtx) -> int:
        """
        Check wether all id vertices share at maximum one value for each parameter.
        Return :
        - 1 if the cluster contains duplicates
        - 0 if the cluster has a single beneficiaire
        - -1 if the cluster has multiple beneficiaires with incoherent identifiers
        """
        cnt = Counter(vtx["categ"])
        if cnt[self.id_column] <= 1:
            return 0

        for col in [self.id_column] + self.no_incoherence:
            del cnt[col]

        if not len(cnt):
            return 1

        _, max_diversity = cnt.most_common(1)[0]
        return 2 * (max_diversity == 1) - 1

    def format_clusters(self):
        """
        Transform dups into a DataFrame matching each duplicate beneficiaire with an origin account.
        """
        if not len(self.dups):
            self.dups = pd.DataFrame(columns=[self.id_column, "origin"])
        else:
            self.dups = pd.concat(
                [
                    pd.DataFrame({self.id_column: clt[1:], "origin": clt[0]})
                    for clt in self.dups
                ],
                axis=0,
            )

    def format_incoherences(self):
        """
        Transform incoherences into a DataFrame matching each duplicate beneficiaire with an origin account.
        """
        if not len(self.incoherences):
            self.incoherences = pd.DataFrame(columns=[self.id_column, "group"])
        else:
            self.incoherences = pd.concat(
                [
                    pd.DataFrame({self.id_column: clt, "group": i})
                    for i, clt in enumerate(self.incoherences)
                ],
                axis=0,
            )
