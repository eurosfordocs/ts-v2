import logging
import os
from logging.handlers import TimedRotatingFileHandler

from dotenv import load_dotenv

from src.settings import CONFIG

load_dotenv()


class ColorFormater(logging.Formatter):
    grey = "\x1b[38;20m"
    yellow = "\x1b[33;20m"
    red = "\x1b[31;20m"
    bold_red = "\x1b[31;1m"
    reset = "\x1b[0m"
    format = "%(asctime)s | %(levelname)s | %(message)s"

    FORMATS = {
        logging.DEBUG: grey + format + reset,
        logging.INFO: grey + format + reset,
        logging.WARNING: yellow + format + reset,
        logging.ERROR: red + format + reset,
        logging.CRITICAL: bold_red + format + reset,
    }

    def format(self, record):
        log_fmt = self.FORMATS.get(record.levelno)
        formatter = logging.Formatter(fmt=log_fmt, datefmt="%Y-%m-%d %H:%M:%S")
        return formatter.format(record)


LOGGING_LEVEL = os.getenv("LOGGING_LEVEL", "INFO")

logging.basicConfig(
    level=LOGGING_LEVEL,
    format="%(asctime)s :: %(levelname)s :: %(message)s",
)

logger = logging.getLogger()
logger.setLevel(LOGGING_LEVEL)

# If the logger has handlers, we configure the first one. Otherwise we add a handler and configure it
if logger.handlers:
    console = logger.handlers[
        0
    ]  # we assume the first handler is the one we want to configure
else:
    console = logging.StreamHandler()
    logger.addHandler(console)

console.setFormatter(ColorFormater())

fn = TimedRotatingFileHandler(
    CONFIG.DUCKDB_FP.parent / "ts-v2.log", when="D", backupCount=14
)
fn.setFormatter(ColorFormater())
logger.addHandler(fn)

logging.info(f"Log level: {LOGGING_LEVEL}")
