import os
from enum import Enum
from pathlib import Path

from dotenv import load_dotenv

load_dotenv(override=True)


class Environment(Enum):
    PROD = "PROD"
    TEST = "TEST"
    CI = "CI"


class Config:
    @property
    def DATA_ROOT_DIR(self):
        return Path(os.getenv("DATA_ROOT_DIR", "data")).absolute()

    @property
    def RAW_DATA_DIR(self):
        return self.DATA_ROOT_DIR / "raw"

    @property
    def CLEANED_DATA_DIR(self):
        return self.DATA_ROOT_DIR / "clean"

    @property
    def DUMP_DATA_DIR(self):
        return self.DATA_ROOT_DIR / "dump"

    @property
    def DUCKDB_FP(self):
        return self.DATA_ROOT_DIR / "duckdb" / "duck.duckdb"

    @property
    def SQL_DIR(self):
        return Path(".") / "src" / "sql"

    @property
    def ENVIRONMENT(self):
        return Environment[os.getenv("ENVIRONMENT", "TEST")]


CONFIG = Config()

ROWS_LIMIT = int(os.getenv("ROWS_LIMIT", 10**9))

CSV_CHUNK_SIZE = int(os.getenv("CSV_CHUNK_SIZE", 100000))

SLACK_WEB_HOOK = os.getenv("SLACK_WEB_HOOK")

COMPUTE_INSTANCE_SERVER_ID = os.getenv("OS_SERVER_ID")

BACKUP_AFTER_PROCESS = os.getenv("BACKUP_AFTER_PROCESS", "False").lower() == "true"

RUNNING_IN_DOCKER = False
if "RUNNING_IN_DOCKER" in os.environ:
    RUNNING_IN_DOCKER = True

# Duck
DUCKDB_MEMORY_LIMIT = os.getenv("DUCKDB_MEMORY_LIMIT", None)

# HOSTS AND PORTS
LOCALHOST = "localhost"
DOCKER_METABASE = "metabase"
DOCKER_MB_POSTGRES = "mb-postgres"

if RUNNING_IN_DOCKER:
    MB_POSTGRES_HOST = DOCKER_MB_POSTGRES
    METABASE_HOST = os.getenv("METABASE_HOST", DOCKER_METABASE)
    MB_POSTGRES_PORT = 5432
else:
    MB_POSTGRES_HOST = LOCALHOST
    METABASE_HOST = os.getenv("METABASE_HOST", LOCALHOST)
    MB_POSTGRES_PORT = 6543

# Metabase
METABASE_USERNAME = os.getenv("METABASE_USERNAME")
METABASE_PASSWORD = os.getenv("METABASE_PASSWORD")
METABASE_BASE_URL = os.getenv("METABASE_BASE_URL", f"http://{METABASE_HOST}:3000")

# S3
S3_ACCESS_KEY = os.getenv("S3_ACCESS_KEY")
S3_SECRET_KEY = os.getenv("S3_SECRET_KEY")
S3_USERNAME = os.getenv("S3_USERNAME")
S3_REGION = os.getenv("S3_REGION")
S3_BUCKET = (
    os.getenv("S3_BUCKET_PROD")
    if CONFIG.ENVIRONMENT == Environment.PROD
    else os.getenv("S3_BUCKET_TEST")
)

# Metabase's Postgres backend
MB_POSTGRES_PASSWORD = os.getenv("MB_POSTGRES_PASSWORD", "")

FORMAT_GOOGLE_DOC_ID = "1szJ1-EFW8FP4ZoK8BLkNrJSq-qwNCN7LSRjm0WNKOLY"
MOTIF_REPLACEMENT_SHEET = "motifs_replacements"
