CREATE OR REPLACE TABLE $entreprise_name AS (
    WITH entreprises_infos AS (
        SELECT
            entreprise_id,
            raison_sociale,
            secteur_activite,
            numero_siren AS siren,
            pays_code AS pays,
            CASE WHEN mere_id = 'NULL' THEN NULL ELSE mere_id::INTEGER END AS mere_id
        FROM transform_declaration
    ),

    unique_entreprise AS (
        SELECT
            entreprise_id,
            MODE(raison_sociale) AS raison_sociale,
            MODE(secteur_activite) AS secteur_activite,
            MODE(siren) AS siren,
            MODE(pays) AS pays,
            MODE(mere_id) AS mere_id,
            COUNT(DISTINCT raison_sociale) AS nb_raison_sociale,
            COUNT(DISTINCT secteur_activite) AS nb_secteur_activite,
            COUNT(DISTINCT siren) AS nb_siren,
            COUNT(DISTINCT pays) AS nb_pays,
            COUNT(DISTINCT mere_id) AS nb_mere_id
        FROM entreprises_infos
        GROUP BY 1
    ),

    manual_ents AS (
        SELECT DISTINCT ON (entreprise_id)
            entreprise_id,
            raison_sociale,
            NULL AS secteur_activite,
            NULL AS siren,
            NULL AS pays,
            mere_id,
            0 AS nb_raison_sociale,
            0 AS nb_secteur_activite,
            0 AS nb_siren,
            0 AS nb_pays,
            0 AS nb_mere_id
        FROM entreprise_manual_correction
        WHERE entreprise_id NOT IN (SELECT entreprise_id FROM entreprises_infos)
    ),

    unknown_mere AS (
        SELECT DISTINCT
            mere_id AS entreprise_id,
            NULL AS raison_sociale,
            NULL AS secteur_activite,
            NULL AS siren,
            NULL AS pays,
            NULL::INTEGER AS mere_id,
            0 AS nb_raison_sociale,
            0 AS nb_secteur_activite,
            0 AS nb_siren,
            0 AS nb_pays,
            0 AS nb_mere_id
        FROM entreprises_infos
        WHERE
            mere_id IS NOT NULL
            AND mere_id NOT IN (
                SELECT entreprise_id FROM entreprises_infos
                UNION ALL
                SELECT entreprise_id FROM manual_ents
            )
    )

    SELECT * FROM unique_entreprise
    UNION
    SELECT * FROM unknown_mere
    UNION
    SELECT * FROM manual_ents
)
