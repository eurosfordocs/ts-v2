WITH multiple_siren AS (
    SELECT
        siren,
        MIN(entreprise_id) AS oldest,
        MIN(mere_id) AS common_mere_id,
        COUNT(DISTINCT mere_id) AS diversity,
        CAST(SUM(CASE WHEN mere_id IS NULL THEN 1 ELSE 0 END) AS BOOLEAN) AS has_null
    FROM main.entreprise
    WHERE siren IS NOT NULL AND pays = 'FR'
    GROUP BY 1
    HAVING COUNT(*) > 1
),

augmented_ent AS (
    SELECT *
    FROM multiple_siren AS ms
    INNER JOIN main.entreprise AS ent ON ms.siren = ent.siren
    WHERE diversity <= 1 AND has_null
    ORDER BY ms.siren
)

UPDATE entreprise
SET mere_id = CASE WHEN diversity = 0 THEN oldest ELSE common_mere_id END
FROM augmented_ent AS aug
WHERE (
    (diversity = 0 AND aug.entreprise_id <> oldest)
    OR (diversity = 1 AND aug.mere_id IS NULL AND entreprise.entreprise_id <> common_mere_id)
)
AND entreprise.entreprise_id = aug.entreprise_id
