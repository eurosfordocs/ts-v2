WITH mere_filiale_info AS (
    SELECT
        filiale.entreprise_id AS declarant_id,
        COALESCE(mere.entreprise_id, filiale.entreprise_id) AS entreprise_id,
        COALESCE(mere.siren, filiale.siren) AS entreprise_siren,
        COALESCE(
            mere.raison_sociale, filiale.raison_sociale
        ) AS entreprise_raison_sociale,
        COALESCE(mere.pays, filiale.pays) AS entreprise_pays,
        COALESCE(
            mere.secteur_activite, filiale.secteur_activite
        ) AS entreprise_secteur_activite
    FROM entreprise AS filiale
    LEFT JOIN entreprise AS mere
        ON filiale.mere_id = mere.entreprise_id
)

UPDATE transform_declaration
SET
    entreprise_siren = mere_filiale_info.entreprise_siren,
    entreprise_raison_sociale = mere_filiale_info.entreprise_raison_sociale,
    entreprise_pays = mere_filiale_info.entreprise_pays,
    entreprise_secteur_activite = mere_filiale_info.entreprise_secteur_activite,
    entreprise_ts_id = mere_filiale_info.entreprise_id,
FROM mere_filiale_info
WHERE transform_declaration.entreprise_id = mere_filiale_info.declarant_id
