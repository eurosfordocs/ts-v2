WITH hierarchies AS (
    SELECT
        filiale.entreprise_id AS filiale_id,
        mere.mere_id AS meta_mere_id
    FROM entreprise AS filiale
    INNER JOIN entreprise AS mere ON filiale.mere_id = mere.entreprise_id
    WHERE mere.mere_id IS NOT NULL
)

UPDATE entreprise
SET mere_id = meta_mere_id
FROM hierarchies
WHERE
    entreprise.entreprise_id = hierarchies.filiale_id
    AND entreprise.entreprise_id != hierarchies.meta_mere_id
