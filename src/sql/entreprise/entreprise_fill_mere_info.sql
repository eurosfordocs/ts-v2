WITH ranked_filiales AS (
    SELECT
        filiale.* EXCLUDE (mere_id),
        mere.entreprise_id AS ent_mere_id,
        RANK() OVER (PARTITION BY mere.entreprise_id ORDER BY filiale.entreprise_id ASC) AS rnk
    FROM main.entreprise AS mere
    LEFT JOIN main.entreprise AS filiale ON mere.entreprise_id = filiale.mere_id
    WHERE
        mere.raison_sociale IS NULL
        OR mere.secteur_activite IS NULL
        OR mere.siren IS NULL
        OR mere.pays IS NULL
)

UPDATE entreprise
SET
    raison_sociale = coalesce(entreprise.raison_sociale, filiale.raison_sociale),
    secteur_activite = coalesce(entreprise.secteur_activite, filiale.secteur_activite),
    siren = coalesce(entreprise.siren, filiale.siren),
    pays = coalesce(entreprise.pays, filiale.pays)
FROM ranked_filiales AS filiale
WHERE
    rnk = 1
    AND filiale.ent_mere_id = entreprise.entreprise_id
