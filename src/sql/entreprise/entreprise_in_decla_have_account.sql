WITH from_declarations AS (
    SELECT DISTINCT entreprise_ts_id
    FROM main.transform_declaration
),

from_ent AS (
    SELECT entreprise_id
    FROM main.entreprise
    WHERE mere_id IS NULL
)

SELECT COUNT(*)
FROM from_declarations AS d
LEFT JOIN from_ent AS e ON d.entreprise_ts_id = e.entreprise_id
WHERE e.entreprise_id IS NULL
