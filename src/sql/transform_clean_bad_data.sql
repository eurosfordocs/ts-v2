-- LABORATOIRES LEHNING SAS utilise le meme rpps pour des dizaines de professionnels différents
UPDATE transform_declaration
SET
    beneficiaire_identifiant = NULL,
    beneficiaire_type_code = NULL
WHERE entreprise_id = 1129;

-- Some companies just put meaningless keywords as name,
UPDATE transform_declaration
SET
    identite = NULL
WHERE identite IN ($bad_identite) OR identite = '';

-- Some companies use SARL for name and actual name in prenom
UPDATE transform_declaration
SET
    identite = UPPER(prenom),
    prenom = NULL
WHERE identite in ('SARL', 'EURL', 'SASU');

-- Some companies just put meaningless keywords as prenom,
UPDATE transform_declaration
SET
    prenom = NULL
WHERE prenom IN ($bad_prenom) OR prenom IN ('', ' ');

-- remove meaningless zip codes
UPDATE transform_declaration
SET
    code_postal = NULL
WHERE code_postal IN ('00000', '12345');

-- remove meaningless zip codes entreprise
UPDATE transform_declaration
SET
    code_postal_entreprise = NULL
WHERE code_postal_entreprise IN ('00000', '12345');
