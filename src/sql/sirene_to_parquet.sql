COPY (
    SELECT
        siren,
        etatadministratifunitelegale = 'A' AS is_active,
        COALESCE(nomusageunitelegale, denominationunitelegale, nomunitelegale) AS nom,
        activiteprincipaleunitelegale AS naf8,
        categoriejuridiqueunitelegale AS code_ju
    FROM (
        SELECT
            siren,
            etatadministratifunitelegale,
            nomusageunitelegale,
            denominationunitelegale,
            nomunitelegale,
            activiteprincipaleunitelegale,
            categoriejuridiqueunitelegale,
            CAST(datedebut AS DATE) AS datedebut,
            CAST(datefin AS DATE) AS datefin
        FROM
            READ_CSV_AUTO(
                '$zip_fn',
                header = TRUE,
                parallel = TRUE,
                delim = ','
            )
    )
    QUALIFY ROW_NUMBER() OVER (PARTITION BY siren ORDER BY datedebut DESC) = 1
)
TO '$output_fn' (format parquet)
