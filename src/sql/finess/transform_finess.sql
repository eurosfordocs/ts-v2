CREATE OR REPLACE TABLE finess AS (
    SELECT
        num_finess_et,
        num_finess_ej AS num_finess_ju,
        raison_sociale_et,
        raison_sociale_longue_et,
        complement_raison_sociale,
        raison_sociale_ej AS raison_sociale_ju,
        raison_sociale_longue_ej AS raison_sociale_longue_ju,
        CONCAT(departement, LPAD(commune::VARCHAR, 3, '0')) AS code_commune,
        departement,
        statut_juridique_ej AS statut_juridique_code_ju,
        libelle_statut_juridique_ej AS statut_juridique_ju,
        statut_juridique,
        type_etablissement,
        categorie_et AS categorie_code_et,
        libelle_categorie_et AS categorie_et,
        categorie_agregat_et AS categorie_agregat_code_et,
        libelle_categorie_agregat_et AS categorie_agregat_et,
        siret,
        LEFT(siret::varchar, 9) AS siren,
        code_ape,
        code_mft,
        libelle_mft AS mft,
        code_sph,
        libelle_sph AS sph,
        date_ouverture,
        date_autorisation,
        date_maj,
        num_uai
    FROM raw_finess
    WHERE dernier_enregistrement

)
