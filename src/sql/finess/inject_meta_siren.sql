WITH matching AS (
    SELECT * FROM frame
),

enriched AS (
    SELECT
        finess.num_finess_et,
        finess.siren,
        COALESCE(matching.origin, finess.siren) AS meta_siren
    FROM finess
    LEFT JOIN matching ON finess.siren = matching.siren
),

popularity AS (
    SELECT
        enriched.meta_siren,
        enriched.siren,
        COUNT(enriched.num_finess_et) AS pop
    FROM enriched
    WHERE enriched.siren IS NOT NULL
    GROUP BY ALL
),

top_siren AS (
    SELECT
        meta_siren,
        siren
    FROM popularity
    QUALIFY ROW_NUMBER() OVER (PARTITION BY meta_siren ORDER BY pop DESC) = 1
),

to_correct AS (
    SELECT DISTINCT
        enriched.siren,
        top_siren.siren AS meta_siren
    FROM enriched
    INNER JOIN top_siren ON enriched.meta_siren = top_siren.meta_siren
)

UPDATE finess
SET meta_siren = to_correct.meta_siren
FROM to_correct
WHERE to_correct.siren = finess.siren
