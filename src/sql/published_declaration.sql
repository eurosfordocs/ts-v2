CREATE TABLE transform_declaration AS (
    WITH ranked_declaration AS (
        SELECT
            id,
            MAX(COALESCE(date_suppression, date_modif, date_ajout)) AS most_recent
        FROM all_raw_declaration
        GROUP BY ALL
    )

    SELECT ard.*
    FROM all_raw_declaration AS ard
    INNER JOIN ranked_declaration
        ON
            ranked_declaration.most_recent = COALESCE(ard.date_suppression, ard.date_modif, ard.date_ajout)
            AND ard.id = ranked_declaration.id
)
