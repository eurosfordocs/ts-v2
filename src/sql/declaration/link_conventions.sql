UPDATE transform_declaration SET convention_ts_id = NULL;

--Link avantages and remunerations to their conventions: matching entreprise et ou ent meres
UPDATE transform_declaration d SET
    convention_ts_id = (
        SELECT c.id
        FROM transform_declaration AS c
        WHERE
            c.lien_interet = 'Convention'
            AND c.identifiant_unique = d.convention_liee
            AND (
                c.entreprise_id = d.entreprise_id
                OR c.entreprise_ts_id = d.entreprise_ts_id
            )
        LIMIT 1
    )
WHERE d.lien_interet != 'Convention';

--Link avantages and remunerations to their conventions: match exact identifiant + origin_account
UPDATE transform_declaration d SET
    convention_ts_id = (
        SELECT c.id
        FROM transform_declaration AS c
        WHERE
            c.lien_interet = 'Convention'
            AND c.ben_origin_id = d.ben_origin_id
        LIMIT 1
    )
WHERE d.lien_interet != 'Convention' AND convention_ts_id IS NULL;


--Link avantages and remunerations to their conventions: match exact nom + prenom + identifiant
UPDATE transform_declaration d SET
    convention_ts_id = (
        SELECT c.id
        FROM transform_declaration AS c
        WHERE
            c.lien_interet = 'Convention'
            AND c.identite = d.identite
            AND c.prenom = d.prenom
            AND c.identifiant_unique = d.convention_liee
        LIMIT 1
    )
WHERE d.lien_interet != 'Convention' AND convention_ts_id IS NULL;
