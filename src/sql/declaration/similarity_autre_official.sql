WITH official_motifs AS (
    SELECT DISTINCT motif_lien_interet
    FROM transform_declaration
    WHERE motif_lien_interet IS NOT NULL AND motif_lien_interet != 'Autre'
),

unique_autre AS (
    SELECT DISTINCT autre_motif
    FROM transform_declaration
    WHERE autre_motif IS NOT NULL
),

scored AS (
    SELECT
        autre_motif,
        motif_lien_interet,
        JARO_WINKLER_SIMILARITY(autre_motif, LOWER(motif_lien_interet)) AS sim
    FROM unique_autre
    CROSS JOIN official_motifs
),

correct_motif AS (
    SELECT *
    FROM scored
    WHERE sim > 0.9
    QUALIFY ROW_NUMBER() OVER (PARTITION BY autre_motif ORDER BY sim DESC) = 1
)

UPDATE transform_declaration td
SET
    motif_lien_interet = cor.motif_lien_interet,
    autre_motif = NULL
FROM correct_motif AS cor
WHERE
    cor.autre_motif = td.autre_motif
    AND td.motif_lien_interet = 'Autre'
