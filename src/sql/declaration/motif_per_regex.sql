UPDATE transform_declaration
SET
    motif_lien_interet = '$motif',
    autre_motif = NULL
WHERE
    (motif_lien_interet = 'Autre' OR motif_lien_interet IS NULL)
    AND LOWER(coalesce(autre_motif, information_evenement)) similar to '$pattern';
