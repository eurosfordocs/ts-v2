-- Create table convention, sum avantages & remuneration for each
CREATE OR REPLACE TABLE convention AS (
    WITH unique_conventions AS (
        SELECT
            id AS convention_ts_id,
            montant AS montant_declare_convention,
            motif_lien_interet,
            motif_lien_interet_code,
            autre_motif,
            information_evenement
        FROM transform_declaration
        WHERE lien_interet = 'Convention'
    ),

    linked_aggs AS (
        SELECT
            c.convention_ts_id,
            SUM(CAST(c.lien_interet = 'Avantage' AS INT)) AS nombre_avantages_lies,
            SUM(CAST(c.lien_interet = 'Rémunération' AS INT)) AS nombre_remunerations_liees,
            SUM(
                CASE WHEN c.lien_interet = 'Avantage' THEN c.montant ELSE 0 END
            ) AS montant_total_avantages_lies,
            SUM(
                CASE WHEN c.lien_interet = 'Rémunération' THEN c.montant ELSE 0 END
            ) AS montant_total_remunerations_liees,
            SUM(c.montant) AS montant_total_lie
        FROM transform_declaration AS c
        WHERE c.lien_interet != 'Convention'
        GROUP BY c.convention_ts_id
    ),

    joined AS (
        SELECT
            un.*,
            COALESCE(agg.nombre_avantages_lies, 0) AS nombre_avantages_lies,
            COALESCE(agg.nombre_remunerations_liees, 0) AS nombre_remunerations_liees,
            COALESCE(agg.montant_total_avantages_lies, 0) AS montant_total_avantages_lies,
            COALESCE(agg.montant_total_remunerations_liees, 0) AS montant_total_remunerations_liees,
            COALESCE(agg.montant_total_lie, 0) AS montant_total_lie
        FROM unique_conventions AS un
        LEFT JOIN linked_aggs AS agg ON un.convention_ts_id = agg.convention_ts_id
    )

    SELECT
        *,
        GREATEST(montant_declare_convention - montant_total_lie, 0) AS montant,
        COALESCE(montant_declare_convention, 0) = 0 AS montant_masque,
        GREATEST(COALESCE(montant_declare_convention, 0), montant_total_lie) AS montant_total_convention
    FROM joined
);

-- Inject back convention aggregates
UPDATE transform_declaration SET
    nombre_avantages_lies = c.nombre_avantages_lies,
    nombre_remunerations_liees = c.nombre_remunerations_liees,
    montant_total_avantages_lies = c.montant_total_avantages_lies,
    montant_total_remunerations_liees = c.montant_total_remunerations_liees,
    montant_declare_convention = c.montant_declare_convention,
    montant_total_convention = c.montant_total_convention,
    montant_masque = c.montant_masque,
    montant = c.montant
FROM convention AS c
WHERE c.convention_ts_id = id;


-- Use convention motif_lien_interet for remunerations
UPDATE transform_declaration d
SET
    motif_lien_interet = c.motif_lien_interet,
    motif_lien_interet_code = c.motif_lien_interet_code,
    autre_motif = c.autre_motif
FROM convention AS c
WHERE
    d.convention_ts_id = c.convention_ts_id
    AND d.lien_interet = 'Rémunération';

-- Use convention information_evenement for remunerations & avantage, if empty
UPDATE transform_declaration d
SET information_evenement = c.information_evenement
FROM convention AS c
WHERE
    d.convention_ts_id = c.convention_ts_id
    AND d.lien_interet != 'Convention'
    AND d.information_evenement IS NULL;
