-- Cleaning motifs: replacing strait quotes, long dashes and double spaces
UPDATE transform_declaration
SET motif_lien_interet = REPLACE(motif_lien_interet,  '’','''')
WHERE motif_lien_interet like '%’%';

UPDATE transform_declaration
SET motif_lien_interet = REPLACE(motif_lien_interet, '–', '-')
WHERE motif_lien_interet like '%–%';

UPDATE transform_declaration
SET motif_lien_interet = REPLACE(motif_lien_interet, '  ', ' ')
WHERE motif_lien_interet like '%  %';


-- Cleaning motif_code (A > AUTRE, CRS > RS, CINM > CIM)
UPDATE transform_declaration
SET motif_lien_interet_code = 'AUTRE'
WHERE motif_lien_interet_code = 'A';

UPDATE transform_declaration
SET motif_lien_interet_code = 'RS'
WHERE motif_lien_interet_code = 'CRS';

UPDATE transform_declaration
SET motif_lien_interet_code = 'CIM'
WHERE motif_lien_interet_code = 'CINM';


-- Extract real words with an autre pattern '\[?autres?\]? ?: ?\[(.*)\]'
UPDATE transform_declaration
SET autre_motif = COALESCE(autre_motif, information_evenement);

-- Extract real words with an autre pattern '\[?autres?\]? ?: ?\[(.*)\]'
UPDATE transform_declaration
SET autre_motif = TRIM(REGEXP_EXTRACT(autre_motif, '\[?autres?\]? ?: ?\[(.*)\]', 1))
WHERE autre_motif SIMILAR TO '\[?autres?\]? ?: ?\[(.*)\]';


-- Extract real words with an autre pattern '\[?autres?\]? ?:(.*)'
UPDATE transform_declaration
SET autre_motif = TRIM(REGEXP_EXTRACT(autre_motif, '\[?autres?\]? ?:(.*)', 1))
WHERE autre_motif SIMILAR TO '\[?autres?\]? ?:(.*)';


-- Extract real words with an autre pattern 'autres? \[(.*)\]'
UPDATE transform_declaration
SET autre_motif = TRIM(REGEXP_EXTRACT(autre_motif, 'autres? \[(.*)\]', 1))
WHERE autre_motif SIMILAR TO 'autres? [\(\[](.*)[\)\]]';


-- Extract real words inside brackets
UPDATE transform_declaration
SET autre_motif = TRIM(REGEXP_EXTRACT(autre_motif, '^\[(.*)\]$', 1))
WHERE autre_motif SIMILAR TO '^\[(.*)\]$';

-- Remove meaningless autre_motif values
UPDATE transform_declaration
SET autre_motif = NULL
WHERE
    autre_motif IN ($null_autre_motifs)
    OR autre_motif = ''
    OR autre_motif SIMILAR TO '[0-9]{{5,15}}';

-- Remove meaningless information_evenement values
UPDATE transform_declaration
SET information_evenement = NULL
WHERE information_evenement IN ($null_information_evenement);

-- Roche creates full sentences
UPDATE transform_declaration
SET motif_lien_interet = 'Partenariat',
    autre_motif = NULL
WHERE autre_motif like 'roche souhaite%'
