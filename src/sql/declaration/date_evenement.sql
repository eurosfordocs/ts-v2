WITH events AS (
    SELECT DISTINCT
        information_evenement,
        date_transmission
    FROM transform_declaration
    WHERE
        information_evenement IS NOT NULL
        AND information_evenement SIMILAR TO '.*[0-9]+.*'
        AND information_evenement NOT SIMILAR TO 'n°\d+'
),

extracted AS (
    SELECT
        information_evenement,
        date_transmission,
        REGEXP_EXTRACT(information_evenement, '\b(\d{1,4}[\-\/ \.]\d{1,2}[\-\/ \.]\d{2,4})\b') AS general_format,
        REGEXP_EXTRACT(information_evenement, '\b(\d{1,2}/\d{1,2}/\d{4})\b') AS full_dates,
        REGEXP_EXTRACT(information_evenement, '\b(\d{1,2}-\d{1,2}-\d{4})\b') AS full_dates_dash,
        REGEXP_EXTRACT(information_evenement, '\b(\d{1,2} \d{1,2} \d{4})\b') AS full_dates_space,
        REGEXP_EXTRACT(information_evenement, '\b(\d{1,2}\.\d{1,2}\.\d{4})\b') AS full_dates_dots,
        --
        REGEXP_EXTRACT(information_evenement, '\b(\d{4}-\d{2}-\d{2})\b') AS full_reverse_dash,
        REGEXP_EXTRACT(information_evenement, '\b(\d{4} \d{2} \d{2})\b') AS full_reverse_space,
        REGEXP_EXTRACT(information_evenement, '\b(\d{4}\.\d{2}\.\d{2})\b') AS full_reverse_dot,
        REGEXP_EXTRACT(information_evenement, '\b(\d{4}/\d{2}/\d{2})\b') AS full_reverse_slash,
        --
        REGEXP_EXTRACT(information_evenement, '\b(\d{1,2}/\d{1,2}/\d{2})\b') AS reduced_slash,
        REGEXP_EXTRACT(information_evenement, '\b(\d{1,2}\.\d{1,2}\.\d{2})\b') AS reduced_dot,
        REGEXP_EXTRACT(information_evenement, '\b(\d{1,2} \d{1,2} \d{2})\b') AS reduced_space,
        REGEXP_EXTRACT(information_evenement, '\b(\d{1,2}-\d{1,2}-\d{2})\b') AS reduced,
        --
        REGEXP_EXTRACT(
            information_evenement,
           '\b(\d{1,2})?\s?(jan(vier|uary)?|f[ée]v(rier)?|february|mars?(ch)?|a[vp]r(il)?|ma[iy]|jul?(ne?|in|ly|illet)|ao[ùu]t?|august|sept?(emb(re|er))?|oct(ob(er|re))?|nov(emb(re|er))?|d[ée]c(emb(re|er))?)\s?\d{2,4}\b' --noqa
        )
        AS literal_month,
        --
        REGEXP_EXTRACT(information_evenement, '\b(\d{4}20\d{2}\b)') AS single_number,
        REGEXP_EXTRACT(information_evenement, '\b(20\d{6})\b') AS inverted_single_number
    FROM events
),

replaced_month AS (
    SELECT
        * EXCLUDE (literal_month),
        CASE
            WHEN literal_month = '' THEN NULL ELSE
                REGEXP_REPLACE(
                    REGEXP_REPLACE(
                        REGEXP_REPLACE(
                            REGEXP_REPLACE(
                                REGEXP_REPLACE(
                                    REGEXP_REPLACE(
                                        REGEXP_REPLACE(
                                            REGEXP_REPLACE(REGEXP_REPLACE(REGEXP_REPLACE(REGEXP_REPLACE(REGEXP_REPLACE(
                                                CASE
                                                    WHEN literal_month SIMILAR TO '^\d+.*' THEN literal_month ELSE
                                                        '01' || literal_month
                                                END,
                                                '\s?jan(vier|uary)?\s?', '-01-'
                                            ),
                                            '\s?(f[ée]v(rier)?|feb(ruary)?)\s?', '-02-'),
                                            '\s?(mars?(ch)?)\s?', '-03-'),
                                            '\s?(a[vp]r(il)?)\s?', '-04-'),
                                            '\s?(ma[iy])\s?', '-05-'),
                                            '\s?(june?|juin)\s?', '-06-'
                                        ),
                                        '\s?(juillet|july?)\s?', '-07-'
                                    ),
                                    '\s?(ao[ùu]t|aug(ust)?)\s?', '-08-'
                                ),
                                '\s?(sept?(emb(re|er))?)\s?', '-09-'
                            ),
                            '\s?(oct(ob(re|er))?)\s?', '-10-'
                        ),
                        '\s?(nov(emb(re|er))?)\s?', '-11-'
                    ),
                    '\s?(d[ée]c(emb(re|er))?)\s?', '-12-'
                )
        END AS literal_month
    FROM extracted
),

cleaned AS (
    SELECT
        information_evenement,
        date_transmission,
        REGEXP_REPLACE(
            COALESCE(
                CASE WHEN full_dates != '' THEN full_dates END,
                CASE WHEN full_dates_dash != '' THEN full_dates_dash END,
                CASE WHEN full_dates_space != '' THEN full_dates_space END,
                CASE WHEN full_dates_dots != '' THEN full_dates_dots END,
                --
                CASE WHEN full_reverse_dash != '' THEN full_reverse_dash END,
                CASE WHEN full_reverse_space != '' THEN full_reverse_space END,
                CASE WHEN full_reverse_dot != '' THEN full_reverse_dot END,
                CASE WHEN full_reverse_slash != '' THEN full_reverse_slash END,
                CASE WHEN reduced != '' THEN reduced END,
                --
                CASE WHEN reduced_slash != '' THEN reduced_slash END,
                CASE WHEN reduced_dot != '' THEN reduced_dot END,
                CASE WHEN reduced_space != '' THEN reduced_space END,
                CASE WHEN reduced_dot != '' THEN reduced_dot END,
                CASE WHEN literal_month != '' THEN literal_month END,
                CASE
                    WHEN
                        inverted_single_number != ''
                        THEN
                            (
                                SUBSTRING(inverted_single_number, 1, 4)
                                || '-'
                                || SUBSTRING(inverted_single_number, 5, 2)
                                || '-'
                                || SUBSTRING(inverted_single_number, 7)
                            )
                END,
                CASE
                    WHEN
                        single_number != ''
                        THEN
                            (
                                SUBSTRING(single_number, 1, 2)
                                || '-'
                                || SUBSTRING(single_number, 3, 2)
                                || '-'
                                || SUBSTRING(single_number, 5)
                            )
                END
            ), '[/\ \.]', '-', 'g'
        ) AS cleaned_dates
    FROM replaced_month
),

first_zero_filled AS (
    SELECT
        * EXCLUDE (cleaned_dates),
        CASE
            WHEN LENGTH(SPLIT(cleaned_dates, '-')[1]) = 1 THEN ('0' || cleaned_dates)
            ELSE cleaned_dates
        END AS cleaned_dates
    FROM cleaned
),


middle_zero_filled AS (
    SELECT
        * EXCLUDE (cleaned_dates),
        CASE
            WHEN
                LENGTH(SPLIT(cleaned_dates, '-')[2]) = 1
                THEN (SUBSTRING(cleaned_dates, 1, 3) || '0' || SUBSTRING(cleaned_dates, 4))
            ELSE cleaned_dates
        END AS cleaned_dates
    FROM first_zero_filled
),


last_zero_filled AS (
    SELECT
        * EXCLUDE (cleaned_dates),
        date_transmission,
        CASE
            WHEN
                LENGTH(SPLIT(cleaned_dates, '-')[3]) = 1
                THEN
                    (
                        SUBSTRING(cleaned_dates, 1, LENGTH(cleaned_dates) - 1)
                        || '0'
                        || SUBSTRING(cleaned_dates, LENGTH(cleaned_dates))
                    )
            ELSE cleaned_dates
        END AS cleaned_dates
    FROM middle_zero_filled
),

casted AS (
    SELECT
        information_evenement,
        date_transmission,
        COALESCE(
            CASE WHEN LENGTH(cleaned_dates) = 10 THEN TRY_STRPTIME(cleaned_dates, '%d-%m-%Y') END,
            CASE WHEN LENGTH(cleaned_dates) = 10 THEN TRY_STRPTIME(cleaned_dates, '%Y-%m-%d') END,
            --
            CASE WHEN LENGTH(cleaned_dates) = 10 THEN TRY_STRPTIME(cleaned_dates, '%m-%d-%Y') END,
            CASE WHEN LENGTH(cleaned_dates) = 10 THEN TRY_STRPTIME(cleaned_dates, '%Y-%d-%m') END,
            --
            CASE
                WHEN
                    LENGTH(cleaned_dates) = 8
                    AND (
                        ABS(EXTRACT(DAY FROM date_transmission - TRY_STRPTIME(cleaned_dates, '%y-%m-%d')))
                        < ABS(EXTRACT(DAY FROM date_transmission - TRY_STRPTIME(cleaned_dates, '%d-%m-%y')))
                    )
                    THEN TRY_STRPTIME(cleaned_dates, '%y-%m-%d')
                WHEN LENGTH(cleaned_dates) = 8 THEN TRY_STRPTIME(cleaned_dates, '%d-%m-%y')
            END
        ) AS date_evenement
    FROM last_zero_filled
)

UPDATE transform_declaration
SET
    date_evenement = casted.date_evenement
FROM casted
WHERE
    casted.information_evenement = transform_declaration.information_evenement
    AND casted.date_transmission = transform_declaration.date_transmission
    AND casted.date_evenement IS NOT NULL
    AND casted.date_evenement > '2000-01-01'
    AND casted.date_evenement < '2040-01-01'
