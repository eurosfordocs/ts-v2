WITH matching AS (
    SELECT *
    FROM READ_CSV_AUTO('$meta_motif_match_url', header = TRUE)
)

UPDATE transform_declaration td
SET meta_motif_lien_interet = matching.meta_motif_lien_interet
FROM matching
WHERE matching.motif_lien_interet = td.motif_lien_interet;
