-- Replace some "Autre" motif_lien_interet with a dictionary
UPDATE transform_declaration
SET
    motif_lien_interet = cm.corrected_categorie,
    motif_lien_interet_code = cm.corrected_categorie_code,
    autre_motif = NULL
FROM (
    SELECT DISTINCT ON (raw_autre_motif) * FROM READ_CSV_AUTO('$categorisation_motifs_url', header=true)
    WHERE corrected_categorie IS NOT NULL
) AS cm
WHERE
    (motif_lien_interet = 'Autre' OR motif_lien_interet IS NULL)
    AND autre_motif = cm.raw_autre_motif;
