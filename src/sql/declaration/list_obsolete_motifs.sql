WITH correction AS (
    SELECT DISTINCT ON (raw_autre_motif) * FROM READ_CSV_AUTO('$categorisation_motifs_url', header=TRUE)
    WHERE corrected_categorie IS NOT NULL
),

motifs AS (
    SELECT COALESCE(autre_motif, information_evenement) AS motif
    FROM main.transform_declaration
    WHERE (motif_lien_interet = 'Autre' OR motif_lien_interet IS NULL)
)

SELECT raw_autre_motif
FROM correction
LEFT JOIN motifs ON motifs.motif = raw_autre_motif
WHERE motifs.motif IS NULL
