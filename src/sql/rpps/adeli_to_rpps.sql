-- Attribut un RPPS aux adeli partageant le meme professionnel.
WITH similar_code_postal AS (
    SELECT
        nom,
        prenom,
        profession,
        code_postal,
        COUNT(DISTINCT rpps) AS n_rpps,
        COUNT(DISTINCT adeli) AS n_adeli,
        MIN(rpps) AS rpps
    FROM rpps
    WHERE code_postal IS NOT NULL
    GROUP BY ALL
    HAVING n_rpps = 1 AND n_adeli > 0
)

UPDATE rpps base
SET rpps = sim.rpps
FROM similar_code_postal AS sim
WHERE
    base.rpps IS NULL
    AND sim.nom = base.nom
    AND sim.prenom = base.prenom
    AND sim.profession = base.profession
    AND sim.code_postal = base.code_postal;


-- Si un rpps unique existe pour un métier,
-- les adeli de ce bénéficiaire correspondent au rpps
WITH similar_names_profession AS (
    SELECT
        nom,
        prenom,
        profession,
        COUNT(DISTINCT rpps) AS n_rpps,
        COUNT(DISTINCT adeli) AS n_adeli,
        AVG(CAST(rpps IS NOT NULL AS INT)) AS rpps_rate,
        MIN(rpps) AS rpps
    FROM rpps
    GROUP BY ALL
    HAVING n_rpps = 1 AND rpps_rate < 1
)

UPDATE rpps base
SET rpps = sim.rpps
FROM similar_names_profession AS sim
WHERE
    base.rpps IS NULL
    AND sim.nom = base.nom
    AND sim.prenom = base.prenom
    AND sim.profession = base.profession;



-- Attribut un rpps aux différentes variantes des adeli
WITH similar_names_profession AS (
    SELECT
        adeli,
        COUNT(DISTINCT rpps) AS n_rpps,
        AVG(CAST(rpps IS NOT NULL AS INT)) AS rpps_rate,
        MIN(rpps) AS rpps
    FROM rpps
    GROUP BY ALL
    HAVING n_rpps = 1 AND rpps_rate < 1
)

UPDATE rpps base
SET rpps = sim.rpps
FROM similar_names_profession AS sim
WHERE
    base.rpps IS NULL
    AND sim.adeli = base.adeli;
