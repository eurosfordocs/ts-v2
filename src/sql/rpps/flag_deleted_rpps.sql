WITH deleted AS (
    SELECT ref."Identifiant PP", ref.hash
    FROM all_raw_rpps AS ref
    LEFT JOIN tmp_raw_rpps AS up ON up."Identifiant PP"::VARCHAR = ref."Identifiant PP"::VARCHAR AND up.hash = ref.hash
    WHERE up.hash IS NULL and ref.supr_date_ is null
)

UPDATE all_raw_rpps
SET supr_date_ = $dt
FROM deleted
where deleted."Identifiant PP" = all_raw_rpps."Identifiant PP" and deleted.hash = all_raw_rpps.hash
