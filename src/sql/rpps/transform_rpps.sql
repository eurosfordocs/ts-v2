CREATE OR REPLACE TABLE rpps AS (
    WITH specialite_replace (specialite, correction_spe) AS (
        VALUES
        ('Spécialiste en Médecine Générale', 'Médecine Générale'),
        ('Qualifié en Médecine Générale', 'Médecine Générale')
    ),

    profession_replace (profession, correction_profession) AS (
        VALUES
        ('Assistant de service social', 'assistant social'),
        ('Audioprothésiste', 'audio-prothésiste'),
        ('Technicien de laboratoire médical', 'technicien de laboratoire')
    ),

    renamed AS (
        SELECT
            "Identifiant PP"::varchar AS identifiant,
            "Nom d'exercice" AS nom,
            "Prénom d'exercice" AS prenom,
            "Libellé profession" AS profession,
            "Libellé savoir-faire" AS specialite,
            "Code catégorie professionnelle" AS categorie_pro,
            "Libellé mode exercice" AS mode_exercice,
            "Numéro FINESS site" AS finess,
            "Numéro FINESS établissement juridique" AS finess_jur,
            "Code postal (coord. structure)" AS code_postal,
            "Code commune (coord. structure)" AS commune_code,
            import_date_,
            supr_date_,
            CASE
                WHEN "Type d'identifiant PP" = 8 THEN 'RPPS' ELSE 'ADELI'
            END AS type_identifiant,
            COALESCE(
                LPAD(CAST("Numéro SIREN site" AS VARCHAR), 9, '0'), LPAD(LEFT("Numéro SIRET site", 9), 14, '0')
            ) AS siren
        FROM all_raw_rpps
    ),

    base AS (
        SELECT
            * EXCLUDE (specialite, profession),
            COALESCE(correction_spe, renamed.specialite) AS specialite,
            COALESCE(correction_profession, renamed.profession) AS profession
        FROM renamed
        LEFT JOIN
            specialite_replace ON renamed.specialite = specialite_replace.specialite
        LEFT JOIN profession_replace ON renamed.profession = profession_replace.profession
    )

    SELECT
        * EXCLUDE (identifiant, type_identifiant, siren),
        CASE WHEN siren LIKE '00000%' THEN SUBSTRING(siren::varchar, 6) ELSE siren END AS siren,
        CASE WHEN type_identifiant = 'RPPS' THEN identifiant END AS rpps,
        CASE WHEN type_identifiant = 'ADELI' THEN identifiant END AS adeli,
        CASE WHEN type_identifiant = 'ADELI' THEN SUBSTRING(identifiant::varchar, 1, 2) END AS dept,
        NULL::VARCHAR AS meta_nom,
        NULL::VARCHAR AS meta_prenom,
        NULL::VARCHAR AS meta_profession,
        NULL::VARCHAR AS meta_specialite,
        NULL::VARCHAR AS origin_account_moving,
        NULL::VARCHAR AS origin_account_same_commune,
        NULL::VARCHAR AS origin_account
    FROM base
)
