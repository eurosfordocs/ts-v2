WITH aggs AS (
    SELECT
        origin_account,
        LIST_SORT(
            LIST_DISTINCT(ARRAY_AGG(profession))
        ) AS profession_array,
        LIST_SORT(
            LIST_DISTINCT(ARRAY_AGG(specialite))
        ) AS specialite_array,
        LIST_DISTINCT(ARRAY_AGG(nom)) AS nom_array,
        LIST_SORT(LIST_DISTINCT(ARRAY_AGG(prenom))) AS prenom_array
    FROM rpps
    GROUP BY ALL
),

meta AS (
    SELECT
        *,
        CASE
            WHEN LEN(profession_array) = 0 THEN NULL
            WHEN LEN(profession_array) = 1 THEN profession_array[1]
            WHEN
                profession_array = ['Psychologue', 'Psychothérapeute']
                THEN 'Psychothérapeute'
            WHEN
                profession_array = ['Infirmier', 'Sage-Femme']
                THEN 'Sage-Femme'
            WHEN profession_array = ['Médecin', 'Sage-Femme'] THEN 'Médecin'
            WHEN profession_array = ['Infirmier', 'Médecin'] THEN 'Médecin'
            WHEN
                profession_array = ['Assistant social', 'Infirmier']
                THEN 'Infirmier'
            WHEN
                profession_array = ['Assistant dentaire', 'Infirmier']
                THEN 'Infirmier'
            WHEN
                profession_array = ['Infirmier', 'Masseur-Kinésithérapeute']
                THEN 'Masseur-Kinésithérapeute'
            WHEN
                profession_array = ['Masseur-Kinésithérapeute', 'Médecin']
                THEN 'Médecin'
            WHEN
                profession_array
                = ['Masseur-Kinésithérapeute', 'Pédicure-Podologue']
                THEN 'Pédicure-Podologue'
            ELSE profession_array[1]
        END AS profession,
        CASE
            WHEN LEN(specialite_array) = 0 THEN NULL
            WHEN LEN(specialite_array) = 1 THEN specialite_array[1]
            WHEN
                specialite_array[1] = 'Médecine Générale'
                THEN specialite_array[2]
            WHEN
                specialite_array[2] = 'Médecine Générale'
                THEN specialite_array[1]
            ELSE specialite_array[1]
        END AS specialite,
        CASE
            WHEN LEN(nom_array) = 0 THEN NULL
            WHEN LEN(nom_array) = 1 THEN nom_array[1]
            ELSE ARRAY_TO_STRING(LIST_SORT(LIST_DISTINCT(STRING_TO_ARRAY(ARRAY_TO_STRING(nom_array, ' '), ' '))), ' ')
        END AS nom,
        CASE
            WHEN LEN(prenom_array) = 0 THEN NULL
            WHEN prenom_array[1] = 'Jean' AND prenom_array[2] LIKE 'Jean %' THEN prenom_array[2]
            ELSE LIST_SORT(prenom_array)[1]
        END AS prenom
    FROM aggs
)

UPDATE rpps
SET
    meta_profession = meta.profession,
    meta_specialite = meta.specialite,
    meta_nom = meta.nom,
    meta_prenom = meta.prenom
FROM meta
WHERE meta.origin_account = rpps.origin_account
