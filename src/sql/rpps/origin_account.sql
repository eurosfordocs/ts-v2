UPDATE rpps
SET origin_account = COALESCE(rpps, adeli);


WITH origin_ids AS (
    SELECT
        origin_id,
        COALESCE(MIN(rpps), MIN(adeli)) AS origin
    FROM rpps_dedup_origin
    GROUP BY ALL
),

adeli_to_rpps AS (
    SELECT
        adeli,
        origin
    FROM rpps_dedup_origin
    INNER JOIN origin_ids ON rpps_dedup_origin.origin_id = origin_ids.origin_id
    WHERE rpps IS NULL AND adeli != origin
)

UPDATE rpps base
SET origin_account = sim.origin
FROM adeli_to_rpps AS sim
WHERE base.adeli = sim.adeli AND rpps IS NULL
