WITH aggs AS (
    SELECT
        origin_account,
        LIST_SORT(
            LIST_DISTINCT(ARRAY_AGG(profession))
        ) AS profession_array
    FROM rpps
    GROUP BY ALL
),

meta AS (
    SELECT
        origin_account,
        CASE
            WHEN LEN(profession_array) = 0 THEN NULL
            WHEN LEN(profession_array) = 1 THEN profession_array[1]
            WHEN
                profession_array = ['Psychologue', 'Psychothérapeute']
                THEN 'Psychothérapeute'
            WHEN
                profession_array = ['Infirmier', 'Sage-Femme']
                THEN 'Sage-Femme'
            WHEN profession_array = ['Médecin', 'Sage-Femme'] THEN 'Médecin'
            WHEN profession_array = ['Infirmier', 'Médecin'] THEN 'Médecin'
            WHEN
                profession_array = ['Assistant social', 'Infirmier']
                THEN 'Infirmier'
            WHEN
                profession_array = ['Assistant dentaire', 'Infirmier']
                THEN 'Infirmier'
            WHEN
                profession_array = ['Infirmier', 'Masseur-Kinésithérapeute']
                THEN 'Masseur-Kinésithérapeute'
            WHEN
                profession_array = ['Masseur-Kinésithérapeute', 'Médecin']
                THEN 'Médecin'
            WHEN
                profession_array
                = ['Masseur-Kinésithérapeute', 'Pédicure-Podologue']
                THEN 'Pédicure-Podologue'
            ELSE profession_array[1]
        END AS profession
    FROM aggs
)

UPDATE rpps
SET
    meta_profession = meta.profession
FROM meta
WHERE meta.origin_account = rpps.origin_account
