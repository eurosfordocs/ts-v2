WITH aggs AS (
    SELECT
        origin_account,
        LIST_SORT(LIST_DISTINCT(ARRAY_AGG(prenom))) AS prenom_array
    FROM rpps
    GROUP BY ALL
),

meta AS (
    SELECT
        origin_account,
        CASE
            WHEN LEN(prenom_array) = 0 THEN NULL
            WHEN prenom_array[1] = 'Jean' AND prenom_array[2] LIKE 'Jean %' THEN prenom_array[2]
            ELSE LIST_SORT(prenom_array)[1]
        END AS prenom
    FROM aggs
)

UPDATE rpps
SET
    meta_prenom = meta.prenom
FROM meta
WHERE meta.origin_account = rpps.origin_account
