WITH aggs AS (
    SELECT
        origin_account,
        LIST_SORT(
            LIST_DISTINCT(ARRAY_AGG(specialite))
        ) AS specialite_array
    FROM rpps
    GROUP BY ALL
),

meta AS (
    SELECT
        origin_account,
        CASE
            WHEN LEN(specialite_array) = 0 THEN NULL
            WHEN LEN(specialite_array) = 1 THEN specialite_array[1]
            WHEN
                specialite_array[1] = 'Médecine Générale'
                THEN specialite_array[2]
            WHEN
                specialite_array[2] = 'Médecine Générale'
                THEN specialite_array[1]
            ELSE specialite_array[1]
        END AS specialite,
    FROM aggs
)

UPDATE rpps
SET
    meta_specialite = meta.specialite
FROM meta
WHERE meta.origin_account = rpps.origin_account
