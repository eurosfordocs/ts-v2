WITH updated AS (
    SELECT up.*
    FROM tmp_raw_rpps AS up
    LEFT JOIN all_raw_rpps AS ref ON up."Identifiant PP"::VARCHAR = ref."Identifiant PP"::VARCHAR AND up.hash = ref.hash
    WHERE ref.hash IS NULL and ref.supr_date_ is null
)

INSERT INTO all_raw_rpps
SELECT $columns
FROM updated
