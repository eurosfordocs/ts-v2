WITH similar_code_postal AS (
    SELECT
        nom,
        prenom,
        profession,
        code_postal,
        COUNT(DISTINCT rpps) AS n_rpps,
        COUNT(DISTINCT adeli) AS n_adeli,
        MIN(adeli) AS adeli
    FROM rpps
    WHERE code_postal IS NOT NULL
    GROUP BY ALL
    HAVING n_rpps = 0 AND n_adeli > 1
)

UPDATE rpps ref
SET origin_account_same_commune = up.adeli
FROM similar_code_postal AS up
WHERE
    ref.nom = up.nom
    AND ref.prenom = up.prenom
    AND ref.profession = up.profession
    AND ref.code_postal = up.code_postal
    AND ref.adeli != up.adeli
