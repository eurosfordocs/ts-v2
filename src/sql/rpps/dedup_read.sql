WITH uniques AS (
    SELECT DISTINCT
        adeli,
        rpps,
        origin_account_moving,
        origin_account_same_commune
    FROM rpps
),

identified AS (
    SELECT
        *,
        ROW_NUMBER() OVER (ORDER BY rpps, adeli) AS id
    FROM uniques
),

identify_id_moving AS (
    SELECT
        identified.* EXCLUDE (origin_account_moving),
        COALESCE(per_rpps.id, per_adeli.id) AS origin_account_moving
    FROM identified
    LEFT JOIN identified AS per_rpps ON identified.origin_account_moving = per_rpps.rpps
    LEFT JOIN identified AS per_adeli ON identified.origin_account_moving = per_adeli.adeli
),

identify_id_commune AS (
    SELECT
        identify_id_moving.* EXCLUDE (origin_account_same_commune),
        COALESCE(per_rpps.id, per_adeli.id) AS origin_account_same_commune
    FROM identify_id_moving
    LEFT JOIN identified AS per_rpps ON identify_id_moving.origin_account_same_commune = per_rpps.rpps
    LEFT JOIN identified AS per_adeli ON identify_id_moving.origin_account_same_commune = per_adeli.adeli
)

SELECT
    id::VARCHAR as id,
    adeli::VARCHAR AS adeli,
    rpps::VARCHAR AS rpps,
    origin_account_moving::VARCHAR as origin_account_moving,
    origin_account_same_commune::VARCHAR AS origin_account_same_commune
FROM identify_id_commune
