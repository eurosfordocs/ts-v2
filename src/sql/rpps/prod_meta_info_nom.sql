WITH aggs AS (
    SELECT
        origin_account,
        LIST_DISTINCT(ARRAY_AGG(nom)) AS nom_array
    FROM rpps
    GROUP BY ALL
),

meta AS (
    SELECT
        origin_account,
        CASE
            WHEN LEN(nom_array) = 0 THEN NULL
            WHEN LEN(nom_array) = 1 THEN nom_array[1]
            ELSE ARRAY_TO_STRING(LIST_SORT(LIST_DISTINCT(STRING_TO_ARRAY(ARRAY_TO_STRING(nom_array, ' '), ' '))), ' ')
        END AS nom
    FROM aggs
)

UPDATE rpps
SET
    meta_nom = meta.nom
FROM meta
WHERE meta.origin_account = rpps.origin_account
