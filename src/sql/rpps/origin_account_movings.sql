WITH acceptable_names_prof AS (
    SELECT
        nom,
        prenom,
        profession
    FROM rpps
    GROUP BY ALL
    HAVING COUNT(DISTINCT rpps) <= 1 AND COUNT(DISTINCT adeli) > 1
),

combined AS (
    SELECT
        f.adeli,
        s.rpps AS new_rpps,
        s.adeli AS new_adeli
    FROM rpps AS f
    INNER JOIN
        acceptable_names_prof
        ON
            f.nom = acceptable_names_prof.nom
            AND f.prenom = acceptable_names_prof.prenom
            AND f.profession = acceptable_names_prof.profession
    INNER JOIN rpps AS s
        ON
            f.nom = s.nom
            AND f.prenom = s.prenom
            AND f.profession = s.profession
            AND f.supr_date_ = s.import_date_
    WHERE
        (s.adeli IS NOT NULL AND f.adeli != s.adeli)
        OR (s.adeli IS NULL AND f.rpps IS NULL)
),

origins AS (
    SELECT
        adeli,
        MIN(new_rpps) AS new_rpps,
        MIN(new_adeli) AS new_adeli
    FROM combined
    GROUP BY ALL
)

UPDATE rpps ref
SET
    origin_account_moving = COALESCE(new_rpps, new_adeli)
FROM origins
WHERE ref.adeli = origins.adeli
