-- identify declaration id
WITH
new_format AS (
    SELECT
        id,
        identifiant_unique,
        lien_interet,
        raison_sociale,
        identite,
        beneficiaire_identifiant
    FROM all_raw_declaration
    QUALIFY ROW_NUMBER() OVER (PARTITION BY id ORDER BY date_ajout) = 1
),

base AS (
    SELECT
        l.id,
        r.id AS raw_id
    FROM legacy_ts AS l
    INNER JOIN new_format AS r ON
        l.identifiant_unique = r.identifiant_unique
        AND l.lien_interet = r.lien_interet
        AND l.raison_sociale = r.raison_sociale
        AND (JARO_WINKLER_SIMILARITY(lower(l.identite), lower(r.identite)) >0.85
        OR (lower(l.beneficiaire_identifiant) = lower(r.beneficiaire_identifiant)) and (l.beneficiaire_identifiant <> '[SO]'))
)

UPDATE legacy_ts
SET id = raw_id
FROM base
WHERE base.id = legacy_ts.id;


-- identify entreprise_id
WITH
new_format AS (
    SELECT
        id,
        entreprise_id,
        raison_sociale
    FROM all_raw_declaration
    WHERE id > 0
    QUALIFY ROW_NUMBER() OVER (PARTITION BY id ORDER BY date_ajout) = 1
),

combined AS (
    SELECT
        legacy_ts.entreprise_identifiant,
        MIN(new_format.entreprise_id) AS entreprise_id
    FROM legacy_ts
    INNER JOIN new_format ON legacy_ts.id = new_format.id
    GROUP BY 1
)

UPDATE legacy_ts
SET entreprise_id = c.entreprise_id
FROM combined AS c
WHERE legacy_ts.entreprise_identifiant = c.entreprise_identifiant;
