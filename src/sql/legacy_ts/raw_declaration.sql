-- legacy_ts_convention
CREATE OR REPLACE TABLE legacy_ts_convention AS (
    WITH raw AS (
    SELECT *, -- noqa
            -CAST(ROW_NUMBER() OVER () * 10 AS INT32) AS id
        FROM READ_CSV_AUTO(
            '$datadir/declaration_convention_2022_01_10_04_00.csv',
            sample_size = -1,
            -- True dataset has malformed dates while CI does not.
            -- This parameter force sniffer to fail to identify dates
            -- and force date columns to be read as string.
            dateformat = '%Y-%m-%d'
        )
    )

    SELECT
        * EXCLUDE (
            conv_montant_ttc, conv_date_debut, conv_date_fin, conv_date_signature, conv_manifestation_date,
            conv_manifestation_nom,
            conv_manifestation_lieu,
            conv_manifestation_organisateur
        ),
        TRY_STRPTIME(conv_date_debut, '%d/%m/%Y') AS date_debut,
        TRY_STRPTIME(conv_date_fin, '%d/%m/%Y') AS date_fin,
        TRY_STRPTIME(conv_date_signature, '%d/%m/%Y') AS date, --noqa
        conv_montant_ttc AS montant,
        CAST(NULL AS STRING) AS convention_liee,
        CONCAT(
            COALESCE(conv_manifestation_date, ''),
            ' ',
            COALESCE(conv_manifestation_nom, ''),
            ' ',
            COALESCE(conv_manifestation_lieu, ''),
            ' ',
            COALESCE(conv_manifestation_organisateur, '')
        ) AS information_evenement,
        'convention' AS lien_interet
    FROM raw
);

-- legacy_ts_avantages
CREATE OR REPLACE TABLE legacy_ts_avantages AS (
    WITH raw AS (
    SELECT *, -- noqa
            -(CAST(ROW_NUMBER() OVER () * 10 + 2 AS INT32)) AS id
        FROM READ_CSV_AUTO(
            '$datadir/declaration_avantage_2022_01_10_04_00.csv',
            sample_size = -1,
            dateformat = '%Y-%m-%d'
        )
    )

    SELECT
        * EXCLUDE (avant_convention_lie, avant_date_signature, avant_montant_ttc, semestre, avant_nature),
        avant_convention_lie AS convention_liee,
        TRY_STRPTIME(avant_date_signature, '%d/%m/%Y') AS date, --noqa
        avant_montant_ttc AS montant,
        CAST(NULL AS STRING) AS conv_objet,
        avant_nature AS conv_objet_autre,
        CAST(NULL AS STRING) AS information_evenement,
        CAST(NULL AS DATE) AS date_debut,
        CAST(NULL AS DATE) AS date_fin,
        'avantage' AS lien_interet
    FROM raw
);


-- legacy_ts_remuneration
CREATE OR REPLACE TABLE legacy_ts_remunerations AS (
    WITH raw AS (
    SELECT *, -- noqa
            -(CAST(ROW_NUMBER() OVER () * 10 + 1 AS INT32)) AS id
        FROM READ_CSV_AUTO(
            '$datadir/declaration_remuneration_2022_01_10_04_00.csv',
            sample_size = -1,
            dateformat = '%Y-%m-%d'
        )
    )

    SELECT
        * EXCLUDE (remu_convention_liee, remu_date, remu_montant_ttc),
        remu_convention_liee AS convention_liee,
        TRY_STRPTIME(remu_date, '%d/%m/%Y') AS date, --noqa
        remu_montant_ttc AS montant,
        CAST(NULL AS STRING) AS conv_objet,
        CAST(NULL AS STRING) AS conv_objet_autre,
        CAST(NULL AS STRING) AS information_evenement,
        CAST(NULL AS DATE) AS date_debut,
        CAST(NULL AS DATE) AS date_fin,
        'remuneration' AS lien_interet
    FROM raw
);

CREATE OR REPLACE TABLE legacy_entreprise AS (
    WITH raw AS (
    SELECT *, -- noqa
            -CAST(ROW_NUMBER() OVER () AS INT32) AS entreprise_id
        FROM READ_CSV_AUTO(
            '$datadir/entreprise_2022_01_10_04_00.csv',
            sample_size = -1
        )
    )

    SELECT
        identifiant,
        entreprise_id,
        REGEXP_EXTRACT(pays_code, '\[(.*)\]', 1) AS entreprise_pays_code,
        pays AS entreprise_nom_pays,
        REGEXP_EXTRACT(secteur_activite_code, '\[(.*)\]', 1) AS entreprise_secteur_activite_code,
        secteur AS secteur_activite,
        denomination_sociale AS entreprise_raison_sociale,
        CONCAT(
            COALESCE(adresse_1, ''),
            ', ',
            COALESCE(adresse_2, ''),
            ', ',
            COALESCE(adresse_3, ''),
            ', ',
            COALESCE(adresse_4, '')
        ) AS entreprise_adresse,
        code_postal AS code_postal_entreprise,
        ville AS entr_ville
    FROM raw
);

-- agregate legacy
CREATE OR REPLACE TABLE legacy_ts AS (
    WITH combined AS (
        SELECT $ordered_intermediate FROM legacy_ts_convention
        UNION ALL
        SELECT $ordered_intermediate FROM legacy_ts_remunerations
        UNION ALL
        SELECT $ordered_intermediate FROM legacy_ts_avantages
    )

    SELECT
        $stable_columns,
        $renamed_columns,
        COALESCE(c.benef_nom, c.benef_denomination_sociale) AS identite,
        REGEXP_EXTRACT(c.benef_categorie_code, '\[(.*)\]', 1) AS beneficiaire_categorie_code,
        REGEXP_EXTRACT(c.benef_qualite_code, '\[(.*)\]', 1) AS beneficiaire_profession_code,
        CONCAT(
            COALESCE(c.benef_adresse1, ''),
            ', ',
            COALESCE(c.benef_adresse2, ''),
            ', ',
            COALESCE(c.benef_adresse3, ''),
            ', ',
            COALESCE(c.benef_adresse4, '')
        ) AS adresse,

        c.ligne_rectification = 'Y' AS demande_de_rectification,
        REGEXP_EXTRACT(c.benef_pays_code, '\[(.*)\]', 1) AS pays_code,
        $empty_strings,
        CAST(NULL AS DATE) AS date_transmission,
        MAKE_DATE(2022, 2, 6) AS date_publication,
        'Publié' AS statut,
        MAKE_DATE(2022, 2, 6) AS date_ajout,
        MAKE_DATE(2022, 2, 6) AS date_modif,
        CAST(NULL AS DATE) AS date_suppression,
        CAST(NULL AS BOOLEAN) AS supprime,
        COALESCE(legacy_motifs.current, c.conv_objet) AS motif_lien_interet,
        c.entreprise_identifiant
    FROM combined AS c
    LEFT JOIN legacy_entreprise ON c.entreprise_identifiant = legacy_entreprise.identifiant
    LEFT JOIN legacy_motifs ON c.conv_objet = legacy_motifs.legacy
);
