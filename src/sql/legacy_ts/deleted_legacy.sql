WITH min_modif AS (
    SELECT MIN(date_modif) AS date_modif
    FROM all_raw_declaration
)

UPDATE legacy_ts
SET
    date_suppression = min_modif.date_modif,
    supprime = TRUE
FROM min_modif
WHERE legacy_ts.id < 0
