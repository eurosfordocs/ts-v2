--Extract declaration_identifiant from declaration_id
ALTER TABLE transform_old_declaration
ADD COLUMN declaration_identifiant VARCHAR;

UPDATE transform_old_declaration SET declaration_identifiant = SPLIT_PART(declaration_id, ' | ', 3);

--Extract motif_lien_interet from detail
ALTER TABLE transform_old_declaration
ADD COLUMN motif_lien_interet VARCHAR;

UPDATE transform_old_declaration SET motif_lien_interet = SPLIT_PART(detail, ' | ', 1),
WHERE detail IS NOT NULL AND detail LIKE '% | %';

--Remove garbage values for ben_identifiant ('N/A', '0', etc)
UPDATE transform_old_declaration
SET ben_identifiant = NULL
WHERE ben_identifiant IN (
    '[SO]',
    'N/A',
    '[BR]',
    '[AUTRE]',
    '999999999',
    'Non renseigné',
    'NON RENSEIGNE',
    '00000000000',
    'Nondisponible',
    'AUTRE',
    'Non disponible',
    '000000000',
    '[Nondisponible]',
    'SANS',
    '[0]',
    '[NA]',
    '?',
    'ASSOCIATION',
    'PAS DE NUMERO',
    '(vide)',
    'Opticien',
    '000',
    'INFIRMIERE',
    'INTERNE',
    'NON TROUVE',
    'Infirmier',
    'SANS RPPS'
);

UPDATE transform_declaration
SET ben_identifiant = NULL
WHERE LEN(ben_identifiant) < 3 AND ben_type_identifiant <> 'ORDRE';

--Remove any value from ben_type_identifiant if ben_identifiant is null
UPDATE transform_declaration
SET ben_type_identifiant = NULL
WHERE ben_identifiant IS NULL;


WITH
old_unique_identifiant AS (
    SELECT declaration_identifiant
    FROM main.transform_old_declaration
    GROUP BY 1 HAVING COUNT(*) = 1
),

new_unique_identifiant AS (
    SELECT identifiant_unique
    FROM main.raw_declaration
    GROUP BY 1
    HAVING COUNT(*) = 1
)

SELECT
    o.identifiant_entreprise,
    n.entreprise_id,
    o.entreprise_emmetrice,
    n.raison_sociale,
    COUNT(*) AS cnt
FROM main.raw_declaration AS n, main.transform_old_declaration AS o
WHERE
    n.identifiant_unique IN (SELECT * FROM new_unique_identifiant)
    AND o.declaration_identifiant IN (SELECT * FROM old_unique_identifiant)
    AND n.identifiant_unique = o.declaration_identifiant AND n.date = o.date
GROUP BY 1, 2, 3, 4
ORDER BY 1;



SELECT
    o.declaration_id,
    n.id,
    o.entreprise_emmetrice,
    n.raison_sociale,
    o.nom_prenom,
    n.identite,
    n.id_beneficiaire,
    COUNT(n.id) OVER (PARTITION BY o.declaration_id) AS matches
FROM main.raw_declaration AS n, main.transform_old_declaration AS o
WHERE
    n.identifiant_unique = o.declaration_identifiant AND n.date = o.date
ORDER BY matches DESC;

SELECT
    companies,
    COUNT(*)
FROM (
    SELECT
        identifiant_unique,
        identite,
        date,
        COUNT(id) AS duplicates,
        GROUP_CONCAT(raison_sociale ORDER BY raison_sociale) AS companies
    FROM main.raw_declaration
    GROUP BY 1, 2, 3
) WHERE duplicates > 1
GROUP BY 1
ORDER BY 2 DESC;


SELECT
    COALESCE(d.motif_lien_interet, cl.motif_lien_interet, 'Inconnu') AS motif_lien_interet,
    COUNT(d.montant) AS "déclarations",
    SUM(d.montant) AS montant
FROM declaration AS d
LEFT JOIN declaration AS cl ON d.convention_ts_id = cl.declaration_ts_id
WHERE d.type_declaration = 'Rémuneration'
GROUP BY 1
ORDER BY 3 DESC;


SELECT
    motif_lien_interet,
    COUNT(*),
    SUM(montant)
FROM main.declaration
GROUP BY 1
