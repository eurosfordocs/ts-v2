WITH multiple_rpps_accounts AS (
    SELECT efd_id
    FROM
        beneficiaire_extended
    GROUP BY 1
    HAVING
        COUNT(DISTINCT $ident) > 1
)

SELECT COUNT(*)
FROM
    multiple_rpps_accounts
