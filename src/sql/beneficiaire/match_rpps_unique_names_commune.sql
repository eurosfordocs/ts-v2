WITH unique_names AS (
    SELECT
        nom,
        prenom,
        commune_code,
        MIN(origin_account) AS origin_account
    FROM rpps
    WHERE commune_code IS NOT NULL
    GROUP BY ALL
    HAVING
        COUNT(DISTINCT origin_account) = 1
),

no_id_benex AS (
    SELECT efd_id
    FROM
        beneficiaire_extended
    WHERE
        nom IS NOT NULL
        AND prenom IS NOT NULL
        AND commune_code IS NOT NULL
        AND categorie_code = 'PRS'
    GROUP BY
        1
    HAVING
        COUNT(DISTINCT rpps) = 0
        AND COUNT(DISTINCT adeli) = 0
),

accounts_with_matching_ids AS (
    SELECT
        benex.efd_id,
        benex.nb_decla,
        unique_names.origin_account
    FROM
        beneficiaire_extended AS benex
    INNER JOIN
        unique_names
        ON
            benex.nom = unique_names.nom
            AND benex.prenom = unique_names.prenom
            AND benex.commune_code = unique_names.commune_code
    INNER JOIN no_id_benex ON benex.efd_id = no_id_benex.efd_id
),

-- because sometimes first and last name are inverted, and a real beneficiary correspond to the inverted,
-- the same account can get 2 rpps, which generates instabilities when joining with other tables.
top_rpps AS (
    SELECT
        efd_id,
        origin_account
    FROM
        accounts_with_matching_ids
    GROUP BY
        1, 2
    QUALIFY ROW_NUMBER() OVER (PARTITION BY efd_id ORDER BY SUM(nb_decla) DESC) = 1
)

UPDATE
beneficiaire_extended benex
SET
    rpps = CASE WHEN LEN(top_rpps.origin_account) = 11 THEN top_rpps.origin_account END,
    adeli = CASE WHEN LEN(top_rpps.origin_account) = 9 THEN top_rpps.origin_account END
FROM
    top_rpps
WHERE
    benex.efd_id = top_rpps.efd_id
