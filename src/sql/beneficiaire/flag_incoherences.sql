UPDATE beneficiaire_extended benex
SET incoherence_group = inco.group
FROM beneficiaire_dedup_incoherences AS inco
WHERE benex.efd_id = inco.efd_id
