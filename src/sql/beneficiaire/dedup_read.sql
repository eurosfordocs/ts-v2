SELECT
    benex.efd_id::VARCHAR AS efd_id,
    benex.adeli,
    benex.rpps,
    benex.rna,
    benex.siren,
    benex.internal_mds,
    benex.internal_iqvia,
    benex.internal_chiesi,
    benex.manual_origin::VARCHAR AS manual_origin,
    benex.acronyme,
    benex.same_name_commune_profession_origin::VARCHAR AS same_name_commune_profession_origin,
    benex.same_name_origin::VARCHAR AS same_name_origin,
    benex.same_name_no_inco_origin::VARCHAR AS same_name_no_inco_origin,
    CASE WHEN categorie_code = 'VET' THEN benex.ordre END AS ordre
FROM
    beneficiaire_extended AS benex
