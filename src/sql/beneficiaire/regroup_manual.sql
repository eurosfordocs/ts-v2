UPDATE
beneficiaire_extended benex
SET
    manual_origin = regroup.origin_account
FROM
    beneficiaire_regroup_manual AS regroup
WHERE
    regroup.efd_id = benex.efd_id
