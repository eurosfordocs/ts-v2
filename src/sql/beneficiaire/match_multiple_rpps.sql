CREATE
OR REPLACE TABLE beneficiaire_match_multiple_rpps AS (
    WITH multiple_rpps_accounts AS (
        SELECT id_beneficiaire
        FROM
            beneficiaire_extended
        GROUP BY
            1
        HAVING
            COUNT(DISTINCT rpps) > 1
    ),

    matching AS (
        SELECT
            benex.id_beneficiaire,
            benex.nom,
            benex.prenom,
            benex.rpps,
            CAST(
                benex.nom = rpps.nom
                AND benex.prenom = benex.prenom AS INT
            ) AS match_name
        FROM
            beneficiaire_extended AS benex
        LEFT JOIN rpps ON benex.rpps = rpps.identifiant
        INNER JOIN multiple_rpps_accounts ON benex.id_beneficiaire = multiple_rpps_accounts.id_beneficiaire
        WHERE
            rpps IS NOT NULL
    )

    SELECT
        id_beneficiaire,
        rpps,
        MAX(match_name) AS match_rpps
    FROM
        matching
    GROUP BY
        ALL
)
