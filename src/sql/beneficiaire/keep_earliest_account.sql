WITH multiple_rpps_accounts AS (
    SELECT id_beneficiaire
    FROM
        beneficiaire_extended
    WHERE
        categorie_code IN ('PRS', 'VET')
    GROUP BY
        1
    HAVING
        COUNT(DISTINCT rpps) > 1
        OR COUNT(DISTINCT adeli) > 1
        OR COUNT(DISTINCT ordre) > 1
),

earliest AS (
    SELECT
        benex.id_beneficiaire,
        benex.rpps,
        benex.adeli,
        benex.ordre,
        MIN(first_id) AS first_id
    FROM
        beneficiaire_extended AS benex
    INNER JOIN multiple_rpps_accounts AS acc ON benex.id_beneficiaire = acc.id_beneficiaire
    WHERE
        rpps IS NOT NULL
        OR adeli IS NOT NULL
        OR ordre IS NOT NULL
    GROUP BY
        ALL
),

top_rpps AS (
    SELECT
        id_beneficiaire,
        MIN(first_id) AS first_id
    FROM
        earliest
    GROUP BY
        ALL
),

to_remove AS (
    SELECT
        benex.id_beneficiaire,
        benex.rpps,
        benex.adeli,
        benex.ordre
    FROM
        beneficiaire_extended AS benex
    INNER JOIN earliest
        ON
            benex.id_beneficiaire = earliest.id_beneficiaire
            AND (
                (
                    benex.rpps IS NOT NULL
                    AND benex.rpps = earliest.rpps
                )
                OR (
                    benex.adeli IS NOT NULL
                    AND benex.adeli = earliest.adeli
                )
                OR (
                    benex.ordre IS NOT NULL
                    AND benex.ordre = earliest.ordre
                )
            )
    INNER JOIN top_rpps ON benex.id_beneficiaire = top_rpps.id_beneficiaire
    WHERE
        earliest.first_id > top_rpps.first_id
)

UPDATE
beneficiaire_extended benex
SET
    rpps = NULL,
    adeli = NULL,
    ordre = NULL
FROM
    to_remove
WHERE
    to_remove.id_beneficiaire = benex.id_beneficiaire
    AND (
        (
            benex.rpps IS NOT NULL
            AND to_remove.rpps = benex.rpps
        )
        OR (
            benex.adeli IS NOT NULL
            AND benex.adeli = to_remove.adeli
        )
        OR (
            benex.ordre IS NOT NULL
            AND benex.ordre = to_remove.ordre
        )
    )
