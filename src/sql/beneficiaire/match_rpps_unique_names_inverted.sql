WITH unique_names AS (
    SELECT
        nom,
        prenom,
        MIN(origin_account) AS origin_account
    FROM
        rpps
    GROUP BY
        ALL
    HAVING
        COUNT(DISTINCT origin_account) = 1
),

no_id_benex AS (
    SELECT efd_id
    FROM
        beneficiaire_extended
    WHERE
        nom IS NOT NULL
        AND prenom IS NOT NULL
        AND categorie_code = 'PRS'
    GROUP BY
        1
    HAVING
        COUNT(DISTINCT rpps) = 0
        AND COUNT(DISTINCT adeli) = 0
),

accounts_with_matching_ids AS (
    SELECT
        benex.efd_id,
        benex.nb_decla,
        unique_names.origin_account
    FROM
        beneficiaire_extended AS benex
    INNER JOIN
        unique_names
        ON
            lower(benex.nom) = lower(unique_names.prenom)
            AND lower(benex.prenom) = lower(unique_names.nom)
    INNER JOIN no_id_benex ON benex.efd_id = no_id_benex.efd_id
),

-- because sometimes first and last name are inverted, and a real beneficiary correspond to the inverted,
-- the same account can get 2 rpps, which generates instabilities when joining with other tables.
top_rpps AS (
    SELECT
        efd_id,
        origin_account
    FROM
        accounts_with_matching_ids
    GROUP BY
        1, 2
    QUALIFY ROW_NUMBER() OVER (PARTITION BY efd_id ORDER BY SUM(nb_decla) DESC) = 1
)

UPDATE
beneficiaire_extended benex
SET
    rpps = CASE WHEN LEN(top_rpps.origin_account) = 11 THEN top_rpps.origin_account END,
    adeli = CASE WHEN LEN(top_rpps.origin_account) = 9 THEN top_rpps.origin_account END
FROM
    top_rpps
WHERE
    benex.efd_id = top_rpps.efd_id
