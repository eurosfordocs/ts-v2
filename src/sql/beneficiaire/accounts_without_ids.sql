WITH no_id AS (
    SELECT efd_id
    FROM
        beneficiaire_extended
    WHERE
        nom IS NOT NULL
        AND prenom IS NOT NULL
        AND categorie_code = 'PRS'
    GROUP BY
        1
    HAVING
        COUNT(DISTINCT rpps) = 0
        AND COUNT(DISTINCT adeli) = 0
)

SELECT COUNT(*)
FROM no_id
