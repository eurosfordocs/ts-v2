CREATE
OR REPLACE TABLE beneficiaire_extended AS (
    SELECT
        efd_id,
        entreprise_id,
        identite AS nom,
        prenom,
        beneficiaire_categorie_code AS categorie_code,
        beneficiaire_type_code AS type_code,
        UPPER(beneficiaire_identifiant) AS identifiant,
        profession_libelle AS profession,
        com_code AS commune_code,
        code_postal,
        CAST(NULL AS UBIGINT) AS origin_account,
        CAST(NULL AS UBIGINT) AS same_name_commune_profession_origin,
        CAST(NULL AS UBIGINT) AS manual_origin,
        CAST(NULL AS UBIGINT) AS same_name_origin,
        CAST(NULL AS UBIGINT) AS same_name_no_inco_origin,
        CAST(NULL AS VARCHAR(9)) AS siren,
        CAST(NULL AS VARCHAR) AS rpps,
        CAST(NULL AS VARCHAR) AS adeli,
        CAST(NULL AS VARCHAR) AS ordre,
        CAST(NULL AS VARCHAR) AS rna,
        CAST(NULL AS VARCHAR) AS internal_mds,
        CAST(NULL AS VARCHAR) AS internal_iqvia,
        CAST(NULL AS VARCHAR) AS internal_chiesi,
        CAST(NULL AS VARCHAR) AS structure,
        CAST(NULL AS VARCHAR) AS acronyme,
        COUNT(*) AS nb_decla,
        MIN(id) AS first_id,
        CAST(NULL AS INT) AS incoherence_group
    FROM
        main.transform_declaration
    GROUP BY
        ALL
)
