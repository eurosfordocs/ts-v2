CREATE OR REPLACE TABLE raw_beneficiaire AS (
    WITH rpps AS (
        SELECT DISTINCT
            origin_account,
            meta_specialite
        FROM (SELECT * FROM '$clean_datadir/rpps.parquet')
    ),
    core as (
        SELECT
            efd_id,
            MODE(
                CASE WHEN beneficiaire_type_code IN ('ADELI', 'RPPS') THEN beneficiaire_identifiant END
            ) AS rpps_id,
            CAST(NULL AS INT) AS origin_account,
        FROM
            raw_declaration
        group by all
    )
    select
        core.*,
        rpps.meta_specialite AS specialite
    from core
    LEFT JOIN rpps ON rpps_id = rpps.origin_account
);
