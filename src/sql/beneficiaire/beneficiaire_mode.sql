WITH top_id AS (
    SELECT
        $ident,
        COALESCE(origin_account, efd_id) AS origin_account,
        SUM(nb_decla) AS tot_decla
    FROM beneficiaire_extended
    GROUP BY ALL
),

optimal AS (
    SELECT DISTINCT ON (origin_account)
        origin_account,
        $ident
    FROM top_id
    ORDER BY tot_decla DESC
)

UPDATE beneficiaire
SET $ident = optimal.$ident
FROM optimal
WHERE COALESCE(beneficiaire.origin_account, beneficiaire.efd_id) = optimal.origin_account
