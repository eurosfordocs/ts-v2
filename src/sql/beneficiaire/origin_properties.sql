CREATE
OR REPLACE TABLE beneficiaire_origin_properties AS (
    SELECT
        dup.efd_id,
        dup.origin_account,
        COALESCE(
            origin.efd_id, dup.efd_id
        ) AS id_account,
        COALESCE(origin.nom, dup.nom) AS nom,
        COALESCE(origin.prenom, dup.prenom) AS prenom,
        COALESCE(
            origin.categorie_code, dup.categorie_code
        ) AS categorie_code,
        COALESCE(
            origin.profession, dup.profession
        ) AS profession,
        COALESCE(origin.siren, dup.siren) AS siren,
        COALESCE(origin.rpps, dup.rpps) AS rpps,
        COALESCE(origin.adeli, dup.adeli) AS adeli,
        COALESCE(origin.ordre, dup.ordre) AS ordre,
        COALESCE(origin.rna, dup.rna) AS rna,
        COALESCE(
            origin.specialite, dup.specialite
        ) AS specialite
    FROM
        beneficiaire AS dup
    LEFT JOIN beneficiaire AS origin ON dup.origin_account = origin.efd_id
)
