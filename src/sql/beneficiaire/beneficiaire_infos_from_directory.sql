-- update infos from rpps
WITH sirene AS (
    SELECT *
    FROM '$sirene_fn'
),

professionnels AS (
    SELECT
        origin_account,
        meta_nom,
        meta_prenom,
        meta_profession,
        meta_specialite
    FROM (
        SELECT
            origin_account,
            meta_nom,
            meta_prenom,
            meta_profession,
            meta_specialite,
            ROW_NUMBER() OVER (PARTITION BY origin_account ORDER BY LEN(nom), nom) AS rnk
        FROM rpps
    ) AS subquery
    WHERE rnk = 1
)

UPDATE beneficiaire
SET
    nom = professionnels.meta_nom,
    prenom = professionnels.meta_prenom,
    profession = professionnels.meta_profession,
    specialite = professionnels.meta_specialite,
    verified_id = TRUE
FROM professionnels
WHERE
    (beneficiaire.rpps IS NOT NULL OR beneficiaire.adeli IS NOT NULL)
    AND professionnels.origin_account = COALESCE(beneficiaire.rpps, beneficiaire.adeli);


-- update infos from sirene
WITH sirene AS (
    SELECT *
    FROM '$sirene_fn'
)

UPDATE beneficiaire
SET
    nom = sirene.nom,
    verified_id = TRUE
FROM sirene
WHERE
    sirene.siren = beneficiaire.siren
