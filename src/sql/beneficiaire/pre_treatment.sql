-- identifiant: remove brackets from values: [10003757720] > 10003757720
UPDATE
beneficiaire_extended
SET
    identifiant = RIGHT(
        LEFT(identifiant, -1),
        -1
    )
WHERE
    identifiant LIKE '[%]';

-- Remove spaces from identifiant
UPDATE
beneficiaire_extended
SET
    identifiant = UPPER(
        REPLACE(identifiant, ' ', '')
    )
WHERE
    identifiant LIKE '% %';

-- Remove garbage values for identifiant ('N/A', '0', etc)
-- Remove identifiants shorter than 3 chars if they are not from type 'ORDRE'
-- Remove code ROUXXXX from ROUSSILHE SAS which is not unique per beneficiary
UPDATE
beneficiaire_extended
SET
    type_code = NULL,
    identifiant = NULL
WHERE
    identifiant IN ($null_identifiant)
    OR (
        LEN(identifiant) < 3
        AND type_code <> 'ORDRE'
    )
    OR identifiant SIMILAR TO '(ROU|UJE)\d{1,9}'
    OR identifiant SIMILAR TO '.*E\+[0-9].*';

-- Update type_identifiant and identifiant to NULL if type is RPPS/ADELI an len(identifiant) is neither 11 nor 9
UPDATE
beneficiaire_extended
SET
    identifiant = NULL
WHERE
    type_code = 'RPPS/ADELI'
    AND LEN(identifiant) NOT IN (9, 11);

-- Correct RNA filled as SIREN
UPDATE
beneficiaire_extended
SET
    type_code = 'RNA'
WHERE
    type_code = 'SIREN'
    AND identifiant SIMILAR TO 'W.*';

-- Correct siren
UPDATE
beneficiaire_extended
SET
    identifiant = CASE
        WHEN
            LENGTH(identifiant) < 9
            AND LUHN_VERIFY(
                LPAD(identifiant, 9, '0'),
                FALSE
            ) THEN LPAD(identifiant, 9, '0') WHEN LENGTH(identifiant) >= 9
        AND LUHN_VERIFY(
            LEFT(identifiant, 9),
            FALSE
        ) THEN LEFT(identifiant, 9)
    END
WHERE
    type_code = 'SIREN';

--Remove any value from type_code if identifiant is null
UPDATE
beneficiaire_extended
SET
    type_code = NULL
WHERE
    identifiant IS NULL;
-- Update type_identifiant to RPPS or ADELI depending of length
UPDATE
beneficiaire_extended
SET
    type_code = CASE WHEN LEN(identifiant) = 11 THEN 'RPPS' ELSE 'ADELI' END
WHERE
    type_code = 'RPPS/ADELI';

-- Set RNA if autre contains the keyword
UPDATE
beneficiaire_extended d
SET
    identifiant = REGEXP_EXTRACT(d.identifiant, 'W\d{9}'),
    type_code = 'RNA'
WHERE
    type_code = 'AUTRE'
    AND d.identifiant SIMILAR TO '.*W\d{9}.*';

-- Set RNA pour les outre mer
UPDATE
beneficiaire_extended d
SET
    identifiant = REGEXP_EXTRACT(d.identifiant, 'W9[A-Z]\d{7}'),
    type_code = 'RNA'
WHERE
    type_code = 'AUTRE'
    AND d.identifiant SIMILAR TO '.*W9[A-Z]\d{7}.*';

-- Set ADELI if autre contains the keyword
UPDATE
beneficiaire_extended d
SET
    identifiant = REGEXP_EXTRACT(d.identifiant, '\d{9}'),
    type_code = 'ADELI'
WHERE
    type_code = 'AUTRE'
    AND d.identifiant LIKE '%ADELI%'
    AND d.identifiant SIMILAR TO '.*\d{9}.*';

-- Set ADELI DOMTOM if autre contains the keyword
UPDATE
beneficiaire_extended d
SET
    identifiant = REGEXP_EXTRACT(d.identifiant, '9[A-Z]\d{7}'),
    type_code = 'ADELI'
WHERE
    type_code = 'AUTRE'
    AND d.identifiant LIKE '%ADELI%'
    AND d.identifiant SIMILAR TO '.*9[A-Z]\d{7}.*';

-- Set RPPS if autre contains the keyword
UPDATE
beneficiaire_extended d
SET
    identifiant = REGEXP_EXTRACT(d.identifiant, '\d{11}'),
    type_code = 'RPPS'
WHERE
    type_code = 'AUTRE'
    AND d.identifiant LIKE '%RPPS%'
    AND d.identifiant SIMILAR TO '.*\d{11}.*';

-- Set SIREN if keyword in identifiant
UPDATE
beneficiaire_extended
SET
    identifiant = CASE WHEN LUHN_VERIFY(
        LEFT(
            REGEXP_EXTRACT(identifiant, '\d{9,14}'),
            9
        ),
        FALSE
    ) THEN LEFT(
        REGEXP_EXTRACT(identifiant, '\d{9,14}'),
        9
    ) END,
    type_code = 'SIREN'
WHERE
    type_code = 'AUTRE'
    AND (
        identifiant SIMILAR TO '.*(SIRE[TN]).*'
        OR LOWER(REPLACE(identifiant, '.', '')) LIKE '%rcs%'
    )
    AND identifiant SIMILAR TO '.*\d{9,14}.*';

-- Set FINESS if keyword in identifiant
UPDATE
beneficiaire_extended
SET
    identifiant = REGEXP_EXTRACT(identifiant, '\d{8,9}'),
    type_code = 'FINESS'
WHERE
    type_code = 'AUTRE'
    AND identifiant LIKE '%FINESS%'
    AND identifiant SIMILAR TO '.*\d{8,9}.*';

-- Set FINESS from Corse if keyword in identifiant
UPDATE
beneficiaire_extended
SET
    identifiant = REGEXP_EXTRACT(identifiant, '2[AB]\d{7}'),
    type_code = 'FINESS'
WHERE
    type_code = 'AUTRE'
    AND identifiant LIKE '%FINESS%'
    AND identifiant SIMILAR TO '.*2[AB]\d{7}.*';
-- Set SIREN if 14 digits and luhn
UPDATE
beneficiaire_extended
SET
    identifiant = LEFT(
        REGEXP_EXTRACT(identifiant, '\d{14}'),
        9
    ),
    type_code = 'SIREN'
WHERE
    type_code = 'AUTRE'
    AND identifiant SIMILAR TO '\d{14}'
    AND LUHN_VERIFY(
        REGEXP_EXTRACT(identifiant, '\d{14}'),
        FALSE
    )
    AND LUHN_VERIFY(
        LEFT(
            REGEXP_EXTRACT(identifiant, '\d{14}'),
            9
        ),
        FALSE
    );
-- Certaines entreprises semblent utiliser le meme code interne pour identifier
-- les bénéficiaires meme avec des id_beneficiaires différents.
UPDATE
beneficiaire_extended
SET
    type_code = 'INTERNAL_MDS',
    identifiant = identifiant
WHERE
    type_code = 'AUTRE'
    AND identifiant SIMILAR TO 'WFR[A-Z]\d{1,}'
    OR identifiant SIMILAR TO 'WPFX\d{1,}';
-- Ajoute le code interne de iqvia
UPDATE
beneficiaire_extended
SET
    type_code = 'INTERNAL_IQVIA',
    identifiant = identifiant
WHERE
    type_code = 'AUTRE'
    AND identifiant SIMILAR TO 'FRA\d+';
-- Ajoute le code interne de chiesi
UPDATE
beneficiaire_extended
SET
    type_code = 'INTERNAL_CHIESI',
    identifiant = identifiant
WHERE
    type_code = 'AUTRE'
    AND identifiant LIKE 'CHIESI-%';
-- Corrige certaines formulations de professions
WITH correct_profession AS (
    SELECT *
    FROM
        (
            VALUES
            (
                'audioprothésiste', 'audio-prothésiste'
            ),
            (
                'technicien de laboratoire médical',
                'technicien de laboratoire'
            ),
            (
                'manipulateur d''électroradiologie médicale',
                'manipulateur erm'
            ),
            (
                'auxiliaire de puériculture', 'infirmier'
            ),
            (
                'physicien médical', 'médecin'
            ),
            ('aide soignant', 'infirmier')
        ) AS t(
      profession_declared, profession_rpps
    )

)

UPDATE
beneficiaire_extended
SET
    profession = profession_rpps
FROM
    correct_profession
WHERE
    profession = profession_declared;


-- Set PRS
UPDATE
beneficiaire_extended
SET categorie_code = 'PRS'
WHERE
    categorie_code NOT IN ('ETU', 'PRS', 'VET', 'PRE')
    AND profession IS NOT NULL
    AND profession NOT IN ('editeur de presse')
    AND prenom IS NOT NULL;

-- Uniformise les informations de vétérinaire
UPDATE
beneficiaire_extended
SET
    profession = 'vétérinaire',
    categorie_code = 'VET'
WHERE
    categorie_code = 'VET'
    OR profession = 'vétérinaire';


-- Uniformise les informations de presse
UPDATE
beneficiaire_extended
SET
    profession = 'editeur de presse',
    categorie_code = 'PRE'
WHERE
    categorie_code = 'PRE'
    OR profession = 'editeur de presse';


-- Correct multiple siren for same entity
-- for example a company can have holding + daughter company
WITH match_siren AS (
    SELECT
        LPAD(siren_to_replace, 9, '0') AS siren_to_replace,
        LPAD(siren, 9, '0') AS siren
    FROM READ_CSV_AUTO(
        '$siren_match_url',
        HEADER=TRUE,
        COLUMNS={ 'siren_to_replace': 'VARCHAR', 'siren': 'VARCHAR', 'commentaire': 'VARCHAR' }
    )
)

UPDATE beneficiaire_extended benex
SET
    identifiant = match_siren.siren
FROM match_siren
WHERE
    type_code = 'SIREN'
    AND LPAD(benex.identifiant, 9, '0') = match_siren.siren_to_replace;

-- Add identifiant
UPDATE
beneficiaire_extended
SET
    siren = CASE
        WHEN
            type_code = 'SIREN'
            AND LUHN_VERIFY(identifiant, FALSE) THEN identifiant
    END,
    rpps = CASE
        WHEN
            type_code = 'RPPS'
            AND LUHN_VERIFY(identifiant, FALSE) THEN identifiant
    END,
    adeli = CASE
        WHEN
            type_code = 'ADELI'
            AND LUHN_VERIFY(identifiant, TRUE) THEN identifiant
    END,
    ordre = CASE
        WHEN
            type_code = 'ORDRE'
            AND profession = 'vétérinaire' THEN identifiant
    END,
    rna = CASE WHEN type_code = 'RNA' THEN identifiant END,
    internal_mds = CASE WHEN type_code = 'INTERNAL_MDS' THEN identifiant END,
    internal_iqvia = CASE WHEN type_code = 'INTERNAL_IQVIA' THEN identifiant END,
    internal_chiesi = CASE WHEN type_code = 'INTERNAL_CHIESI' THEN identifiant END;


-- Corrige quelques noms d associations
UPDATE beneficiaire_extended
SET nom = REGEXP_EXTRACT(nom, 'CENTRE.*')
WHERE nom SIMILAR TO 'LE CENTRE.*';

-- Ajoute le siren a partir du finess
UPDATE beneficiaire_extended
SET
  siren = finess.siren,
  categorie_code = 'ETA'
FROM finess
WHERE type_code = 'FINESS' AND LUHN_VERIFY(identifiant, TRUE) AND LPAD(identifiant, 9, '0') = num_finess_et;

-- Ajoute le siren à partir du finess ju
WITH etab_ju AS (
    SELECT
        num_finess_ju,
        FIRST(siren) AS siren
  FROM finess
  GROUP BY 1
)

UPDATE beneficiaire_extended
SET
  siren = etab_ju.siren,
  categorie_code = 'ETA'
FROM etab_ju
WHERE type_code = 'FINESS' AND LUHN_VERIFY(identifiant, TRUE) AND LPAD(identifiant, 9, '0') = num_finess_ju;

-- Correct siren from finess
update beneficiaire_extended benex
SET siren = origin
from finess_regroup_siren where benex.siren = finess_regroup_siren.siren;
