WITH multiple_rpps_accounts AS (
    SELECT efd_id
    FROM
        beneficiaire_extended
    WHERE
        categorie_code = 'PRS'
    GROUP BY
        1
    HAVING
        COUNT(DISTINCT rpps) > 1
        OR COUNT(DISTINCT adeli) > 1
)

UPDATE beneficiaire_extended benex
SET
    rpps = NULL,
    adeli = NULL
FROM multiple_rpps_accounts AS dups
WHERE benex.efd_id = dups.efd_id
