WITH dups AS (
    SELECT efd_id
    FROM beneficiaire_extended
    WHERE categorie_code IN ('PRS', 'VET', 'ETU')
    GROUP BY ALL
    HAVING COUNT(DISTINCT siren) > 0
)

UPDATE beneficiaire_extended b
SET
    siren = NULL,
    structure = b.siren
FROM dups
WHERE dups.efd_id = b.efd_id AND b.siren IS NOT NULL
