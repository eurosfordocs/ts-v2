CREATE
OR REPLACE TABLE beneficiaire AS (
    SELECT DISTINCT ON (efd_id)
        efd_id,
        origin_account,
        NULL::VARCHAR AS categorie_code,
        NULL::VARCHAR AS nom,
        NULL::VARCHAR AS prenom,
        NULL::VARCHAR AS siren,
        NULL::VARCHAR AS rna,
        NULL::VARCHAR AS rpps,
        NULL::VARCHAR AS adeli,
        NULL::VARCHAR AS ordre,
        NULL::VARCHAR AS profession,
        NULL::VARCHAR AS specialite,
        FALSE::BOOLEAN AS verified_id,
        NULL::INT AS incoherence_group
    FROM beneficiaire_extended
);
