WITH dups AS (
    SELECT efd_id
    FROM beneficiaire_extended
    GROUP BY ALL
    HAVING COUNT(DISTINCT rpps) > 0 AND COUNT(DISTINCT siren) > 0
),

hospital_worker AS (
    SELECT DISTINCT
        b.efd_id,
        b.siren
    FROM beneficiaire_extended AS b
    INNER JOIN dups ON b.efd_id = dups.efd_id
    INNER JOIN finess USING (siren)
)

UPDATE beneficiaire_extended b
SET siren = NULL
FROM hospital_worker AS hw
WHERE hw.efd_id = b.efd_id AND b.siren = hw.siren
