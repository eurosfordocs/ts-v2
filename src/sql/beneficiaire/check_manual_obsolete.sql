SELECT DISTINCT regroup.efd_id
FROM
    beneficiaire_regroup_manual AS regroup
INNER JOIN beneficiaire_extended AS benex ON regroup.efd_id = benex.efd_id
WHERE
    COALESCE(
        manual_origin, same_name_commune_profession_origin
    ) IS NOT NULL
