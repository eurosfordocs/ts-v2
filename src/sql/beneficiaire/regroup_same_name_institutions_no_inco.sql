WITH same_name AS (
    SELECT
        nom,
        MIN(efd_id) AS origin_account
    FROM
        beneficiaire_extended
    WHERE
        categorie_code not in ('PRS', 'VET', 'ETU', 'INF')
    GROUP BY ALL
    HAVING
        COUNT(DISTINCT efd_id) > 1
        AND COUNT(DISTINCT siren) <= 1
        AND COUNT(DISTINCT rna) <= 1
)

UPDATE beneficiaire_extended benex
SET same_name_no_inco_origin = same_name.origin_account
FROM same_name
WHERE
    same_name.nom = benex.nom
    AND categorie_code not in ('PRS', 'VET', 'ETU', 'INF')
    AND same_name.origin_account <> efd_id
