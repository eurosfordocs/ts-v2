WITH cast_dedup AS (
    SELECT
        efd_id::UBIGINT AS efd_id,
        origin::UBIGINT AS origin
    FROM beneficiaire_dedup_origin
)

UPDATE
main.beneficiaire_extended
SET
    origin_account = dedup.origin
FROM
    beneficiaire_dedup_origin AS dedup
WHERE
    beneficiaire_extended.efd_id = dedup.efd_id
