WITH similars AS (
    SELECT
        nom,
        prenom,
        commune_code,
        profession,
        MIN(efd_id) AS origin_account
    FROM
        main.beneficiaire_extended
    WHERE
        commune_code IS NOT NULL
        AND prenom IS NOT NULL
        AND categorie_code IN ('PRS', 'VET')
    GROUP BY
        ALL
    HAVING
        COUNT(DISTINCT efd_id) > 1
        AND COUNT(DISTINCT rpps) <= 1
        AND COUNT(DISTINCT adeli) <= 1
        AND COUNT(DISTINCT ordre) <= 1
)

UPDATE
beneficiaire_extended ben
SET
    same_name_commune_profession_origin = similars.origin_account
FROM
    similars
WHERE
    ben.nom = similars.nom
    AND ben.prenom = similars.prenom
    AND ben.commune_code = similars.commune_code
    AND ben.profession = similars.profession
    AND ben.efd_id <> similars.origin_account
