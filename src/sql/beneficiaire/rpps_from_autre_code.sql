-- Des identifiants 'AUTRE' peuvent être des rpps.
-- Pour les identifier, on join à identifiant la table rpps
-- et on applique des filtres de similitude sur le nom et le prénom
WITH matched AS (
    SELECT
        benex.efd_id,
        benex.nom,
        benex.prenom,
        benex.identifiant,
        rpps.nom AS rpps_nom,
        rpps.prenom AS rpps_prenom
    FROM beneficiaire_extended AS benex
    INNER JOIN rpps ON benex.identifiant = rpps.rpps
    WHERE
        benex.type_code = 'AUTRE'
        AND benex.rpps IS NULL
        AND benex.categorie_code = 'PRS'
        AND benex.adeli IS NULL
),

correct_rpps AS (
    SELECT DISTINCT
        efd_id,
        identifiant
    FROM matched
    WHERE
        nom = rpps_nom
        OR (LOWER(nom) = LOWER(rpps_prenom) AND LOWER(rpps_nom) = LOWER(prenom))
        OR (
            prenom = rpps_prenom
            AND (nom LIKE CONCAT('%', rpps_nom, '%') OR rpps_nom LIKE CONCAT('%', nom, '%'))
            OR (JARO_WINKLER_SIMILARITY(rpps_nom, nom) + JARO_WINKLER_SIMILARITY(REVERSE(rpps_nom), REVERSE(nom))) / 2
            > 0.9
            OR JARO_WINKLER_SIMILARITY(rpps_nom, ARRAY_TO_STRING(ARRAY_REVERSE(SPLIT(nom, ' ')), ' ')) > 0.9
        )
)

UPDATE beneficiaire_extended benex
SET rpps = benex.identifiant
FROM correct_rpps
WHERE
    benex.identifiant = correct_rpps.identifiant
    AND benex.efd_id = correct_rpps.efd_id
    AND benex.rpps IS NULL
    AND categorie_code = 'PRS'
    AND benex.adeli IS NULL;


WITH matched AS (
    SELECT
        benex.efd_id,
        benex.nom,
        benex.prenom,
        benex.identifiant,
        rpps.nom AS rpps_nom,
        rpps.prenom AS rpps_prenom
    FROM beneficiaire_extended AS benex
    INNER JOIN rpps ON benex.identifiant = rpps.adeli
    WHERE
        benex.type_code = 'AUTRE'
        AND benex.rpps IS NULL
        AND benex.categorie_code = 'PRS'
        AND benex.adeli IS NULL
),

correct_rpps AS (
    SELECT DISTINCT
        efd_id,
        identifiant
    FROM matched
    WHERE
        nom = rpps_nom
        OR (LOWER(nom) = LOWER(rpps_prenom) AND LOWER(rpps_nom) = LOWER(prenom))
        OR (
            prenom = rpps_prenom
            AND (nom LIKE CONCAT('%', rpps_nom, '%') OR rpps_nom LIKE CONCAT('%', nom, '%'))
            OR (JARO_WINKLER_SIMILARITY(rpps_nom, nom) + JARO_WINKLER_SIMILARITY(REVERSE(rpps_nom), REVERSE(nom))) / 2
            > 0.9
            OR JARO_WINKLER_SIMILARITY(rpps_nom, ARRAY_TO_STRING(ARRAY_REVERSE(SPLIT(nom, ' ')), ' ')) > 0.9
        )
)

UPDATE beneficiaire_extended benex
SET adeli = benex.identifiant
FROM correct_rpps
WHERE
    benex.identifiant = correct_rpps.identifiant
    AND benex.efd_id = correct_rpps.efd_id
    AND benex.rpps IS NULL
    AND categorie_code = 'PRS'
    AND benex.adeli IS NULL;
