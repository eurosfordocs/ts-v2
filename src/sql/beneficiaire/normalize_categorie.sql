-- Normalize categorie
WITH matching AS (
    SELECT
        beneficiaire_categorie_code,
        beneficiaire_categorie
    FROM READ_CSV_AUTO('$categorie_matching_url', header = TRUE)
)

UPDATE transform_declaration td
SET beneficiaire_categorie = matching.beneficiaire_categorie
FROM matching
WHERE td.beneficiaire_categorie_code = matching.beneficiaire_categorie_code;
