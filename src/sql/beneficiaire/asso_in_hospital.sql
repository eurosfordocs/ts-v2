WITH has_account_rna AS (
    SELECT
        b.$ident,
        COUNT(DISTINCT finess.num_finess_ju) AS is_hospital,
        COUNT(DISTINCT b.rna) AS nb_rna
    FROM beneficiaire_extended AS b
    LEFT JOIN finess ON b.siren = finess.siren
    WHERE b.$ident IS NOT NULL
    GROUP BY ALL
    HAVING nb_rna = 1 AND is_hospital > 0
),

to_update AS (
    SELECT DISTINCT
        benex.$ident,
        benex.siren
    FROM beneficiaire_extended AS benex
    INNER JOIN has_account_rna ON benex.$ident = has_account_rna.$ident
    INNER JOIN finess ON benex.siren = finess.siren
    WHERE finess.siren IS NOT NULL
)

UPDATE beneficiaire_extended benex
SET
    siren = NULL,
    structure = benex.siren
FROM to_update
WHERE
    to_update.siren = benex.siren
    AND benex.$ident = to_update.$ident
