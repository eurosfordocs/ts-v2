WITH accros AS (
    SELECT
        nom,
        REGEXP_EXTRACT(nom, '\(((?:[A-Z]\.?){3,}[A-Z]\.?)\)', 1) AS acro_par,
        REGEXP_EXTRACT(nom, '((?:[A-Z]\.){3,}[A-Z]\.?)', 1) AS acro_simple
    FROM beneficiaire_extended
),

enriched AS (
    SELECT
        *,
        REPLACE(
            CASE
                WHEN acro_par != '' THEN acro_par
                WHEN acro_simple != '' THEN acro_simple
            END, '.', ''
        ) AS acronyme
    FROM accros
)

UPDATE beneficiaire_extended benex
SET acronyme = enriched.acronyme
FROM enriched
WHERE
    enriched.nom = benex.nom
    AND enriched.acronyme NOT IN ('EURL', 'SARL', 'SASU', 'SELARL')
