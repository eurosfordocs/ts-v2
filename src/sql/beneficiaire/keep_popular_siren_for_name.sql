WITH multiple_ident_accounts AS (
    SELECT
        efd_id,
        nom
    FROM
        beneficiaire_extended
    WHERE
        nom IS NOT NULL
        AND LENGTH(nom) > 2
    GROUP BY ALL
    HAVING
        COUNT(DISTINCT $ident) > 1
),

ident_occurences AS (
    SELECT
        benex.nom,
        benex.$ident,
        COUNT(benex.efd_id) AS popularity
    FROM beneficiaire_extended AS benex
    INNER JOIN multiple_ident_accounts AS acc ON benex.nom = acc.nom
    WHERE $ident IS NOT NULL
    GROUP BY ALL
),

most_popular AS (
    SELECT
        nom,
        $ident,
        popularity,
        MAX(popularity) OVER (PARTITION BY nom) AS max_popularity
    FROM ident_occurences
),

to_correct AS (
    SELECT
        mia.efd_id,
        mp.$ident
    FROM multiple_ident_accounts AS mia
    INNER JOIN most_popular AS mp ON mia.nom = mp.nom
    WHERE mp.popularity = mp.max_popularity
)

UPDATE beneficiaire_extended benex
SET $ident = NULL
FROM to_correct
WHERE
    to_correct.efd_id = benex.efd_id
    AND to_correct.$ident != benex.$ident
    AND benex.$ident IS NOT NULL
