WITH multiple_ids_accounts AS (
    SELECT id_beneficiaire
    FROM
        beneficiaire_extended
    GROUP BY
        1
    HAVING
        COUNT(DISTINCT rpps) > 1
),

popularity AS (
    SELECT
        benex.id_beneficiaire,
        benex.$ident,
        SUM(nb_decla) AS nb_decla
    FROM
        beneficiaire_extended AS benex
    INNER JOIN multiple_ids_accounts ON benex.id_beneficiaire = multiple_ids_accounts.id_beneficiaire
WHERE
    rpps IS NOT NULL
GROUP BY
    ALL
),

top_ids AS (
SELECT
    id_beneficiaire,
    MAX(nb_decla) AS nb_decla
FROM
    popularity
GROUP BY
    ALL
),

to_remove AS (
SELECT
    benex.id_beneficiaire,
    benex.$ident
FROM beneficiaire_extended AS benex
INNER JOIN popularity
    ON
        benex.id_beneficiaire = popularity.id_beneficiaire
        AND benex.$ident IS NOT NULL
        AND benex.$ident = popularity.$ident
INNER JOIN top_ids ON benex.id_beneficiaire = top_ids.id_beneficiaire
WHERE
    popularity.nb_decla < top_ids.nb_decla
)

UPDATE
beneficiaire_extended benex
SET
rpps = NULL
FROM
to_remove
WHERE
to_remove.id_beneficiaire = benex.id_beneficiaire
AND benex.$ident IS NOT NULL
AND to_remove.$ident = benex.$ident
