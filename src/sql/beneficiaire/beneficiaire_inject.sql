UPDATE
  transform_declaration
SET
  $source = origin_dup_account.$target,
FROM
  beneficiaire_origin_properties origin_dup_account
WHERE
  transform_declaration.efd_id = origin_dup_account.efd_id
