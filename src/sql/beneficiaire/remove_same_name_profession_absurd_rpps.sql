WITH similars AS (
    SELECT
        nom,
        prenom,
        code_postal,
        profession,
        MIN(efd_id) AS origin_account
    FROM beneficiaire_extended
    WHERE
        nom IS NOT NULL
        AND prenom IS NOT NULL
        AND code_postal IS NOT NULL
        AND profession IS NOT NULL
        AND categorie_code = 'PRS'
    GROUP BY ALL
    HAVING
        COUNT(DISTINCT efd_id) > 1
        AND COUNT(DISTINCT $var) > 1
),

benex_incoherence AS (
    SELECT DISTINCT
        benex.efd_id,
        benex.nom,
        benex.prenom,
        benex.code_postal,
        benex.profession,
        benex.$var
    FROM beneficiaire_extended AS benex
    INNER JOIN similars
        ON
            benex.nom = similars.nom
            AND benex.prenom = similars.prenom
            AND benex.code_postal = similars.code_postal
            AND benex.profession = similars.profession
    WHERE benex.$var IS NOT NULL
),

named_match_rpps AS (
    SELECT
        benex.efd_id,
        benex.nom,
        benex.prenom,
        benex.code_postal,
        benex.profession,
        benex.$var,
        rpps.nom AS rpps_nom,
        rpps.prenom AS rpps_prenom,
        rpps.profession AS rpps_profession,
        rpps.code_postal AS rpps_code_postal,
        JARO_WINKLER_SIMILARITY(rpps.nom, benex.nom) AS jaro_nom,
        JARO_WINKLER_SIMILARITY(
            rpps.nom, ARRAY_TO_STRING(ARRAY_REVERSE(SPLIT(benex.nom, ' ')), ' ')
        ) AS jaro_nom_inverted,
        JARO_WINKLER_SIMILARITY(rpps.prenom, benex.prenom) AS jaro_prenom,
        CAST(rpps.profession = benex.profession AS INT) AS same_profession,
        CAST(
            rpps.code_postal = benex.code_postal AS INT
        ) AS same_code_postal,
        CAST(
            LEFT(rpps.code_postal, 2) = LEFT(benex.code_postal, 2) AS INT
        ) AS same_dep
    FROM benex_incoherence AS benex
    LEFT JOIN rpps ON benex.$var = rpps.$var
),

scored AS (
    SELECT
        *,
        COALESCE(jaro_prenom, 0)
        + GREATEST(COALESCE(jaro_nom, 0), COALESCE(jaro_nom_inverted, 0))
        + COALESCE(same_code_postal, 0) * 0.04
        + COALESCE(same_profession, 0) * 0.02
        + COALESCE(same_dep, 0) * 0.01 AS score
    FROM named_match_rpps
),

acceptable_identification AS (
    SELECT
        nom,
        prenom,
        code_postal,
        profession,
        MAX(score) AS max_score
    FROM scored
    GROUP BY ALL
    HAVING max_score > 1.7
),

flagged_errors AS (
    SELECT
        scored.*,
        top_score.max_score
    FROM scored
    INNER JOIN acceptable_identification AS top_score
        ON
            scored.nom = top_score.nom
            AND scored.prenom = top_score.prenom
            AND scored.code_postal = top_score.code_postal
            AND scored.profession = top_score.profession
),


all_correct_ones AS (
    SELECT DISTINCT
        nom,
        prenom,
        code_postal,
        profession,
        $var
    FROM flagged_errors
    WHERE score = max_score
),

to_correct AS (
    SELECT DISTINCT
        f.nom,
        f.prenom,
        f.code_postal,
        f.profession,
        f.$var
    FROM
        flagged_errors AS f
    LEFT JOIN all_correct_ones AS aco
        ON
            f.nom = aco.nom
            AND f.prenom = aco.prenom
            AND f.code_postal = aco.code_postal
            AND f.profession = aco.profession
            AND aco.$var IS NOT NULL
            AND f.$var = aco.$var
    WHERE
        f.score < f.max_score
        AND aco.nom IS NULL
)

UPDATE beneficiaire_extended ben
SET
    $var = NULL
FROM
    to_correct AS cor
WHERE
    ben.nom = cor.nom
    AND ben.prenom = cor.prenom
    AND ben.code_postal = cor.code_postal
    AND ben.profession = cor.profession
    AND ben.$var IS NOT NULL
    AND ben.$var = cor.$var
