WITH multiple_rpps_accounts AS (
    SELECT efd_id
    FROM
        beneficiaire_extended
    WHERE
        categorie_code = 'PRS'
    GROUP BY
        1
    HAVING
        COUNT(DISTINCT $var) > 1
),

benex_incoherence AS (
    SELECT DISTINCT
        benex.efd_id,
        benex.nom,
        benex.prenom,
        benex.code_postal,
        benex.profession,
        benex.rpps,
        benex.adeli
    FROM
        beneficiaire_extended AS benex
    INNER JOIN multiple_rpps_accounts AS mra ON benex.efd_id = mra.efd_id
    WHERE benex.$var IS NOT NULL
),

joined_comined_benex AS (
    SELECT
        benex.*,
        rpps.nom AS rpps_nom,
        rpps.prenom AS rpps_prenom,
        rpps.profession AS rpps_profession,
        rpps.code_postal AS rpps_code_postal
    FROM
        benex_incoherence AS benex
    INNER JOIN rpps
        ON
            benex.$var = rpps.$var
),

benex_sub_scoring AS (
    SELECT
        *,
        JARO_WINKLER_SIMILARITY(rpps_nom, nom) AS jaro_nom,
        JARO_WINKLER_SIMILARITY(rpps_prenom, prenom) AS jaro_prenom,
        JARO_WINKLER_SIMILARITY(LOWER(rpps_nom), LOWER(prenom)) AS jaro_nom_prenom,
        JARO_WINKLER_SIMILARITY(LOWER(rpps_prenom), LOWER(nom)) AS jaro_prenom_nom,
        CAST(
            rpps_profession = profession AS INT
        ) AS same_profession,
        CAST(
            rpps_code_postal = code_postal AS INT
        ) AS same_code_postal,
        CAST(
            LEFT(rpps_code_postal, 2) = LEFT(code_postal, 2) AS INT
        ) AS same_dep
    FROM joined_comined_benex
),

benex_scoring AS (
    SELECT
        *,
        GREATEST(
            COALESCE(jaro_prenom, 0)
            + COALESCE(jaro_nom, 0),
            COALESCE(jaro_prenom_nom, 0)
            + COALESCE(jaro_nom_prenom, 0)
        )
        + COALESCE(same_code_postal, 0) * 0.04
        + COALESCE(same_profession, 0) * 0.02
        + COALESCE(same_dep, 0) * 0.01 AS score
    FROM benex_sub_scoring
),

identified_reference AS (
    SELECT
        efd_id,
        MAX(score) AS max_score
    FROM
        benex_scoring
    GROUP BY
        ALL
    HAVING
        max_score > 1.7
),

flagged_errors AS (
    SELECT
        sim.*,
        identified_reference.max_score,
        COALESCE(sim.score, 0) AS score
    FROM benex_scoring AS sim
    INNER JOIN identified_reference ON sim.efd_id = identified_reference.efd_id
),

all_correct_ones AS (
    SELECT DISTINCT
        efd_id,
        $var
    FROM flagged_errors
    WHERE score = max_score
),

to_correct AS (
    SELECT DISTINCT
        f.efd_id,
        f.$var
    FROM flagged_errors AS f
    LEFT JOIN all_correct_ones AS aco
        ON f.efd_id = aco.efd_id AND f.$var = aco.$var
    WHERE
        f.score < f.max_score
        AND aco.efd_id IS NULL
)

UPDATE
beneficiaire_extended ben
SET
    $var = NULL
FROM
    to_correct AS cor
WHERE
    ben.efd_id = cor.efd_id
    AND ben.$var = cor.$var
