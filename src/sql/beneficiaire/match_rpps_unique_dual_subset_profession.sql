WITH unique_names AS (
    SELECT
        nom,
        prenom,
        commune_code,
        meta_profession AS profession,
        MIN(origin_account) AS origin_account
    FROM
        rpps
    GROUP BY ALL
    HAVING
        COUNT(DISTINCT origin_account) = 1
),


no_id_benex AS (
    SELECT efd_id
    FROM beneficiaire_extended
    WHERE
        nom IS NOT NULL
        AND prenom IS NOT NULL
        AND profession IS NOT NULL
        AND categorie_code IN ('PRS', 'ETU')
        AND nom LIKE '% %'
    GROUP BY ALL
    HAVING
        COUNT(DISTINCT rpps) = 0
        AND COUNT(DISTINCT adeli) = 0
)
,

enriched AS (
    SELECT
        benex.efd_id,
        benex.nom,
        benex.prenom,
        benex.profession,
        benex.nb_decla,
        UNNEST(SPLIT(benex.nom, ' ')) AS nom_subset
    FROM beneficiaire_extended AS benex
    INNER JOIN no_id_benex ON benex.efd_id = no_id_benex.efd_id
    WHERE benex.nom LIKE '% %'
),

accounts_with_matching_ids AS (
    SELECT
        enriched.efd_id,
        unique_names.origin_account,
        enriched.nb_decla
    FROM enriched
    INNER JOIN
        unique_names
        ON
            enriched.nom_subset = unique_names.nom
            AND enriched.prenom = unique_names.prenom
            AND enriched.profession = unique_names.profession
),

top_rpps AS (
    SELECT
        efd_id,
        origin_account
    FROM
        accounts_with_matching_ids
    GROUP BY
        1, 2
    QUALIFY ROW_NUMBER() OVER (PARTITION BY efd_id ORDER BY SUM(nb_decla) DESC) = 1
)

UPDATE
beneficiaire_extended benex
SET
    rpps = CASE WHEN LEN(top_rpps.origin_account) = 11 THEN top_rpps.origin_account END,
    adeli = CASE WHEN LEN(top_rpps.origin_account) = 9 THEN top_rpps.origin_account END
FROM
    top_rpps
WHERE
    benex.efd_id = top_rpps.efd_id;
