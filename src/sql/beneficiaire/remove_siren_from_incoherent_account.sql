WITH multiple_siren_accounts AS (
    SELECT efd_id
    FROM
        beneficiaire_extended
    WHERE
        categorie_code NOT IN ('PRS', 'ETU', 'VET', 'INF')
    GROUP BY
        1
    HAVING
        COUNT(DISTINCT siren) > 1
),

benex_incoherence AS (
    SELECT DISTINCT
        benex.efd_id,
        benex.nom,
        benex.siren
    FROM
        beneficiaire_extended AS benex
    INNER JOIN multiple_siren_accounts ON benex.efd_id = multiple_siren_accounts.efd_id
    WHERE
        benex.siren IS NOT NULL
),

sirene AS (
    SELECT
        *,
        CASE
            WHEN nom = 'ASSISTANCE PUBLIQUE HOPITAUX DE PARIS' THEN 'APHP'
            WHEN nom = 'ASSISTANCE PUBLIQUE HOPITAUX DE MARSEILLE' THEN 'APHM'
            WHEN nom SIMILAR TO 'CENTRE HOSPITALIER (REGIONAL )?(ET )?(UNIVERSITAIRE )?.*'
                THEN REGEXP_REPLACE(nom, 'CENTRE HOSPITALIER (REGIONAL )?(ET )?(UNIVERSITAIRE )?', 'CHU')
        END AS reduced_nom
    FROM '$sirene_fn'
),

benex_scoring AS (
    SELECT
        benex.*,
        sirene.siren AS official_siren,
        GREATEST(
            COALESCE(JARO_WINKLER_SIMILARITY(sirene.reduced_nom, benex.nom), 0),
            COALESCE(JARO_WINKLER_SIMILARITY(sirene.nom, benex.nom), 0)
        ) AS score
    FROM
        benex_incoherence AS benex
    LEFT JOIN sirene ON
        benex.siren = sirene.siren
),

identified_reference AS (
    SELECT
        efd_id,
        MAX(score) AS max_score,
        COUNT(DISTINCT official_siren) AS nb_siren
    FROM
        benex_scoring
    GROUP BY
        ALL
    HAVING
        max_score > 0.8
        -- if only one matches the sirene database,
        -- we will remove unknown ones
        OR nb_siren = 1
),


flagged_errors AS (
    SELECT
        sim.* EXCLUDE (score),
        identified_reference.max_score,
        COALESCE(sim.score, 0) AS score
    FROM
        benex_scoring AS sim
    INNER JOIN identified_reference ON sim.efd_id = identified_reference.efd_id
),


all_correct_ones AS (
    SELECT DISTINCT
        efd_id,
        siren
    FROM flagged_errors
    WHERE score = max_score
),

to_correct AS (
    SELECT DISTINCT
        f.efd_id,
        f.siren
    FROM flagged_errors AS f
    LEFT JOIN all_correct_ones AS aco ON f.efd_id = aco.efd_id AND f.siren = aco.siren
    WHERE
        f.score < f.max_score
        AND aco.efd_id IS NULL
)

UPDATE
beneficiaire_extended ben
SET
    siren = NULL
FROM
    to_correct AS cor
WHERE
    ben.efd_id = cor.efd_id
    AND ben.siren IS NOT NULL
    AND ben.siren = cor.siren
