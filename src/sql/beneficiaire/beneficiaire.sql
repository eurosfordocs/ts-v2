CREATE
OR REPLACE TABLE beneficiaire AS (
    WITH sirene AS (
        SELECT *
        FROM '$sirene_fn'
    ),

    aggs AS (
        SELECT
            COALESCE(origin_account, efd_id) AS origin_account,
            MODE(nom) AS nom,
            MODE(prenom) AS prenom,
            MODE(categorie_code) AS categorie_code,
            MODE(profession) AS profession,
            MODE(origin_account) AS origin_account,
            MODE(siren) AS siren,
            MODE(rpps) AS rpps,
            MODE(adeli) AS adeli,
            MODE(ordre) AS ordre,
            MODE(rna) AS rna,
            MODE(
                CASE WHEN rpps IS NOT NULL THEN rpps WHEN adeli IS NOT NULL THEN adeli END
            ) AS id_rpps
        FROM
            beneficiaire_extended
        GROUP BY
            1
    ),

    unique_rpps AS (
        SELECT
            origin_account,
            meta_nom,
            meta_prenom,
            meta_profession,
            meta_specialite
        FROM rpps
        QUALIFY ROW_NUMBER() OVER (PARTITION BY origin_account ORDER BY LEN(nom), nom, prenom) = 1
    ),

    accounts AS (
        SELECT DISTINCT ON (efd_id)
            efd_id,
            origin_account
        FROM beneficiaire_extended
    ),

    combined AS (
        SELECT
            accounts.efd_id,
            accounts.origin_account,
            categorie_code,
            aggs.siren,
            rpps,
            adeli,
            ordre,
            rna,
            unique_rpps.meta_specialite AS specialite,
            inco.group AS incoherence_group,
            unique_rpps.origin_account IS NOT NULL AS verified_id,
            COALESCE(unique_rpps.meta_nom, sirene.nom, aggs.nom) AS nom,
            COALESCE(unique_rpps.meta_prenom, aggs.prenom) AS prenom,
            COALESCE(
                unique_rpps.meta_profession,
                CASE WHEN categorie_code IN ('ETU', 'PRS', 'VET') THEN aggs.profession END
            ) AS profession
        FROM accounts
        INNER JOIN aggs ON aggs.origin_account = COALESCE(accounts.origin_account, accounts.efd_id)
        LEFT JOIN unique_rpps ON aggs.id_rpps = unique_rpps.origin_account
        LEFT JOIN sirene ON aggs.siren = sirene.siren
        LEFT JOIN beneficiaire_dedup_incoherences AS inco ON accounts.efd_id = inco.efd_id
    )

    SELECT *
    FROM combined
)
