with multiple_rpps_accounts as (
  select
    id_beneficiaire
  from
    beneficiaire_extended benex
  where
    categorie_code in ('PRS', 'VET')
  group by
    1
  having
    count(distinct rpps)> 1
    or count(distinct adeli) > 1
    or count(distinct ordre)> 1
),
popularity as (
  select
    benex.id_beneficiaire,
    benex.rpps,
    benex.adeli,
    benex.ordre,
    sum(nb_decla) nb_decla
  from
    beneficiaire_extended benex
    join multiple_rpps_accounts acc on benex.id_beneficiaire = acc.id_beneficiaire
  where
    rpps not null
    or adeli not null
    or ordre not null
  group by
    all
),
top_rpps as (
  select
    id_beneficiaire,
    max(nb_decla) as nb_decla
  from
    popularity
  group by
    all
),
to_remove as (
  select
    benex.id_beneficiaire,
    benex.rpps,
    benex.adeli,
    benex.ordre
  from
    beneficiaire_extended benex
    join popularity on popularity.id_beneficiaire = benex.id_beneficiaire
    and (
      (
        benex.rpps not null
        and popularity.rpps = benex.rpps
      )
      or (
        benex.adeli not null
        and benex.adeli = popularity.adeli
      )
      or (
        benex.ordre not null
        and benex.ordre = popularity.ordre
      )
    )
    join top_rpps on top_rpps.id_beneficiaire = benex.id_beneficiaire
  where
    popularity.nb_decla < top_rpps.nb_decla
)
update
  beneficiaire_extended benex
set
  rpps = null,
  adeli = null,
  ordre = null
from
  to_remove
where
  to_remove.id_beneficiaire = benex.id_beneficiaire
  and (
    (
      benex.rpps not null
      and to_remove.rpps = benex.rpps
    )
    or (
      benex.adeli not null
      and benex.adeli = to_remove.adeli
    )
    or (
      benex.ordre not null
      and benex.ordre = to_remove.ordre
    )
  )
