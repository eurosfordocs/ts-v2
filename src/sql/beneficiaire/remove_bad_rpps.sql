WITH has_good_matching AS (
    SELECT id_beneficiaire
    FROM
        beneficiaire_match_multiple_rpps
    GROUP BY
        1
    HAVING
        MAX(match_rpps) = 1
),

bad_matchings AS (
    SELECT
        match_multiple.id_beneficiaire,
        match_multiple.rpps
    FROM
        beneficiaire_match_multiple_rpps AS match_multiple
    INNER JOIN has_good_matching ON match_multiple.id_beneficiaire = has_good_matching.id_beneficiaire
    WHERE
        match_rpps = 0
)

UPDATE
beneficiaire_extended benex
SET
    rpps = NULL
FROM
    bad_matchings AS bm
WHERE
    benex.id_beneficiaire = bm.id_beneficiaire
    AND benex.rpps = bm.rpps
