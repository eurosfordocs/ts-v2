import logging
import tempfile
import zipfile
from pathlib import Path

from src.constants.csv import SIREN_CSV, SIREN_URL, SIREN_ZIP_NAME
from src.settings import CONFIG
from src.utils import duck
from src.utils.downloader import Downloader

logger = logging.getLogger()


def process_sirene():
    sirene_parquet = CONFIG.CLEANED_DATA_DIR / "sirene.parquet"
    if sirene_parquet.exists():
        logger.info(f"process_siren : {sirene_parquet} exists")
        return
    else:
        sirene_to_parquet(sirene_parquet)


def sirene_to_parquet(output_fn: Path):
    siren_downloader = Downloader(SIREN_URL, SIREN_ZIP_NAME, update_days=30)
    siren_downloader.run()

    sql_fn = CONFIG.SQL_DIR / "sirene_to_parquet.sql"

    with tempfile.TemporaryDirectory() as tmpdirname:
        with zipfile.ZipFile(CONFIG.RAW_DATA_DIR / SIREN_ZIP_NAME) as zip_ref:
            zip_ref.extractall(tmpdirname)
        csv_fn = Path(tmpdirname) / SIREN_CSV
        duck.execute_script(
            sql_fn, parameters={"zip_fn": csv_fn, "output_fn": str(output_fn)}
        )
    logger.info(f"sirene_to_parquet, output={str(output_fn)}, zip_fn={csv_fn}")
