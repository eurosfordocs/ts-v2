import logging
import re
from datetime import datetime
from pathlib import Path

import pandas as pd

from src.constants.columns import RPPS_CLEANING_FUNCTIONS
from src.constants.csv import RPPS_TXT_FILE_NAME
from src.constants.tables import ALL_RAW_RPPS
from src.graph_dedup import GrapDeduplicator
from src.settings import CONFIG, Environment
from src.utils import duck
from src.utils.downloader import Extractor
from src.utils.utils import timeit

RPPS_SQL_DIR = CONFIG.SQL_DIR / "rpps"
HASH_COLUMNS = [
    "Identifiant PP",
    "Nom d'exercice",
    "Prénom d'exercice",
    "Libellé profession",
    "Libellé savoir-faire",
    "Code catégorie professionnelle",
    "Libellé mode exercice",
    "Numéro FINESS site",
    "Numéro FINESS établissement juridique",
    "Code postal (coord. structure)",
    "Code commune (coord. structure)",
    "Type d'identifiant PP",
    "Numéro SIREN site",
    "Numéro SIRET site",
]


def reset_rpps():
    histories = [
        "PS_LibreAcces_20190825.zip",
        "PS_LibreAcces_20200719.zip",
        "PS_LibreAcces_202204181148.zip",
    ]
    check_histories_present(histories)
    import_rpps(histories[0], ALL_RAW_RPPS)
    for fn in histories[1:]:
        upsert_rpps(fn)
    add_new_columns()
    duck.drop_column(ALL_RAW_RPPS, "column52")
    duck.drop_table("raw_rpps")


def date_from_name(name: str) -> str:
    dt_part = re.match(r"PS_LibreAcces_(\d{8,}).zip", name)
    if dt_part is None:
        return "CURRENT_DATE"
    dt = datetime.strptime(dt_part.group(1)[:8], "%Y%m%d").strftime("%Y-%m-%d")
    return f"'{dt}'"


def import_rpps(fn: str, table_name: str):
    duck.execute_query(f"drop table if exists {table_name}")
    rpps_fp = CONFIG.RAW_DATA_DIR / RPPS_TXT_FILE_NAME
    if rpps_fp.exists():
        rpps_fp.unlink()
    rpps_extractor = Extractor(fn, RPPS_TXT_FILE_NAME)
    rpps_extractor.run()
    duck.import_csv(table_name, rpps_fp, auto=True, delim="|", quote="", sample_size=-1)
    columns = [f'"{x}"' for x in HASH_COLUMNS]
    duck.calculate_hash(table_name, "hash", columns)
    duck.add_import_date(table_name, date_from_name(fn))
    duck.add_column(table_name, "supr_date_", "DATE")
    Path(rpps_fp).unlink()


def upsert_rpps(fn: str):
    import_rpps(fn, "tmp_raw_rpps")
    columns = [f'"{c}"' for c in duck.list_columns(ALL_RAW_RPPS)]
    duck.execute_script(
        RPPS_SQL_DIR / "flag_deleted_rpps.sql",
        parameters={"dt": date_from_name(fn)},
    )
    duck.execute_script(
        RPPS_SQL_DIR / "inject_new_rpps.sql",
        parameters={"columns": ", ".join(columns)},
    )
    duck.drop_table("tmp_raw_rpps")


def add_new_columns():
    columns = {
        '"Code genre activité"': "VARCHAR",
        '"Code rôle"': "VARCHAR",
        '"Libellé genre activité"': "VARCHAR",
        '"Libellé rôle"': "VARCHAR",
        "column56": "VARCHAR",
    }
    for c, t in columns.items():
        duck.add_column(ALL_RAW_RPPS, c, t)


def check_histories_present(fns):
    for fn in fns:
        if not (CONFIG.RAW_DATA_DIR / fn).exists():
            raise RuntimeError(f"Missing historical RPPS file : {str(fn)}")


@timeit
def transform_rpps():
    duck.execute_script(RPPS_SQL_DIR / "transform_rpps.sql")
    duck.clean_columns(RPPS_CLEANING_FUNCTIONS, "rpps")
    adeli_to_rpps()
    origin_account()
    if CONFIG.ENVIRONMENT != Environment.PROD:
        duck.execute_script(RPPS_SQL_DIR / "meta_infos.sql")
    else:
        duck.execute_script(RPPS_SQL_DIR / "prod_meta_info_profession.sql")
        duck.execute_script(RPPS_SQL_DIR / "prod_meta_info_specialite.sql")
        duck.execute_script(RPPS_SQL_DIR / "prod_meta_info_nom.sql")
        duck.execute_script(RPPS_SQL_DIR / "prod_meta_info_prenom.sql")


def adeli_to_rpps():
    duck.execute_script(RPPS_SQL_DIR / "adeli_to_rpps.sql")


def origin_account():
    duck.execute_script(RPPS_SQL_DIR / "origin_account_same_commune.sql")
    duck.execute_script(RPPS_SQL_DIR / "origin_account_movings.sql")
    deduplicate()
    duck.execute_script(RPPS_SQL_DIR / "origin_account.sql")


def deduplicate():
    """
    Use graph techniques to deduplicate beneficiaire, and inject back the relation.
    """
    identifiers = read_identifiers()

    deduper = GrapDeduplicator(
        "id",
        ["adeli", "rpps"],
        direct_link=["origin_account_moving", "origin_account_same_commune"],
        no_incoherence=["adeli"],
    ).run_deduplication(identifiers)

    logging.info(f"RPPS deduplication found {len(deduper.dups)} duplicates")
    logging.info(f"RPPS deduplication found {len(deduper.incoherences)} incoherences")

    id_to_origin = (  # noqa: F841
        pd.merge(identifiers, deduper.dups, on=["id"], how="left")
        .pipe(lambda df: df.fillna({"origin": df["id"]}).astype({"origin": "int"}))
        .rename(columns={"origin": "origin_id"})[["adeli", "rpps", "id", "origin_id"]]
    )
    duck.execute_query(
        "CREATE or replace TABLE rpps_dedup_origin AS SELECT * FROM extra_table",
        extra_table=id_to_origin,
    )

    inco = deduper.incoherences  # noqa: F841
    duck.execute_query(
        "CREATE or replace TABLE rpps_incoherences AS SELECT * FROM extra_table",
        extra_table=inco,
    )


def read_identifiers():
    filename = RPPS_SQL_DIR / "dedup_read.sql"
    with open(filename) as f:
        query = f.read()

    with duck.get_con() as con:
        return con.execute(query).df()
