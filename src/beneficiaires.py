import logging

import pandas as pd

from src.constants.columns import STATIC_COLUMNS
from src.constants.csv import (
    CATEGORIE_MATCHING_URL,
    REGROUPEMENT_BENEFICIAIRE_MANUEL_URL,
    SIREN_MATCH_URL,
)
from src.constants.miscellaneous import NULL_BEN_IDENTIFIANT
from src.constants.tables import RAW_DECLARATION
from src.format import efd, ts
from src.graph_dedup import GrapDeduplicator
from src.settings import CONFIG, Environment
from src.utils import duck
from src.utils.udfs import LUHN_UDF, REMOVE_UNICODE_SPACE_UDF
from src.utils.utils import list_params_to_str

BEN_SQL_DIR = CONFIG.SQL_DIR / "beneficiaire"


def process_beneficiaire():
    create_beneficiaire_extended()
    beneficiaire_pre_processing()
    ensure_single_ids()
    match_rpps_unique_names()
    remove_siren_hospital_workers()
    set_siren_as_structure_with_rpps()
    regroup_same_name()
    manual_regroup()
    deduplicate()
    join_to_origin()
    create_beneficiaire()
    create_origin_properties_table()
    update_transform_declaration()


def create_beneficiaire_extended():
    duck.execute_script(BEN_SQL_DIR / "beneficiaire_extended.sql")


def create_raw_beneficiaire():
    efd_column = next(x for x in STATIC_COLUMNS if x[0] == efd.BEN_EFD_ID)
    duck.add_column(RAW_DECLARATION, *efd_column)
    duck.execute_script(
        BEN_SQL_DIR / "beneficiaire_raw.sql",
        parameters={"clean_datadir": CONFIG.CLEANED_DATA_DIR},
        single_thread=True,
    )

    mode_columns = [
        {"mode": "identite", "as": "nom"},
        {"mode": "prenom", "as": "prenom"},
        {"mode": "beneficiaire_categorie", "as": "categorie"},
        {"mode": "profession_libelle", "as": "profession"},
        {"mode": "beneficiaire_profession_code", "as": "profession_code"},
        {"mode": "com_code", "as": "commune_code"},
        {"mode": "structure_exercice", "as": "structure"},
    ]
    for mc in mode_columns:
        duck.execute_query(
            f"""
                CREATE OR REPLACE TABLE raw_beneficiaire AS (
                WITH new_col as (
                    select efd_id, MODE({mc['mode']}) as {mc['as']} FROM raw_declaration group by 1
                )
                SELECT * from raw_beneficiaire left join new_col using(efd_id)
                )"""
        )
        logging.info(f"Added mode column {mc['as']}")

    # more complex cases
    complex_columns = [
        {"when": "beneficiaire_type_code = 'SIREN'", "as": "siren"},
        {
            "when": "beneficiaire_type_code = 'RPPS/ADELI' AND LEN(beneficiaire_identifiant) = 11",
            "as": "rpps",
        },
        {
            "when": "beneficiaire_type_code = 'RPPS/ADELI' AND LEN(beneficiaire_identifiant) = 9",
            "as": "adeli",
        },
        {"when": "beneficiaire_type_code = 'FINESS'", "as": "finess"},
        {"when": "beneficiaire_type_code = 'ORDRE'", "as": "ordre"},
        {"when": "beneficiaire_type_code = 'RNA'", "as": "rna"},
    ]

    for cc in complex_columns:
        duck.execute_query(
            f"""
    CREATE OR REPLACE TABLE raw_beneficiaire AS (
    WITH new_col as (
        select efd_id, MODE(case when {cc['when']} then beneficiaire_identifiant end) as {cc['as']}
        FROM raw_declaration group by 1
    )
    SELECT * from raw_beneficiaire left join new_col using(efd_id)
    )"""
        )
        logging.info(f"Added mode column {cc['as']}")


def create_beneficiaire():
    """
    Creating the final beneficiaire tables breaks production as requiring too much RAM.
    A dedicated implementation is done to consome less RAM but takes much more time to comple.
    This is a problem only while developping so we keep the two logic.
    """
    sirene_fn = CONFIG.CLEANED_DATA_DIR / "sirene.parquet"
    if CONFIG.ENVIRONMENT == Environment.TEST:
        duck.execute_script(
            BEN_SQL_DIR / "beneficiaire.sql",
            parameters={"sirene_fn": str(sirene_fn)},
        )
    else:
        duck.execute_script(
            BEN_SQL_DIR / "beneficiaire_empty.sql",
        )
        columns = [
            "categorie_code",
            "nom",
            "prenom",
            "profession",
            "siren",
            "rna",
            "ordre",
            "rpps",
            "adeli",
        ]
        for c in columns:
            duck.execute_script(
                BEN_SQL_DIR / "beneficiaire_mode.sql",
                parameters={"ident": c},
            )
        duck.execute_script(
            BEN_SQL_DIR / "beneficiaire_infos_from_directory.sql",
            parameters={"sirene_fn": str(sirene_fn)},
        )
        duck.execute_script(BEN_SQL_DIR / "update_incoherence_group.sql")


def update_transform_declaration():
    beneficiaire_injection = {
        efd.BEN_ORIGIN_ID: "origin_account",
        ts.BENEFICIAIRE_CATEGORIE_CODE: "categorie_code",
        ts.PROFESSION_LIBELLE: "profession",
        efd.BEN_SIREN: "siren",
        efd.BEN_RPPS: "rpps",
        efd.BEN_ADELI: "adeli",
        efd.BEN_ORDRE: "ordre",
        efd.BEN_RNA: "rna",
        ts.IDENTITE: "nom",
        ts.PRENOM: "prenom",
        efd.BEN_SPECIALITE: "specialite",
    }
    for source, target in beneficiaire_injection.items():
        duck.execute_script(
            BEN_SQL_DIR / "beneficiaire_inject.sql",
            parameters={"source": source, "target": target},
        )
        logging.info(f"inject {source} from beneficiaire")
    duck.execute_script(
        BEN_SQL_DIR / "normalize_categorie.sql",
        parameters={"categorie_matching_url": CATEGORIE_MATCHING_URL},
    )


def beneficiaire_pre_processing():
    rq = "update beneficiaire_extended set identifiant = remove_unicode_space(identifiant)"
    duck.execute_query(rq, udfs=[REMOVE_UNICODE_SPACE_UDF])
    duck.execute_script(
        BEN_SQL_DIR / "pre_treatment.sql",
        parameters={
            "null_identifiant": list_params_to_str(NULL_BEN_IDENTIFIANT),
            "siren_match_url": SIREN_MATCH_URL,
        },
        udfs=[LUHN_UDF],
    )
    duck.execute_script(BEN_SQL_DIR / "rpps_from_autre_code.sql")
    duck.execute_script(BEN_SQL_DIR / "fill_acronyme.sql")


def join_to_origin():
    """
    Spread all unique identifiants across all instances of unique id_beneficiaire.
    """
    duck.execute_script(BEN_SQL_DIR / "join_to_origin.sql")


def create_origin_properties_table():
    """
    Create a table containing the properties of the origin account for each beneficiaire.
    """
    duck.execute_script(BEN_SQL_DIR / "origin_properties.sql")


def match_rpps_unique_names():
    """
    Identify the RPPS or ADELI code of beneficiaire from the RPPS
    if the beneficiaire is the only one with its first and last name.
    """
    steps = [
        "names_profession",
        "names_commune",
        "names",
        "names_profession_inverted",
        "names_commune_inverted",
        "names_inverted",
        "dual_reversed_profession",
        "dual_subset_profession",
    ]

    log_accounts_without_id("inital")

    for step in steps:
        with open(BEN_SQL_DIR / f"match_rpps_unique_{step}.sql") as f:
            res = duck.execute_query(f.read(), fetch_results="first")
            logging.info(f"Add RPPS from {step} in phonebook : {res[0]}")

        log_accounts_without_id(step)


def log_accounts_without_id(step: str):
    with open(BEN_SQL_DIR / "accounts_without_ids.sql") as f:
        res = duck.execute_query(f.read(), fetch_results="first")
        logging.info(f"Accounts without ids post {step} : {res[0]}")


def deduplicate():
    """
    Use graph techniques to deduplicate beneficiaire, and inject back the relation.
    """
    identifiers = read_identifiers()
    deduper = GrapDeduplicator(
        "efd_id",
        [
            "siren",
            "adeli",
            "rpps",
            "rna",
            "ordre",
            "internal_mds",
            "internal_iqvia",
            "internal_chiesi",
        ],
        direct_link=[
            "manual_origin",
            "same_name_commune_profession_origin",
            "same_name_no_inco_origin",
            "same_name_origin",
        ],
        no_incoherence=["adeli"],
    ).run_deduplication(identifiers)

    logging.info(f"deduplication found {len(deduper.dups)} duplicates")
    logging.info(f"deduplication found {len(deduper.incoherences)} incoherences")
    inject_origin(deduper.dups)
    inject_incoherence(deduper.incoherences)
    duck.execute_script(BEN_SQL_DIR / "flag_incoherences.sql")


def read_identifiers():
    with open(BEN_SQL_DIR / "dedup_read.sql") as f:
        query = f.read()

    with duck.get_con() as con:
        return con.execute(query).df()


def inject_origin(df: pd.DataFrame):
    duck.execute_query(
        "CREATE or replace TABLE beneficiaire_dedup_origin AS SELECT * FROM extra_table",
        extra_table=df,
    )


def inject_incoherence(df: pd.DataFrame):
    duck.execute_query(
        "CREATE or replace TABLE beneficiaire_dedup_incoherences AS SELECT * FROM extra_table",
        extra_table=df,
    )


def manual_regroup():
    rq = f"""
        CREATE OR REPLACE TABLE beneficiaire_regroup_manual as (
            SELECT *
            FROM read_csv('{REGROUPEMENT_BENEFICIAIRE_MANUEL_URL}',
                header = true,
                columns = {{
                    'efd_id': 'UBIGINT',
                    'id_beneficiaire':'UBIGINT',
                    'origin_account_beneficiaire':'UBIGINT',
                    'origin_account':'UBIGINT',
                    'nom':'STRING'
                }}
            )
            WHERE efd_id <> origin_account
            )
    """
    duck.execute_query(rq)

    with open(BEN_SQL_DIR / "check_manual_obsolete.sql") as f:
        res = duck.execute_query(f.read(), fetch_results="all")
        ids = sorted([x[0] for x in res])
        logging.info(
            f"Regroupement beneficiaires: les regroupements suivants manuels sont obsoletes {ids} "
        )

    with open(BEN_SQL_DIR / "regroup_manual.sql") as f:
        res = duck.execute_query(f.read(), fetch_results="first")
        logging.info(
            f"Regroupement beneficiaires: {res[0]} beneficiaire etendus regroupes manuellement"
        )


def regroup_same_name():
    for identifier in ["rpps", "adeli"]:
        with open(BEN_SQL_DIR / "remove_same_name_profession_absurd_rpps.sql") as f:
            res = duck.execute_query(
                f.read(), fetch_results="all", parameters={"var": identifier}
            )
        logging.info(f"Regroupement beneficiaires: {res[0]} {identifier} retirés.")
    steps = [
        "regroup_same_name_commune_profession",
        "regroup_same_name_institutions_no_inco",
    ]
    for name in steps:
        with open(BEN_SQL_DIR / f"{name}.sql") as f:
            res = duck.execute_query(f.read(), fetch_results="all")
        logging.info(
            f"Regroupement beneficiaires: {res[0]} comptes regroupes avec {name}."
        )


def log_no_matching():
    rq = """
    with no_matching as (
        select id_beneficiaire
        from beneficiaire_match_multiple_rpps
        group by 1
        having max(match_rpps) = 0
    )
    select count(distinct id_beneficiaire) from no_matching
    """
    res = duck.execute_query(rq, fetch_results="first")
    logging.info(f"RPPS multiples: {res[0]} beneficiaire sans rpps dans l'annuaire")


def log_many_matching():
    rq = """
    with many_matching as (
        select id_beneficiaire
        from beneficiaire_match_multiple_rpps
        group by 1
        having sum(match_rpps) > 1
    )
    select count(distinct id_beneficiaire) from many_matching
    """
    res = duck.execute_query(rq, fetch_results="first")
    logging.info(
        f"RPPS multiples: {res[0]} beneficiaire avec plusieurs rpps dans l'annuaire"
    )


def ensure_single_ids():
    """
    Certains comptes bénéficiaires sont mentionnés avec plusieurs RPPS/ADELI différents.
    Cette étape cherche à identifier des RPPS absurde du point de vue nom, prénom du bénéficiaire et de les retirer.
    """
    for identifier in ["rpps", "adeli"]:
        log_accounts_with_duplicate_ids(identifier)
        duck.execute_script(
            BEN_SQL_DIR / "remove_incoherent_rpps_from_account.sql",
            parameters={"var": identifier},
        )
        log_accounts_with_duplicate_ids(identifier)
    duck.execute_script(BEN_SQL_DIR / "remove_rpps_from_incoherent_account.sql")
    log_accounts_with_duplicate_ids("rpps")
    log_accounts_with_duplicate_ids("siren")
    duck.execute_script(
        BEN_SQL_DIR / "remove_siren_from_incoherent_account.sql",
        parameters={"sirene_fn": str(CONFIG.CLEANED_DATA_DIR / "sirene.parquet")},
    )
    log_accounts_with_duplicate_ids("siren")
    duck.execute_script(
        BEN_SQL_DIR / "keep_popular_siren_for_name.sql", parameters={"ident": "siren"}
    )
    log_accounts_with_duplicate_ids("siren")
    log_accounts_with_duplicate_ids("rna")
    duck.execute_script(
        BEN_SQL_DIR / "keep_popular_siren_for_name.sql", parameters={"ident": "rna"}
    )
    log_accounts_with_duplicate_ids("rna")


def log_accounts_with_duplicate_ids(ident: str):
    with open(BEN_SQL_DIR / "accounts_with_duplicate_ids.sql") as f:
        res = duck.execute_query(
            f.read(), fetch_results="first", parameters={"ident": ident}
        )
        logging.info(f"Accounts with multiple {ident} : {res[0]}")


def remove_siren_hospital_workers():
    with open(BEN_SQL_DIR / "remove_hospital_siren_with_rpps.sql") as f:
        res = duck.execute_query(f.read(), fetch_results="first")
        logging.info(f"Hospital workers accounts : {res[0]}")

    for ident in ["efd_id", "nom"]:
        with open(BEN_SQL_DIR / "asso_in_hospital.sql") as f:
            res = duck.execute_query(
                f.read(), fetch_results="first", parameters={"ident": ident}
            )
            logging.info(f"Associations within hospitals : {res[0]}")


def set_siren_as_structure_with_rpps():
    with open(BEN_SQL_DIR / "siren_as_structure_if_rpps.sql") as f:
        res = duck.execute_query(f.read(), fetch_results="first")
        logging.info(f"Siren considered as structure : {res[0]}")
