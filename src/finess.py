import logging

import pandas as pd

from src.graph_dedup import GrapDeduplicator
from src.settings import CONFIG
from src.utils import duck

FINESS_SQL_DIR = CONFIG.SQL_DIR / "finess"


def transform_finess():
    duck.execute_script(FINESS_SQL_DIR / "transform_finess.sql")
    fetch_meta_siren()


def fetch_meta_siren():
    """
    Use graph techniques attribute a single siren to a finess
    """
    identifiers = read_identifiers()
    deduper = GrapDeduplicator(
        "siren",
        ["num_finess_ej", "num_finess_et"],
        direct_link=[],
        no_incoherence=["num_finess_ej", "num_finess_et"],
    ).run_deduplication(identifiers)

    logging.info(f"deduplication found {len(deduper.dups)} duplicates")
    inject_meta_siren(deduper.dups)


def read_identifiers():
    with open(FINESS_SQL_DIR / "dedup_input.sql") as f:
        rq = f.read()

    with duck.get_con() as con:
        return con.execute(rq).df()


def inject_meta_siren(frame: pd.DataFrame):
    rq = """
    CREATE OR REPLACE TABLE finess_regroup_siren AS (
        SELECT siren, origin
        from extra_table
    )
    """
    duck.execute_query(rq, extra_table=frame)
