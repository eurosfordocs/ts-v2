import src.format.duck_types as t

ID = "id"
DECLARATION_TS_ID = "declaration_ts_id"
TOKEN = "token"
TYPE_DECLARATION = "type_declaration"
DECLARATION_IDENTIFIANT = "declaration_identifiant"
CONVENTION_IDENTIFIANT = "convention_identifiant"
CONVENTION_TS_ID = "convention_ts_id"
CONVENTION_META_MOTIF = "meta_motif_lien_interet"
MOTIF_LIEN_INTERET_CODE = "motif_lien_interet_code"
MOTIF_LIEN_INTERET = "motif_lien_interet"
AUTRE_MOTIF = "autre_motif"
INFORMATION_EVENEMENT = "information_evenement"
MONTANT = "montant"
MONTANT_DECLARE_CONVENTION = "montant_declare_convention"
MONTANT_MASQUE = "montant_masque"
MONTANT_TOTAL_AVANTAGES_LIES = "montant_total_avantages_lies"
MONTANT_TOTAL_CONVENTION = "montant_total_convention"
MONTANT_TOTAL_REMUNERATIONS_LIEES = "montant_total_remunerations_liees"
NOMBRE_AVANTAGES_LIES = "nombre_avantages_lies"
NOMBRE_REMUNERATIONS_LIEES = "nombre_remunerations_liees"
DATE = "date"
DATE_DEBUT = "date_debut"
DATE_FIN = "date_fin"
DEMANDE_DE_RECTIFICATION = "demande_de_rectification"
STATUT = "statut"
DATE_PUBLICATION = "date_publication"
DATE_TRANSMISSION = "date_transmission"
DATE_EVENEMENTT = "date_evenement"
SEMESTRE_ANNEE = "semestre_annee"
ANNEE = "annee"

BENEFICIAIRE_TS_ID = "beneficiaire_ts_id"
BEN_ORIGIN_ID = "ben_origin_id"
BEN_EFD_ID = "efd_id"
BEN_NOM = "ben_nom"
BEN_PRENOM = "ben_prenom"
BEN_NOM_PRENOM = "ben_nom_prenom"
BEN_CATEGORIE_CODE = "ben_categorie_code"
BEN_CATEGORIE = "ben_categorie"
BEN_TYPE_IDENTIFIANT = "ben_type_identifiant"
BEN_IDENTIFIANT = "ben_identifiant"
BEN_PROFESSION = "ben_profession"
BEN_SPECIALITE = "ben_specialite"
BEN_STRUCTURE = "ben_structure"
BEN_ADRESSE = "ben_adresse"
BEN_CODE_POSTAL = "ben_code_postal"
BEN_VILLE = "ben_ville"
BEN_VILLE_SANSCEDEX = "ben_ville_sanscedex"
BEN_LAT_LON = "ben_lat_lon"
BEN_COMMUNE = "ben_commune"
BEN_COMMUNE_CODE = "ben_commune_code"
BEN_EPCI = "ben_epci"
BEN_EPCI_CODE = "ben_epci_code"
BEN_DEPARTEMENT_CODE = "ben_departement_code"
BEN_DEPARTEMENT = "ben_departement"
BEN_REGION_CODE = "ben_region_code"
BEN_REGION = "ben_region"
BEN_PAYS_CODE = "ben_pays_code"
BEN_PAYS = "ben_pays"
BEN_SIREN = "ben_siren"
BEN_RPPS = "ben_rpps"
BEN_ADELI = "ben_adeli"
BEN_ORDRE = "ben_ordre"
BEN_RNA = "ben_rna"

ENTREPRISE_TS_ID = "entreprise_ts_id"
ENTREPRISE_RAISON_SOCIALE = "entreprise_raison_sociale"
ENTREPRISE_SECTEUR_ACTIVITE = "entreprise_secteur_activite"
ENTREPRISE_SIREN = "entreprise_siren"
ENTREPRISE_PAYS = "entreprise_pays"

DECLARANT_TS_ID = "declarant_ts_id"
DECLARANT_RAISON_SOCIALE = "declarant_raison_sociale"
DECLARANT_SECTEUR_ACTIVITE = "declarant_secteur_activite"
DECLARANT_VILLE = "declarant_ville"
DECLARANT_DEPARTEMENT = "declarant_departement"
DECLARANT_REGION = "declarant_region"
DECLARANT_PAYS = "declarant_pays"
DECLARANT_MERE_TS_ID = "declarant_mere_ts_id"
DECLARANT_CODE_POSTAL = "declarant_code_postal"
DECLARANT_SIREN = "declarant_siren"


EFT_TYPES = {
    DECLARATION_TS_ID: t.INTEGER,
    TOKEN: t.VARCHAR,
    TYPE_DECLARATION: t.VARCHAR,
    DECLARATION_IDENTIFIANT: t.VARCHAR,
    CONVENTION_IDENTIFIANT: t.VARCHAR,
    CONVENTION_TS_ID: t.INTEGER,
    CONVENTION_META_MOTIF: t.VARCHAR,
    MOTIF_LIEN_INTERET_CODE: t.VARCHAR,
    MOTIF_LIEN_INTERET: t.VARCHAR,
    AUTRE_MOTIF: t.VARCHAR,
    INFORMATION_EVENEMENT: t.VARCHAR,
    MONTANT: t.INTEGER,
    MONTANT_DECLARE_CONVENTION: t.INTEGER,
    MONTANT_MASQUE: t.INTEGER,
    MONTANT_TOTAL_AVANTAGES_LIES: t.INTEGER,
    MONTANT_TOTAL_CONVENTION: t.INTEGER,
    MONTANT_TOTAL_REMUNERATIONS_LIEES: t.INTEGER,
    NOMBRE_AVANTAGES_LIES: t.INTEGER,
    NOMBRE_REMUNERATIONS_LIEES: t.INTEGER,
    DATE: t.DATE,
    DATE_DEBUT: t.DATE,
    DATE_FIN: t.DATE,
    DEMANDE_DE_RECTIFICATION: t.VARCHAR,
    STATUT: t.VARCHAR,
    DATE_PUBLICATION: t.DATE,
    DATE_TRANSMISSION: t.DATE,
    DATE_EVENEMENTT: t.DATE,
    SEMESTRE_ANNEE: t.VARCHAR,
    ANNEE: t.INTEGER,
    BENEFICIAIRE_TS_ID: t.INTEGER,
    BEN_EFD_ID: t.UBIGINT,
    BEN_ORIGIN_ID: t.UBIGINT,
    BEN_NOM: t.VARCHAR,
    BEN_PRENOM: t.VARCHAR,
    BEN_NOM_PRENOM: t.VARCHAR,
    BEN_CATEGORIE_CODE: t.VARCHAR,
    BEN_CATEGORIE: t.VARCHAR,
    BEN_TYPE_IDENTIFIANT: t.VARCHAR,
    BEN_IDENTIFIANT: t.VARCHAR,
    BEN_PROFESSION: t.VARCHAR,
    BEN_SPECIALITE: t.VARCHAR,
    BEN_STRUCTURE: t.VARCHAR,
    BEN_PAYS_CODE: t.VARCHAR,
    BEN_PAYS: t.VARCHAR,
    BEN_ADRESSE: t.VARCHAR,
    BEN_CODE_POSTAL: t.VARCHAR,
    BEN_VILLE: t.VARCHAR,
    BEN_VILLE_SANSCEDEX: t.VARCHAR,
    BEN_LAT_LON: t.VARCHAR,
    BEN_COMMUNE: t.VARCHAR,
    BEN_COMMUNE_CODE: t.VARCHAR,
    BEN_EPCI: t.VARCHAR,
    BEN_EPCI_CODE: t.VARCHAR,
    BEN_DEPARTEMENT_CODE: t.VARCHAR,
    BEN_DEPARTEMENT: t.VARCHAR,
    BEN_REGION_CODE: t.VARCHAR,
    BEN_REGION: t.VARCHAR,
    BEN_SIREN: t.VARCHAR,
    BEN_RPPS: t.VARCHAR,
    BEN_ADELI: t.VARCHAR,
    BEN_ORDRE: t.VARCHAR,
    BEN_RNA: t.VARCHAR,
    ENTREPRISE_TS_ID: t.INTEGER,
    ENTREPRISE_RAISON_SOCIALE: t.VARCHAR,
    ENTREPRISE_SECTEUR_ACTIVITE: t.VARCHAR,
    ENTREPRISE_SIREN: t.VARCHAR,
    ENTREPRISE_PAYS: t.VARCHAR,
    DECLARANT_TS_ID: t.INTEGER,
    DECLARANT_RAISON_SOCIALE: t.VARCHAR,
    DECLARANT_SECTEUR_ACTIVITE: t.VARCHAR,
    DECLARANT_VILLE: t.VARCHAR,
    DECLARANT_DEPARTEMENT: t.VARCHAR,
    DECLARANT_REGION: t.VARCHAR,
    DECLARANT_PAYS: t.VARCHAR,
    DECLARANT_CODE_POSTAL: t.VARCHAR,
    DECLARANT_SIREN: t.VARCHAR,
}
