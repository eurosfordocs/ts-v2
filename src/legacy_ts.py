from zipfile import ZipFile

import pandas as pd

from src.constants.columns import ALL_RAW_DECLARATION_COLUMNS
from src.constants.tables import ALL_RAW_DECLARATION
from src.format import ts
from src.settings import CONFIG, Environment
from src.utils import duck
from src.utils.downloader import Downloader

LEGACY_SQL_DIR = CONFIG.SQL_DIR / "legacy_ts"
LEGACY_URL = (
    "https://drive.usercontent.google.com/download?id=1St3TWK-BDLB1mda4ePT65KweRb8LKmq-&export=download&authuser=3&confirm=t&uuid=64ae63d0-d7f4-4900-962f-76d178f543ca&at=APvzH3p9aDTemMUPb51jTJjYnfvY:1735667852283"
    if CONFIG.ENVIRONMENT != Environment.CI
    else "https://gitlab.com/eurosfordocs/ts-v2/-/raw/main/frontend/public/ci/exports-etalab-2022-02-06-pre-migration.zip"
)

LEGACY_ZIP_NAME = "exports-etalab-2022-02-06-pre-migration.zip"
LEGACY_RENAME_COLUMNS = {
    "benef_codepostal": "code_postal",
    "benef_etablissement": "structure_exercice",
    "benef_identifiant_valeur": "beneficiaire_identifiant",
    "benef_prenom": "prenom",
    "benef_ville": "ville",
    "categorie": "beneficiaire_categorie",
    "conv_objet_autre": "autre_motif",
    "denomination_sociale": "raison_sociale",
    "identifiant_type": "beneficiaire_type_code",
    "ligne_identifiant": "identifiant_unique",
    "pays": "beneficiaire_nom_pays",
    "qualite": "profession_libelle",
}

LEGACY_KEEP_COLUMNS = [
    "code_postal_entreprise",
    "convention_liee",
    "date_debut",
    "date_fin",
    "date",
    "entr_ville",
    "entreprise_id",
    "entreprise_nom_pays",
    "entreprise_secteur_activite_code",
    "id",
    "information_evenement",
    "lien_interet",
    "montant",
    "secteur_activite",
]

LEGACY_NO_EQUIVALENT = [
    "beneficiaire_nom_prenom_com_name",
    "beneficiaire_type",
    "com_code",
    "com_name",
    "dep_code",
    "dep_name",
    "entr_dep_name",
    "entr_reg_name",
    "epci_code",
    "epci_name",
    "geom",
    "id_beneficiaire",
    "mere_id",
    "motif_lien_interet_code",
    "numero_siren",
    "reg_code",
    "reg_name",
    "semestre_annee",
    "token",
    "ville_sanscedex",
]

ORDERED_COLUMNS = [
    "benef_adresse1",
    "benef_adresse2",
    "benef_adresse3",
    "benef_adresse4",
    "benef_categorie_code",
    "benef_codepostal",
    "benef_denomination_sociale",
    "benef_etablissement_codepostal",
    "benef_etablissement_ville",
    "benef_etablissement",
    "benef_identifiant_type_code",
    "benef_identifiant_valeur",
    "benef_nom",
    "benef_objet_social",
    "benef_pays_code",
    "benef_prenom",
    "benef_qualite_code",
    "benef_specialite_code",
    "benef_speicalite_libelle",
    "benef_titre_code",
    "benef_titre_libelle",
    "benef_ville",
    "categorie",
    "conv_objet_autre",
    "conv_objet",
    "convention_liee",
    "date_debut",
    "date_fin",
    "date",
    "denomination_sociale",
    "entreprise_identifiant",
    "id",
    "identifiant_type",
    "information_evenement",
    "lien_interet",
    "ligne_identifiant",
    "ligne_rectification",
    "ligne_type",
    "montant",
    "pays",
    "qualite",
]

MOTIFS = {
    "Achat / location d'espace publicitaire": "Contrat d'achat ou de location d'espaces publicitaires",
    "Achat / location d'espaces dans le cadre d'événements scientifiques": "Contrat d'achat ou de location d'espaces dans le cadre de manifestations scientifiques",
    "Achat de documentation scientifique": None,
    "Autre": None,
    "Autres prestations de services": None,
    "Cession de droits / licence d'exploitation": None,
    "Contrat / expertise autre que scientifique": "Contrat de conseil / d'expertise autre que scientifique",
    "Contrat d'expert scientifique, contrat dans le cadre d'une recherche, contrat de consultant": "Contrat d'expert scientifique",
    "Contrat d'intervenant à une manifestation / orateur": "Contrat  d'intervenant à une manifestation",
    "Don / Mécénat": "Mécénat",
    "Edition": None,
    "Enquête / Etude / Etude de marché (hors recherche)": "Enquête, étude, étude de marché (hors recherche)",
    "Evaluation produit cosmétique": None,
    "Formation": None,
    "Hospitalité": None,
    "Inscription congrès": "Frais d'inscription à une manifestation",
    "Parrainage": None,
    "Partenariat": None,
    "Prêt de matériel": None,
    "Recherche scientifique": "Contrat de recherche scientifique",
    "Remise d'une bourse": "Contrat de remise d'une bourse de recherche",
    "Remise de prix": "Contrat de remise de prix",
    "Vigilance produit cosmétique": None,
    "Contrat d'interview": None,
}


def merge_legacy_declaration():
    if count_legacy_lines() > 0:
        return
    extract_legacy()
    insert_legacy_ts()


def count_legacy_lines():
    cnts = duck.execute_query(
        "select sum(cast(date_modif='2022-02-06' as int)) from all_raw_declaration",
        fetch_results="first",
    )
    return cnts[0] or 0


def extract_legacy():
    ts_downloader = Downloader(LEGACY_URL, LEGACY_ZIP_NAME)
    ts_downloader.run()
    fn = CONFIG.RAW_DATA_DIR / LEGACY_ZIP_NAME
    with ZipFile(str(fn)) as zipfn:
        extract_dir = str(fn.with_suffix(""))
        zipfn.extractall(path=extract_dir)


def insert_legacy_ts():
    updated_motifs_table()

    datadir = (CONFIG.RAW_DATA_DIR / LEGACY_ZIP_NAME).with_suffix("")
    duck.execute_script(
        LEGACY_SQL_DIR / "raw_declaration.sql",
        parameters={
            "datadir": str(datadir),
            "renamed_columns": ",\n".join(
                f"{k} AS {v}" for k, v in LEGACY_RENAME_COLUMNS.items()
            ),
            "stable_columns": ",\n".join(LEGACY_KEEP_COLUMNS),
            "empty_strings": ",\n".join(
                [f"CAST(NULL AS STRING) AS {c}" for c in LEGACY_NO_EQUIVALENT]
            ),
            "ordered_intermediate": ",".join(ORDERED_COLUMNS),
        },
    )
    duck.execute_script(LEGACY_SQL_DIR / "identify_id.sql")
    duck.calculate_md5("legacy_ts", ts.DECLARATION_HASH, ts.COLONES_DECLARATION)
    duck.execute_script(LEGACY_SQL_DIR / "deleted_legacy.sql")
    duck.execute_script(LEGACY_SQL_DIR / "update_stock_date_ajout.sql")
    insert_legacy()
    clean_tables()


def updated_motifs_table():
    motifs = pd.DataFrame(
        [[k, v] for k, v in MOTIFS.items()], columns=["legacy", "current"]
    )
    query = "create or replace table legacy_motifs as (select * from extra_table)"
    duck.execute_query(query, extra_table=motifs)


def insert_legacy():
    column_list = ", ".join(ALL_RAW_DECLARATION_COLUMNS)
    query = f"""
    insert into {ALL_RAW_DECLARATION} ({column_list})
    select {column_list}
    from legacy_ts
    """
    duck.execute_query(query)


def clean_tables():
    to_drop = [
        "legacy_entreprise",
        "legacy_motifs",
        "legacy_ts_avantages",
        "legacy_ts_convention",
        "legacy_ts_remunerations",
        "legacy_ts",
    ]
    for table in to_drop:
        duck.drop_table(table)
