import logging

from src.constants.columns import ALL_RAW_DECLARATION_COLUMNS
from src.constants.csv import RPPS_ZIP_NAME
from src.constants.Exceptions import MissingTable
from src.constants.tables import ALL_RAW_DECLARATION, ALL_RAW_RPPS, RAW_DECLARATION
from src.format import ts
from src.legacy_ts import merge_legacy_declaration
from src.rpps import import_rpps, upsert_rpps
from src.settings import CONFIG
from src.utils import duck
from src.utils.slack import send_slack_message
from src.utils.utils import frame_it


def handle_creates():
    columns = sorted(set(ts.COLONES_DECLARATION + [ts.DECLARATION_HASH]))
    column_list = ", ".join(columns)

    query = f"""
    create or replace table created as (
        select
            {column_list},
            CURRENT_DATE() as {ts.DATE_AJOUT},
            CURRENT_DATE() as {ts.DATE_MODIF},
            cast(null as date) as {ts.DATE_SUPPR},
            cast(null as boolean) as {ts.SUPPRIME}
        from {RAW_DECLARATION} td
            WHERE NOT EXISTS (SELECT {ts.ID} FROM {ALL_RAW_DECLARATION} ad
                        WHERE td.{ts.ID} = ad.{ts.ID})
    )
    """

    rows = duck.execute_query(query, fetch_results="first")[0]
    logging.info(f"{rows} declarations are new")
    return rows


def handle_deletes():
    query = f"""
        create or replace table deleted as(
            with ranked_version as (
                select {ts.ID},
                {ts.DATE_MODIF},
                {ts.DATE_FIN},
                {ts.DATE_DEBUT},
                {ts.DATE_PUBLICATION},
                {ts.DATE},
                {ts.DATE_SUPPR},
                ROW_NUMBER() OVER(PARTITION BY {ts.ID} ORDER BY {ts.DATE_MODIF} DESC) as rk
                from {ALL_RAW_DECLARATION} ad
            )
            select
                {ts.ID},
                {ts.DATE_MODIF},
                case
                    when {ts.DATE_FIN} is null and {ts.DATE_DEBUT} is not null then FALSE
                    when {ts.DATE_FIN} is null then (today() - greatest( {ts.DATE}, {ts.DATE_PUBLICATION})) / 365 < 5
                    else (today() - greatest( {ts.DATE}, {ts.DATE_PUBLICATION}, {ts.DATE_FIN})) / 365 < 5
                end as {ts.SUPPRIME}
            from ranked_version ad
            where not exists (
                select 1 from {RAW_DECLARATION} td
                where td.{ts.ID} = ad.{ts.ID}
            )
            and rk = 1
            and ad.{ts.DATE_SUPPR} is null
        )
    """
    rows = duck.execute_query(query, fetch_results="first")[0]
    logging.info(f"{rows} declarations have been deleted")
    return rows


def handle_updates():
    columns = sorted(set(ts.COLONES_DECLARATION + [ts.DECLARATION_HASH]))
    column_list = ", ".join(f"td.{c}" for c in columns)
    query = f"""
    create or replace table updated as (
        with ranked_version as (
            select {ts.ID}, {ts.DECLARATION_HASH} ,
            ROW_NUMBER() OVER(PARTITION BY {ts.ID} ORDER BY {ts.DATE_MODIF} DESC) as rk
            from {ALL_RAW_DECLARATION} ad
        ),
        last_version as (
            select {ts.ID}, {ts.DECLARATION_HASH}
            from ranked_version
            where rk =1
        )
        select {column_list},
        current_date() as {ts.DATE_MODIF},
        cast(null as date) as {ts.DATE_AJOUT},
        cast(null as date) as {ts.DATE_SUPPR},
        cast(null as boolean) as {ts.SUPPRIME}
        FROM {RAW_DECLARATION} td
        INNER JOIN last_version lv on td.{ts.ID} = lv.{ts.ID}
        where td.{ts.DECLARATION_HASH} <> lv.{ts.DECLARATION_HASH}
    )
    """
    rows = duck.execute_query(query, fetch_results="first")[0]
    logging.info(f"{rows} declarations have changed")
    return rows


def import_data(tn):
    if duck.table_exists(RAW_DECLARATION):
        duck.drop_table(tn)
        duck.execute_query(
            f"""
            Create table {tn} as
            select *,
            current_date() as {ts.DATE_AJOUT},
            cast(null as date) as {ts.DATE_MODIF},
            cast(null as date) {ts.DATE_SUPPR},
            cast(null as boolean) as {ts.SUPPRIME}
            from {RAW_DECLARATION}
            """
        )
        logging.info(f"Created {tn} as a copy from {RAW_DECLARATION}")
    else:
        raise MissingTable(f"Missing table {RAW_DECLARATION}, cannot merge")


@frame_it
def merge():
    merge_all_raw_rpps()
    merge_all_raw_declaration()


def merge_all_raw_rpps():
    raw_zip_fn = CONFIG.CLEANED_DATA_DIR / f"{ALL_RAW_RPPS}.parquet"

    if not duck.table_exists(ALL_RAW_RPPS) and not raw_zip_fn.exists():
        import_rpps(RPPS_ZIP_NAME, ALL_RAW_RPPS)
    else:
        if not duck.table_exists(ALL_RAW_RPPS) and raw_zip_fn.exists():
            duck.import_parquet(ALL_RAW_RPPS, str(raw_zip_fn))

        upsert_rpps(RPPS_ZIP_NAME)
    duck.export_parquet(table=ALL_RAW_RPPS, fp=str(raw_zip_fn))


def merge_all_raw_declaration():
    duck.calculate_md5(RAW_DECLARATION, ts.DECLARATION_HASH, ts.COLONES_DECLARATION)

    raw_decla_fn = CONFIG.CLEANED_DATA_DIR / f"{ALL_RAW_DECLARATION}.parquet"

    if not duck.table_exists(ALL_RAW_DECLARATION) and not raw_decla_fn.exists():
        import_data(ALL_RAW_DECLARATION)
    else:
        if not duck.table_exists(ALL_RAW_DECLARATION) and raw_decla_fn.exists():
            duck.import_parquet(ALL_RAW_DECLARATION, str(raw_decla_fn))
        upsert()

    merge_legacy_declaration()
    duck.export_parquet(table=ALL_RAW_DECLARATION, fp=raw_decla_fn)


def upsert():
    """
    Upserts the records from the daily import (table raw_declaration) in the table all_raw_declaration.
    In details, it first detects updates, creations and deletions and stores them into 3 tables (created, updated,
    deleted). This allows to set the values like date_ajout_efd, date_modif, and the boolean supprime.
    Then updates are historized (we find and store which fields have changed).

    Then we update the all_raw_declaration table: we flag all records who have changed (updated and deleted),
    and add all records from updated, deleted and created. This is done in one transaction.
    A Slack message is sent with the number of updates, creates, deletes.
    """

    updates = handle_updates()
    creates = handle_creates()
    deletes = handle_deletes()
    flag_deleted()
    insert_created_and_updated()

    logging.info("applied changes to all_declarations")
    send_slack_message(
        f"""Today we have:
        - {creates} new declarations
        - {updates} updates to existing declarations
        - {deletes} declarations deleted"""
    )

    for t in ["updated", "created", "deleted"]:
        duck.drop_table(t)


def flag_deleted():
    query = f"""
    update {ALL_RAW_DECLARATION}
    set {ts.DATE_SUPPR} = current_date(),
    {ts.SUPPRIME} = deleted.{ts.SUPPRIME}
    from deleted
    where deleted.{ts.ID} = {ALL_RAW_DECLARATION}.{ts.ID}
    and (deleted.{ts.DATE_MODIF} = {ALL_RAW_DECLARATION}.{ts.DATE_MODIF}
        or ( deleted.{ts.DATE_MODIF} is null and {ALL_RAW_DECLARATION}.{ts.DATE_MODIF} is null))
    """
    duck.execute_query(query)


def insert_created_and_updated():
    column_list = ", ".join(ALL_RAW_DECLARATION_COLUMNS)
    query = f"""
    insert into {ALL_RAW_DECLARATION} ({column_list})
    select {column_list}
    from updated
    union
    select {column_list}
    from created
    """
    duck.execute_query(query)
