import datetime
import logging
import os
import subprocess

from src.constants.tables import (
    ALL_RAW_DECLARATION,
    ALL_RAW_RPPS,
    BENEFICIAIRE,
    ENTREPRISES,
    TS_DECLARATION,
)
from src.settings import (
    CONFIG,
    DOCKER_METABASE,
    MB_POSTGRES_PASSWORD,
    S3_BUCKET,
    Environment,
)
from src.utils import docker, duck, s3
from src.utils.utils import frame_it

TABLES = [ALL_RAW_DECLARATION, ALL_RAW_RPPS, BENEFICIAIRE, ENTREPRISES, TS_DECLARATION]


@frame_it
def restore_from_S3(date, bucket=S3_BUCKET):
    # DOWNLOADS
    for table in TABLES:
        object_name = f"{date.isoformat()}/{table}.parquet"
        fp = CONFIG.CLEANED_DATA_DIR / f"{table}_{date}.parquet"
        if not fp.exists():
            s3.download(object_name=object_name, file_path=fp, bucket=bucket)
    logging.info(f"Downloaded clean files from S3 ({date})")

    # LOAD IN DUCKDB
    duck.reset()
    for table in TABLES:
        fp = CONFIG.CLEANED_DATA_DIR / f"{table}_{date}.parquet"
        duck.import_parquet(table_name=table, fp=fp)
    logging.info("Loaded downloaded clean files into duckdb")

    # RENAME FILES so they don't acumualte
    if CONFIG.ENVIRONMENT == Environment.PROD:
        for table in TABLES:
            dated_fp = CONFIG.CLEANED_DATA_DIR / f"{table}_{date}.parquet"
            clean_fp = CONFIG.CLEANED_DATA_DIR / f"{table}.parquet"
            if clean_fp.exists():
                clean_fp.unlink()
            os.rename(dated_fp, clean_fp)
        logging.info("dated parquet files > clean parquet file")


def backup_file(fp):
    last_update = datetime.datetime.fromtimestamp(os.path.getmtime(fp))
    if last_update.date() >= datetime.datetime.today().date():
        s3.upload_file(fp)
    else:
        logging.warning(f"File {fp} exists, but older as today, so we won't back it up")


@frame_it
def daily_backup():
    if s3.can_connect():
        for fp in [CONFIG.CLEANED_DATA_DIR / f"{o}.parquet" for o in TABLES]:
            backup_file(fp)

        backup_mb_data()
        s3.clean_bucket()
    else:
        logging.info("Skipping backup as there is no working S3 connection")


def backup_mb_data():
    out_fp = CONFIG.DUMP_DATA_DIR / "mb_postgres.dump"

    env = os.environ.copy()
    env["PGPASSWORD"] = MB_POSTGRES_PASSWORD
    cmd = f"pg_dump -h localhost -U postgres -p 6543 -Fc > {out_fp}"
    logging.info("calling pgdump on MB Postgres...")
    res = subprocess.run(cmd, shell=True, env=env)
    if res.returncode == 0:
        logging.info(f"dumped MB Postgres to file {out_fp}")
        backup_file(out_fp)
    else:
        logging.info(f"PG dump was unsuccessful: {res}")


def restore_mb_data(date, bucket):
    object_name = f"{date.today()}/mb_postgres.dump"

    fp = CONFIG.DUMP_DATA_DIR / f"mb_postgres_{date.today()}.dump"
    if not fp.exists():
        s3.download(object_name=object_name, file_path=fp, bucket=bucket)

    docker.stop_docker(DOCKER_METABASE)

    env = os.environ.copy()
    env["PGPASSWORD"] = MB_POSTGRES_PASSWORD
    cmd = f"pg_restore --clean -h localhost -U postgres -p 6543 -d postgres {fp}"
    logging.info("calling pgrestore on MB Postgres...")
    res = subprocess.run(cmd, shell=True, env=env)
    if res.returncode == 0:
        logging.info(f"restored MB Postgres from file {fp}")
    else:
        logging.info(f"pg_restore was unsuccessful: {res}")

    docker.restart_docker(DOCKER_METABASE)
