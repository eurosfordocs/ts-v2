# main tables
ALL_RAW_DECLARATION = "all_raw_declaration"
ALL_RAW_RPPS = "all_raw_rpps"
TS_DECLARATION = "ts_declaration"
RAW_DECLARATION = "raw_declaration"
RAW_RPPS = "raw_rpps"
RPPS = "rpps"
RAW_FINESS = "raw_finess"
RAW_SIRENE = "raw_sirene"

BENEFICIAIRE = "beneficiaire"
ENTREPRISES = "entreprise"

OLD_DECLARATION = "old_declaration"

# tables used only in the transform process
TRANSFORM_DECLARATION = "transform_declaration"
CONVENTION = "convention"
