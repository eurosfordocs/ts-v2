class MissingTable(Exception):
    pass


class FileNotFoundOnS3Bucket(Exception):
    pass


class TSV2Exception(Exception):
    pass
