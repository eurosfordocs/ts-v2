# values
CONVENTION = "Convention"
REMUNERATION = "Rémunération"
ADVANTAGE = "Avantage"

NULL_INFORMATION_EVENEMENT = ["non-renseigne", "01-01-1900", "autre"]
NULL_AUTRE_MOTIF = [
    "autre",
    "0",
    "motif inconnu",
    "autre contrat",
    "zerovalue",
    "autres",
    "other",
    "texte",
    "non-renseigne",
    "",
    "na",
    "divers",
    "n/a",
    "avantage",
    "autre (prcision obligatoire)",
    "non",
]

NULL_BEN_IDENTIFIANT = [
    "(BLANK)",
    "(VIDE)",
    "-",
    ".",
    "0",
    "000",
    "0000",
    "000000000",
    "00000000000",
    "1",
    "10000000001",
    "999999999",
    "?",
    "ADELI",
    "ADELIINCONNU",
    "ASSO",
    "ASSOCIATION",
    "ASSOCIATIONLOI1901",
    "AUCUNRPPS",
    "AUTRE",
    "BR",
    "ENCOURS",
    "ETRANGER",
    "FINESS",
    "FINESSETABLISSEMENT",
    "FR",
    "INCONNU",
    "INFIRMIER",
    "INFIRMIERE",
    "INTERNE",
    "LOI1901",
    "LZVGMLKV",
    "MEDECINETRANGER",
    "N/A",
    "NA",
    "NC",
    "NEANT",
    "NIMÉDECIN,NIPHARMACIEN",
    "NO",
    "NON DISPONIBLE",
    "NON RENSEIGNE",
    "NON RENSEIGNÉ",
    "NON TROUVE",
    "NONDISPONIBLE",
    "NONRENSEIGNE",
    "NONRENSEIGNÉ",
    "NONTROUVE",
    "NONTROUVÉ",
    "NORPPS",
    "NOTAPPLICABLE",
    "NOVALUE",
    "OPTICIEN",
    "PAS DE NUMERO",
    "PASDENRPPS",
    "PASDENUMERO",
    "PASDENUMÉROADELI",
    "PASDERPPS",
    "PASDERPPSCARMÉDECINMILITAIRE",
    "PASDERPPSCARPSYCHOLOGUE",
    "PASDERPPSINFIRMIERE",
    "PENDING",
    "RPPS",
    "RPPSENATTENTE",
    "RPPSENCOURS",
    "SANNUMERO",
    "SANRPPS",
    "SANS",
    "SANS RPPS",
    "SANSINFORMATION",
    "SANSNUMERO",
    "SANSRPPS",
    "SIRET",
    "SIRET:",
    "SIRET:0",
    "SIRET:NON",
    "SIRETPHARMACIE",
    "SIRETPHIE",
    "SIRETT",
    "SO",
    "TEC",
    "XXXXXXXXX",
    "XXXXXXXXXX",
    "XXXXXXXXXXX",
]

MOTIF_REGEX = {
    ".*location.*stand.*": "Contrat d''achat ou de location d''espaces publicitaires",
    ".*hospitalit.*": "Hospitalité",
    "(^|.* )epu( .*|$)": "Formation",
    "^iqvia( .*|$)": "Enquête, étude, étude de marché (hors recherche)",
    ".*anti-cadeaux.*": "Formation",
    ".*cadeau.*": "Hospitalité",
    ".*chocolat.*": "Hospitalité",
}

MOTIFS_KEYWORDS = {
    "Hospitalité - hébergement": ["h[ôo]tels?", "h[ée]bergements?", "hébergment"],
    "Hospitalité - restauration": [
        "repas",
        "d[îi]ners?",
        "restauration",
        "d[ée]jeuners?",
        "collations?",
        "coffee?",
        "lunch",
        "viennoiserie",
    ],
    "Frais de transport": [
        "taxis?",
        "trains?",
        "avions?",
        "transports?",
        "d[eé]placements?",
    ],
    "Contrat de participation à une manifestation": [
        "conventions?",
        "colloques?",
        "congr[eè]s",
        "workshops?",
        "ateliers?",
        "rencontres?",
        "inscriptions?",
        "s[ée]minaires?",
        "adh[ée]sions?",
        "forum",
        "seminar",
    ],
    "Frais de réunion / d''organisation": ["r[ée]unions?"],
    "Contrat d''expert scientifique": ["honoraires?"],
    "Enquête, étude, étude de marché (hors recherche)": ["mediacom"],
    "Bourse de recherche": ["grant"],
}

BAD_IDENTITE = [
    "[ND]",
    "000000",
    "ASSOCIATION DECLAREE",
    "ASSOCIATION LOI 1901",
    "ASSOCIATION",
    "BUREAU",
    "CABINET IMAGERIE MEDICALE",
    "CABINET MEDICAL",
    "CENTRE HOSPITALIER GENERAL",
    "CENTRE HOSPITALIER REGIONAL",
    "CENTRE HOSPITALIER UNIVERSITAIRE",
    "CENTRE HOSPITALIER",
    "CHU",
    "CLINIQUE VETERINAIRE",
    "DIRECTION GENERALE",
    "ETABLISSEMENT DE SANTE",
    "ETABLISSEMENT HOSPITALIER",
    "ETABLISSEMENT PUBLIC ETABLISSEMENT HOSPITALIER",
    "ETABLISSEMENT",
    "GROUPE HOSPITALIER",
    "HOPITAL DE JOUR",
    "HOPITAL PRIVE",
    "HOPITAL",
    "HOSPITAL/CLINIC/MEDICAL CENTRE",
    "NON RENSEIGNEE",
    "PHARMACIE",
    "SARL",
    "SOCIETE A RESPONSABILITE LIMITEE",
    "SOCIETE PAR ACTIONS SIMPLIFIEE",
]

BAD_PRENOM = [
    "0",
    "Association De Patients",
    "Association Loi 1901",
    "Association",
    "Centre De Lutte Contre Le Cancer",
    "Centre Hospitalier",
    "Centre Lutte Cancer",
    "De Promouvoir Les Actions Et Les Echanges Relatifs A La Formation Medicale Continue",
    "Etablissement De Sante",
    "Etablissement De Soins",
    "Formation Medicale Continue",
    "Monsieur Ou Madame",
    "N.A",
    "N/A",
    "Na",
    "Organisation De Congres",
    "Promouvoir L''Enseignement Et La Recherche",
    "Sante",
]
