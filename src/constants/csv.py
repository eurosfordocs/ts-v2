from src.settings import CONFIG, Environment

SEMICOLON_SEPARATOR = ";"
COMMA_SEPARATOR = ","
DEFAULT_SEPARATOR = SEMICOLON_SEPARATOR

# Source URL and local zip names
TRANSPARENCE_SANTE_URL = (
    "https://minio.data.has-sante.fr/adex/data/prod/raw/ts/declarations.csv.gz"
)

RPPS_URL = "https://service.annuaire.sante.fr/annuaire-sante-webservices/V300/services/extraction/PS_LibreAcces"

FINESS_URL = (
    "https://www.data.gouv.fr/fr/datasets/r/48dadbce-56a8-4dc4-ac51-befcdfd82521"
)
SIREN_URL = (
    "https://files.data.gouv.fr/insee-sirene/StockUniteLegaleHistorique_utf8.zip"
)

# On the CI server, we download from our website because it is much faster than sante.fr
# the data is possibly outdated, but it doesn't matter.
if CONFIG.ENVIRONMENT == Environment.CI:
    RPPS_URL = (
        "https://gitlab.com/eurosfordocs/ts-v2/-/raw/main/frontend/public/ci/rpps.zip"
    )
    TRANSPARENCE_SANTE_URL = "https://gitlab.com/eurosfordocs/ts-v2/-/raw/main/frontend/public/ci/declarations.csv.gz"
    FINESS_URL = "https://gitlab.com/eurosfordocs/ts-v2/-/raw/main/frontend/public/ci/raw_finess_ci.csv"
    SIREN_URL = "https://gitlab.com/eurosfordocs/ts-v2/-/raw/main/frontend/public/ci/raw_sirene_ci.zip"

RPPS_ZIP_NAME = "rpps.zip"
RPPS_TXT_FILE_NAME = "PS_LibreAcces_Personne_activite.txt"

# RAW DATA
DECLARATION_CSV_GZ = "declarations.csv.gz"
OLD_DECLARATION_PARQUET = "old_declarations.parquet"
FINESS_CSV = "raw_finess.csv"
SIREN_CSV = "StockUniteLegaleHistorique_utf8.csv"
SIREN_ZIP_NAME = "StockUniteLegaleHistorique_utf8.zip"

# CLEAN DATA
TS_DECLARATION_PARQUET = "ts_declaration.parquet"
TS_DECLARATION_CSV_GZ = "ts_declaration.csv.gz"

# Regroupement des entreprises
REGROUPEMENT_ENTREPRISES_MANUEL_URL = "https://docs.google.com/spreadsheets/d/1uzxY1M2xQpuZL_1XrZT3xlg0aaP9H2HX5crK15YsOX0/gviz/tq?tqx=out:csv&sheet=regroupement_entreprises"

# categorisation des motifs
CATEGORISATION_MOTIFS_URL = "https://docs.google.com/spreadsheets/d/1uzxY1M2xQpuZL_1XrZT3xlg0aaP9H2HX5crK15YsOX0/gviz/tq?tqx=out:csv&sheet=categorisation_motifs"
META_CONVENTION_MOTIF_URL = "https://docs.google.com/spreadsheets/d/1uzxY1M2xQpuZL_1XrZT3xlg0aaP9H2HX5crK15YsOX0/gviz/tq?tqx=out:csv&sheet=meta_categories"
REGROUPEMENT_BENEFICIAIRE_MANUEL_URL = "https://docs.google.com/spreadsheets/d/1uzxY1M2xQpuZL_1XrZT3xlg0aaP9H2HX5crK15YsOX0/gviz/tq?tqx=out:csv&sheet=regroupement_efd_beneficiaire"
SIREN_MATCH_URL = "https://docs.google.com/spreadsheets/d/1uzxY1M2xQpuZL_1XrZT3xlg0aaP9H2HX5crK15YsOX0/gviz/tq?tqx=out:csv&sheet=regroupement_siren"
CATEGORIE_MATCHING_URL = "https://docs.google.com/spreadsheets/d/1uzxY1M2xQpuZL_1XrZT3xlg0aaP9H2HX5crK15YsOX0/gviz/tq?tqx=out:csv&sheet=categorie_matching"
