from src.clean_functions import (
    clean_nom,
    clean_prenom,
    clean_string,
    clean_to_lower,
    clean_type_declaration,
)
from src.format import efd, ts

STATIC_COLUMNS = [
    (efd.ANNEE, "INTEGER", f"substring({ts.SEMESTRE_ANNEE}, 1,4):: INTEGER"),
    (efd.ENTREPRISE_TS_ID, "INTEGER", None),
    (efd.ENTREPRISE_RAISON_SOCIALE, "VARCHAR", None),
    (efd.ENTREPRISE_SECTEUR_ACTIVITE, "VARCHAR", None),
    (efd.ENTREPRISE_SIREN, "VARCHAR", None),
    (efd.ENTREPRISE_PAYS, "VARCHAR", None),
    (efd.BEN_SIREN, "VARCHAR", None),
    (efd.BEN_RPPS, "VARCHAR", None),
    (efd.BEN_ADELI, "VARCHAR", None),
    (efd.BEN_ORDRE, "VARCHAR", None),
    (efd.BEN_RNA, "VARCHAR", None),
    (efd.BEN_ORIGIN_ID, "UBIGINT", None),
    (efd.BEN_SPECIALITE, "VARCHAR", None),
    (efd.CONVENTION_TS_ID, "INTEGER", None),
    (efd.CONVENTION_META_MOTIF, "VARCHAR", None),
    (efd.NOMBRE_AVANTAGES_LIES, "INTEGER", None),
    (efd.NOMBRE_REMUNERATIONS_LIEES, "INTEGER", None),
    (efd.MONTANT_TOTAL_AVANTAGES_LIES, "INTEGER", None),
    (efd.MONTANT_TOTAL_REMUNERATIONS_LIEES, "INTEGER", None),
    (efd.MONTANT_TOTAL_CONVENTION, "INTEGER", None),
    (efd.MONTANT_DECLARE_CONVENTION, "INTEGER", None),
    (efd.MONTANT_MASQUE, "BOOLEAN", None),
    (
        efd.BEN_EFD_ID,
        "UBIGINT",
        "hash(CONCAT(entreprise_id||'-'||coalesce(identite, id::varchar)||'-'||coalesce(prenom, '__NULL__')||'-'||coalesce(code_postal, '__NULL__')||'-'||coalesce(profession_libelle, '__NULL__')))",
    ),
    (efd.DATE_EVENEMENTT, "DATE", None),
]

ENRICHED_COLUMNS = [
    (
        efd.BEN_NOM_PRENOM,
        "VARCHAR",
        f"CASE WHEN {efd.BEN_PRENOM} IS NULL THEN {efd.BEN_NOM} "
        f"ELSE {efd.BEN_NOM} || ' ' || {efd.BEN_PRENOM} END",
    ),
]

TO_DROP = [
    ts.BENEFICIAIRE_TYPE_CODE,
    ts.BENEFICIAIRE_NOM_PRENOM_COM_NAME,
    ts.MERE_ID,
    ts.BENEFICIAIRE_PROFESSION_CODE,
]

RENAMING_MAP = {
    ts.ID: efd.DECLARATION_TS_ID,
    ts.TOKEN: efd.TOKEN,
    ts.LIEN_INTERET: efd.TYPE_DECLARATION,
    ts.IDENTIFIANT_UNIQUE: efd.DECLARATION_IDENTIFIANT,
    ts.CONVENTION_LIEE: efd.CONVENTION_IDENTIFIANT,
    ts.MOTIF_LIEN_INTERET_CODE: efd.MOTIF_LIEN_INTERET_CODE,
    ts.MOTIF_LIEN_INTERET: efd.MOTIF_LIEN_INTERET,
    ts.AUTRE_MOTIF: efd.AUTRE_MOTIF,
    ts.INFORMATION_EVENEMENT: efd.INFORMATION_EVENEMENT,
    ts.DATE: efd.DATE,
    ts.DATE_DEBUT: efd.DATE_DEBUT,
    ts.DATE_FIN: efd.DATE_FIN,
    ts.DEMANDE_DE_RECTIFICATION: efd.DEMANDE_DE_RECTIFICATION,
    ts.STATUT: efd.STATUT,
    ts.DATE_PUBLICATION: efd.DATE_PUBLICATION,
    ts.DATE_TRANSMISSION: efd.DATE_TRANSMISSION,
    ts.SEMESTRE_ANNEE: efd.SEMESTRE_ANNEE,
    ts.IDENTITE: efd.BEN_NOM,
    ts.PRENOM: efd.BEN_PRENOM,
    ts.ID_BENEFICIAIRE: efd.BENEFICIAIRE_TS_ID,
    ts.BENEFICIAIRE_CATEGORIE_CODE: efd.BEN_CATEGORIE_CODE,
    ts.BENEFICIAIRE_CATEGORIE: efd.BEN_CATEGORIE,
    ts.BENEFICIAIRE_TYPE: efd.BEN_TYPE_IDENTIFIANT,
    ts.BENEFICIAIRE_IDENTIFIANT: efd.BEN_IDENTIFIANT,
    ts.PROFESSION_LIBELLE: efd.BEN_PROFESSION,
    ts.STRUCTURE_EXERCICE: efd.BEN_STRUCTURE,
    ts.PAYS_CODE: efd.BEN_PAYS_CODE,
    ts.BENEFICIAIRE_NOM_PAYS: efd.BEN_PAYS,
    ts.ADRESSE: efd.BEN_ADRESSE,
    ts.CODE_POSTAL: efd.BEN_CODE_POSTAL,
    ts.VILLE: efd.BEN_VILLE,
    ts.VILLE_SANSCEDEX: efd.BEN_VILLE_SANSCEDEX,
    ts.GEOM: efd.BEN_LAT_LON,
    ts.COM_NAME: efd.BEN_COMMUNE,
    ts.COM_CODE: efd.BEN_COMMUNE_CODE,
    ts.EPCI_NAME: efd.BEN_EPCI,
    ts.EPCI_CODE: efd.BEN_EPCI_CODE,
    ts.DEP_CODE: efd.BEN_DEPARTEMENT_CODE,
    ts.DEP_NAME: efd.BEN_DEPARTEMENT,
    ts.REG_CODE: efd.BEN_REGION_CODE,
    ts.REG_NAME: efd.BEN_REGION,
    ts.ENTREPRISE_ID: efd.DECLARANT_TS_ID,
    ts.RAISON_SOCIALE: efd.DECLARANT_RAISON_SOCIALE,
    ts.SECTEUR_ACTIVITE: efd.DECLARANT_SECTEUR_ACTIVITE,
    ts.ENTR_VILLE: efd.DECLARANT_VILLE,
    ts.ENTR_DEP_NAME: efd.DECLARANT_DEPARTEMENT,
    ts.ENTR_REG_NAME: efd.DECLARANT_REGION,
    ts.ENTREPRISE_NOM_PAYS: efd.DECLARANT_PAYS,
    ts.CODE_POSTAL_ENTREPRISE: efd.DECLARANT_CODE_POSTAL,
    ts.NUMERO_SIREN: efd.DECLARANT_SIREN,
}

BEN_CLEANING_FUNCTIONS = {
    ts.PRENOM: clean_prenom,
    ts.IDENTITE: clean_nom,
    ts.PROFESSION_LIBELLE: clean_to_lower,
    ts.STRUCTURE_EXERCICE: clean_string,
}

RPPS_CLEANING_FUNCTIONS = {
    "prenom": clean_prenom,
    "nom": clean_nom,
    "profession": clean_to_lower,
}

DECLARATION_CLEANING_FUNCTIONS = {
    ts.LIEN_INTERET: clean_type_declaration,
    ts.MOTIF_LIEN_INTERET: clean_string,
    ts.AUTRE_MOTIF: clean_to_lower,
    ts.INFORMATION_EVENEMENT: clean_to_lower,
}

CLEANING_FUNCTIONS = {
    ts.STATUT: clean_string,
    ts.ADRESSE: clean_string,
    ts.VILLE: clean_string,
    ts.VILLE_SANSCEDEX: clean_string,
}


ALL_RAW_DECLARATION_COLUMNS = sorted(
    set(
        ts.COLONES_DECLARATION
        + [
            ts.DECLARATION_HASH,
            ts.DATE_AJOUT,
            ts.DATE_MODIF,
            ts.DATE_SUPPR,
            ts.SUPPRIME,
        ]
    )
)
