import logging
import unicodedata

from src.constants.miscellaneous import ADVANTAGE, CONVENTION, REMUNERATION


def correct_latin_utf_glitches(input_str):
    """
    :param input_str:
    :return:
    """
    if "Ã" in input_str:
        try:
            r = input_str.encode("cp1252").decode("utf-8")
            return r
        except UnicodeDecodeError:
            logging.debug(f"Decoding error correcting glitch of string: {input_str}")
            pass
        except UnicodeEncodeError:
            logging.debug(f"Encoding error correcting glitch of string: {input_str}")
            pass
    return input_str


def remove_extra_space(input_str):
    return " ".join(input_str.split()).strip()


def clean_string(input_str):
    if input_str is not None:
        return remove_extra_space(
            remove_bad_punctuation(
                remove_unicode_space(correct_latin_utf_glitches(input_str))
            )
        )


def remove_accents(input_str):
    """
    removes accents (see https://stackoverflow.com/questions/517923/what-is-the-best-way-to-remove-accents-normalize-in-a-python-unicode-string/518232#518232)
    """
    return "".join(
        c
        for c in unicodedata.normalize("NFD", input_str)
        if unicodedata.category(c) != "Mn"
    )


def clean_prenom(input_str):
    if input_str is not None:
        input_str = clean_string(input_str)
        return remove_accents(input_str).replace("-", " ").title()


def clean_to_lower(input_str):
    if input_str is not None:
        input_str = clean_string(input_str)
        return input_str.lower()


def clean_nom(input_str):
    if input_str is not None:
        input_str = clean_string(input_str)
        return remove_accents(input_str).replace("-", " ").replace("'", " ").upper()


def clean_type_declaration(input_str):
    mapping = {
        "convention": CONVENTION,
        "remuneration": REMUNERATION,
        "avantage": ADVANTAGE,
    }
    return mapping[input_str] if input_str in mapping else None


def remove_unicode_space(s):
    if s is not None:
        return s.replace("\xa0", " ").replace("\u202f", " ")


def remove_bad_punctuation(s):
    if s is not None:
        to_remove = ['"', "#", "*"]
        for punct in to_remove:
            s = s.replace(punct, "")
        s = s.replace("’", "'")
        return s
