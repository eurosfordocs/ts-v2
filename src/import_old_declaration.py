import os

from src.constants.csv import OLD_DECLARATION_PARQUET
from src.constants.directories import ROOTED_RAW_DATA_DIR, SQL_DIR
from src.constants.tables import OLD_DECLARATION
from src.utils import duck


def import_old_declarations(reset=False):
    load()
    transform()


def load():
    fp = os.path.join(ROOTED_RAW_DATA_DIR, OLD_DECLARATION_PARQUET)
    duck.import_parquet(OLD_DECLARATION, fp, delete=True)


def transform():
    duck.drop_table("transform_old_declaration")
    duck.execute_query(
        f"create table transform_old_declaration as (select * from {OLD_DECLARATION})"
    )
    duck.execute_script(os.path.join(SQL_DIR, "transform_old_declaration.sql"))
