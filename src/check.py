import logging

from src.constants.checks import CHECKS
from src.constants.tables import TRANSFORM_DECLARATION
from src.settings import CONFIG, Environment
from src.utils import duck


def check(table=TRANSFORM_DECLARATION):
    check_total_lines(table)
    check_convention_linking(table)

    if CONFIG.ENVIRONMENT != Environment.CI:
        check_unique_values(table)

    logging.info("Passed all data quality checks")


def check_total_lines(table):
    tot_rows = duck.execute_query(
        f"select count(*) from {table}", fetch_results="first"
    )[0]
    expected_at_least = 8000000 if CONFIG.ENVIRONMENT != Environment.CI else 9815
    assert (
        tot_rows >= expected_at_least
    ), f"Expected more than {expected_at_least} lines, got {tot_rows}"


def check_unique_values(table):
    for column, expected in CHECKS.items():
        expected = set(expected)
        actual = {
            x[0]
            for x in duck.execute_query(
                f"select distinct {column} from {table}",
                fetch_results="all",
            )
            if x[0] is not None
        }

        missings = sorted(expected - actual)
        # We don't ask for string equality as some very rare profession ('Physicien médical')
        # may come and go.
        assert len(missings) < 0.21 * len(
            expected
        ), f"Missing values for {column} : {','.join(missings)}"

        extra = sorted(actual - expected)
        assert not extra, f"Extra values for {column} : {','.join(extra)}"


def check_convention_linking(table):
    """
    Check categorisation of declarations
    the decla where motif_lien_interet is null are almost all remuneration for which we could not find the convention,
    so by checking this, we also check that the convention-linking is working.
    """
    actual = duck.execute_query(
        f"""
    SELECT ROUND(AVG(CAST(motif_lien_interet = 'Autre' or motif_lien_interet  is null as int)) * 100,2)
    FROM {table}
    """,
        fetch_results="first",
    )[0]
    no_cat_limit = 6 if CONFIG.ENVIRONMENT != Environment.CI else 13
    assert (
        actual < no_cat_limit
    ), f"Expected less than {no_cat_limit}% of all declarations with no motif_lien_interet or 'Autre', got {actual}"
