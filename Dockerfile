FROM python:3.11

WORKDIR /usr/src/app

COPY ./requirements.txt ./
RUN pip install --no-cache-dir -r requirements.txt

ENV RUNNING_IN_DOCKER True

CMD ["bash"]
